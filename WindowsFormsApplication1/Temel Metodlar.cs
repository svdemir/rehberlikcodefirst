﻿using System;
using System.Data;
using System.Data.SQLite;
using System.Windows.Forms;

namespace rbsProje
{
    public static class Temel_Metodlar
    {
        public static DataSet SqlKomutCalistirSorgu(string SQLkomut)
        {
            using (var DBbaglanti = new SQLiteConnection("Data Source=db.sqlite;Version=3;"))
            {
                SQLiteCommand command = new SQLiteCommand(SQLkomut, DBbaglanti);
                DBbaglanti.Open();
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(command);
                DataSet ds = new DataSet();
                try
                {
                    adapter.Fill(ds);
                    DBbaglanti.Close();
                    DBbaglanti.Dispose();
                    adapter.Dispose();
                    return ds;
                }
                catch (Exception)
                {
                    DBbaglanti.Close();
                    DBbaglanti.Dispose();
                    adapter.Dispose();
                    return null;
                }
            }
        }

        public static DataSet SqlKomutCalistirSorgu(SQLiteCommand SQLcommand)
        {
            using (var DBbaglanti = new SQLiteConnection("Data Source=db.sqlite;Version=3;"))
            {
                SQLcommand.Connection = DBbaglanti;
                DBbaglanti.Open();
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(SQLcommand);
                DataSet ds = new DataSet();
                try
                {
                    adapter.Fill(ds);
                }
                catch (Exception ex)
                {
                    return null;
                }
                DBbaglanti.Close();
                DBbaglanti.Dispose();
                adapter.Dispose();
                return ds;
            }
        }

        public static void SqlKomutCalistir(string SQLkomut)
        {
            using (var DBbaglanti = new SQLiteConnection("Data Source=db.sqlite;Version=3;"))
            {
                SQLiteCommand command = new SQLiteCommand(SQLkomut, DBbaglanti);
                DBbaglanti.Open();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception)
                {
                    return;
                }
                command.Dispose();
                DBbaglanti.Close();
                DBbaglanti.Dispose();
            }
        }

        public static bool SqlKomutCalistir(SQLiteCommand SQLcommand)
        {
            using (var DBbaglanti = new SQLiteConnection("Data Source=db.sqlite;Version=3;"))
            {
                SQLcommand.Connection = DBbaglanti;
                DBbaglanti.Open();
                try
                {
                    SQLcommand.ExecuteNonQuery();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public static void Numaralandir(DataGridView dataGridView)
        {
            if (dataGridView != null)
            {
                for (int count = 0; (count <= (dataGridView.Rows.Count - 1)); count++)
                {
                    dataGridView.Rows[count].HeaderCell.Value = string.Format((count + 1).ToString(), "0");
                }
            }
        }

        public static SQLiteConnection Baglanti
        {
            get
            {
                var DBbaglanti = new SQLiteConnection("Data Source=db.sqlite;Version=3;");
                return DBbaglanti;
            }
            set
            {
            }
        }

    }
}
