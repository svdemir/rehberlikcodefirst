﻿using rbsProje.Kontroller.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace rbsProje
{
    public partial class frmUyeGorusmeMEB : Form
    {
        private static frmUyeGorusmeMEB instance;

        public static frmUyeGorusmeMEB GetForm
        {
            get
            {
                if (instance == null || instance.IsDisposed)
                {
                    instance = new frmUyeGorusmeMEB();
                }
                else
                {
                    instance.BringToFront();
                }
                return instance;
            }
        }

        public frmUyeGorusmeMEB()
        {
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
        }

        private void frmUyeGorusmeMEB_Load(object sender, EventArgs e)
        {
            uCuyeListele21.btnDetayGetir.Visible = false;
            uCuyeListele21.btnResimDuzenle.Visible = false;
            uCuyeListele21.treeView1.Size = new Size(229, 220);

            backgroundWorker1.RunWorkerAsync();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            DBkaynakVerileriGetir();
        }

        private void DBkaynakVerileriGetir()
        {
            using (dbEntities veritabanim = new dbEntities())
            {
                cmbGorusmeyiYapan.DataSource = veritabanim.rehberOgretmenler.OrderBy(p => p.rehberOgretmenAdSoyad).ToList();
                cmbGorusmeyiYapan.DisplayMember = "rehberOgretmenAdSoyad";
                cmbGorusmeyiYapan.ValueMember = "rehberID";

                clistGorusmeSonuc.DataSource = veritabanim.kaynakSonucCumleleri.OrderBy(s => s.sonucCumlesi).ToList();
                clistGorusmeSonuc.DisplayMember = "sonucCumlesi";
                clistGorusmeSonuc.ValueMember = "cumleID";
            }
        }

        private void btnOgrenciGorusmeYazdir_Click(object sender, EventArgs e)
        {
            if (uCuyeListele21.txtAd.Text != null && uCuyeListele21.txtAd.Text != "")
            {
                PrintDocument doc = new PrintDocument();
                doc.PrintPage += new PrintPageEventHandler(doc_PrintPage);
                PrintPreviewDialog pdg = new PrintPreviewDialog();
                pdg.Document = doc;
                pdg.Height = 700;
                pdg.Width = 900;
                pdg.Show();
            }
            else
            {
                MessageBox.Show("Alanları boş bırakmayınız.", "Yazdırma Hatası", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void doc_PrintPage(object sender, PrintPageEventArgs e)
        {
            Image arkaplan = Properties.Resources.ogrenciForm;
            //e.Graphics.DrawImage(arkaplan, e.Graphics.VisibleClipBounds);
            e.Graphics.DrawImage(arkaplan, new RectangleF(0, 0, 825, 1170));



            string tarih = dateTimePicker1.Value.Date.ToString("dd MMMM yyyy");
            e.Graphics.DrawString(tarih, new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(600, 120, 620, 365));

            string ogrenciAd = uCuyeListele21.txtAd.Text;
            e.Graphics.DrawString(ogrenciAd, new System.Drawing.Font(new FontFamily("Segoe UI"), 9, FontStyle.Bold), Brushes.Black, new RectangleF(240, 175, 620, 365));

            string cinsiyet = uCuyeListele21.txtCinsiyet.Text;
            string cinsiyetKisa = "";
            if (!String.IsNullOrEmpty(cinsiyet))
            {
                cinsiyetKisa = cinsiyet.Substring(0, 1);
            }

            if (cinsiyetKisa == "K" || cinsiyetKisa == "k")
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(666, 171, 620, 365));
            }
            else if (cinsiyetKisa == "E" || cinsiyetKisa == "e")
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(713, 171, 620, 365));
            }

            string ogrenciNo = uCuyeListele21.txtNo.Text;
            e.Graphics.DrawString(ogrenciNo, new System.Drawing.Font(new FontFamily("Segoe UI"), 9, FontStyle.Bold), Brushes.Black, new RectangleF(670, 197, 620, 365));

            string ogrenciDogumTarihi = uCuyeListele21.txtDogumYil.Text;
            e.Graphics.DrawString(ogrenciDogumTarihi, new System.Drawing.Font(new FontFamily("Segoe UI"), 9, FontStyle.Bold), Brushes.Black, new RectangleF(240, 197, 620, 365));

            string okulAdim;
            using (dbEntities veritabanim = new dbEntities())
            {
                okulAdim = veritabanim.ayarlar.FirstOrDefault().OKULADI;
            }

            e.Graphics.DrawString(okulAdim, new System.Drawing.Font(new FontFamily("Segoe UI"), 9, FontStyle.Bold), Brushes.Black, new RectangleF(240, 222, 400, 50));

            string ogrenciSinifRehber = uCuyeListele21.txtSinif.Text + "         " + uCuyeListele21.txtRehberOgr.Text;
            e.Graphics.DrawString(ogrenciSinifRehber, new System.Drawing.Font(new FontFamily("Segoe UI"), 9, FontStyle.Bold), Brushes.Black, new RectangleF(240, 255, 620, 365));


            if (chkBasvuru1.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(236, 282, 620, 365));
            }
            if (chkBasvuru2.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(236, 299, 620, 365));
            }
            if (chkBasvuru3.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(412, 282, 620, 365));
            }
            if (chkBasvuru4.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(412, 299, 620, 365));
            }
            if (chkBasvuru5.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(561, 282, 620, 365));
            }
            if (chkBasvuru6.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(561, 299, 620, 365));
            }

            //konu gelecek

            if (chkGorusmeKonusu1.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(236, 330, 620, 365));
            }
            if (chkGorusmeKonusu2.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(236, 346, 620, 365));
            }
            if (chkGorusmeKonusu3.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(236, 362, 620, 365));
            }
            if (chkGorusmeKonusu4.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(412, 330, 620, 365));
            }
            if (chkGorusmeKonusu5.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(412, 346, 620, 365));
            }
            if (chkGorusmeKonusu6.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(412, 362, 620, 365));
            }
            if (chkGorusmeKonusu7.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(560, 321, 620, 365));
            }
            if (chkGorusmeKonusu8.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(560, 337, 620, 365));
            }
            if (chkGorusmeKonusu9.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(560, 353, 620, 365));
            }
            if (chkGorusmeKonusu10.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(560, 369, 620, 365));
            }

            //-----------------

            if (chkYapilanCalisma1.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(236, 395, 620, 365));
            }
            if (chkYapilanCalisma2.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(410, 395, 620, 365));
            }
            if (chkYapilanCalisma3.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(560, 395, 620, 365));
            }
            if (chkYapilanCalisma4.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(236, 419, 620, 365));
            }

            StringFormat format = new StringFormat();
            format.Alignment = StringAlignment.Center;

            StringFormat format2 = new StringFormat();
            format2.LineAlignment = StringAlignment.Center;

            string gorusmeMetni = richTextBox1.Text;
            //e.Graphics.DrawRectangle(new Pen(Color.Black, 1), new Rectangle(230, 466, 526, 128));  //görüşme metni
            e.Graphics.DrawString(gorusmeMetni, new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(235, 466, 526, 128), format2);
            e.Graphics.DrawString(tarih, new System.Drawing.Font(new FontFamily("Segoe UI"), 10, FontStyle.Bold), Brushes.Black, new RectangleF(90, 555, 620, 365));


            string sonucCumlesi = "";
            for (int i = 0; i < (clistGorusmeSonuc.CheckedItems.Count); i++)
            {
                sonucCumlesi += "\t\u2022 " + ((kaynakSonucCumleleri)clistGorusmeSonuc.CheckedItems[i]).sonucCumlesi + "\n";
            }
            //e.Graphics.DrawRectangle(new Pen(Color.Black, 1), new Rectangle(230, 847, 526, 128));  //Sonuçlar
            e.Graphics.DrawString(sonucCumlesi, new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(235, 847, 526, 128), format2);


            if (chkYonKurum1.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(236, 983, 620, 365));
            }
            if (chkYonKurum2.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(317, 983, 620, 365));
            }
            if (chkYonKurum3.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(405, 983, 620, 365));
            }
            if (chkYonKurum4.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(500, 983, 620, 365));
            }
            if (chkYonKurum5.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(577, 983, 620, 365));
            }
            if (chkYonKurum6.Checked)
            {
                e.Graphics.DrawString("X", new System.Drawing.Font(new FontFamily("Segoe UI"), 14, FontStyle.Bold), Brushes.Black, new RectangleF(651, 983, 620, 365));
            }

            string gorusmeYapanKisi = cmbGorusmeyiYapan.Text;
            e.Graphics.DrawString(gorusmeYapanKisi, new System.Drawing.Font(new FontFamily("Segoe UI"), 12, FontStyle.Bold), Brushes.Black, new RectangleF(550, 1033, 230, 130), format);

        }

        private void btnKaydet_Click(object sender, EventArgs e)
        {

            UyeGorusmeMEB mb = new UyeGorusmeMEB();
            mb.BasvurmaSekli = new List<string>();
            mb.CalismaOzeti = richTextBox1.Text;
            mb.GorusmeKonusu = new List<string>();

            mb.OkulNo = uCuyeListele21.ogrNo;

            mb.Sonuc = new List<string>();
            mb.YapılanCalisma = new List<string>();
            mb.YonlendirilenKurum = new List<string>();
            mb.genelGorusmeID = Convert.ToInt32(DateTime.Now.ToString("HHmmssfff"));
            mb.GorusmeTarihi = dateTimePicker1.Value.ToString("yyyyMMdd");
            mb.GorusmeYapan = Convert.ToInt64(cmbGorusmeyiYapan.SelectedValue);


            foreach (Control c in this.Controls["groupBox3"].Controls["groupBox1"].Controls)
            {
                if (((CheckBox)c).Checked)
                {
                    mb.BasvurmaSekli.Add(c.Text);
                }
            }

            foreach (Control c in this.Controls["groupBox3"].Controls["groupBox4"].Controls)
            {
                if (((CheckBox)c).Checked)
                {
                    mb.YapılanCalisma.Add(c.Text);
                }
            }

            foreach (Control c in this.Controls["groupBox3"].Controls["groupBox2"].Controls)
            {
                if (((CheckBox)c).Checked)
                {
                    mb.GorusmeKonusu.Add(c.Text);
                }
            }

            foreach (Control c in this.Controls["groupBox3"].Controls["groupBox5"].Controls)
            {
                if (((CheckBox)c).Checked)
                {
                    mb.YonlendirilenKurum.Add(c.Text);
                }
            }


            for (int i = 0; i < clistGorusmeSonuc.CheckedItems.Count; i++)
            {
                mb.Sonuc.Add(((kaynakSonucCumleleri)clistGorusmeSonuc.CheckedItems[i]).sonucCumlesi);
            }

            string serializedData = string.Empty;
            XmlSerializer serializer = new XmlSerializer(typeof(UyeGorusmeMEB));
            using (StringWriter sw = new StringWriter())
            {
                serializer.Serialize(sw, mb);
                serializedData = sw.ToString();
            }

            //UyeGorusmeMEB deserializedPerson;
            //XmlSerializer deserializer = new XmlSerializer(typeof(UyeGorusmeMEB));
            //using (TextReader tr = new StringReader(serializedData))
            //{
            //    deserializedPerson = (UyeGorusmeMEB)deserializer.Deserialize(tr);
            //}

            string komut = "INSERT INTO iller(id,baslik) VALUES('" + mb.genelGorusmeID.ToString() + "','" + serializedData + "')";
            Temel_Metodlar.SqlKomutCalistir(komut);
        }
    }
}
