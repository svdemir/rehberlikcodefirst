﻿using System;
using System.Windows.Forms;
using System.Linq;
using System.Collections.Generic;
using System.Collections;
using System.Data.Entity;

namespace rbsProje.Kontroller.Ayarlar
{
    public partial class UCayarlar : UserControl
    {
        public UCayarlar()
        {
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
        }

        private void btnKaydet_Click(object sender, EventArgs e)
        {
            using (dbEntities veritabanim = new dbEntities())
            {
                var sorgu = veritabanim.ayarlar.Where(v => v.ID == 1).SingleOrDefault();
                sorgu.OKULADI = txtOkulAdi.Text;
                veritabanim.SaveChanges();
                MessageBox.Show("Ayarlar başarıyla kaydedildi.", "Ayarlar", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void UCayarlar_Load(object sender, EventArgs e)
        {
            backgroundWorker1.RunWorkerAsync();
        }

        private void btnHepsiniSil_Click(object sender, EventArgs e)
        {
            DialogResult cevap = MessageBox.Show("Silme işlemini onaylıyor musunuz?", "Silme İşlemi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (cevap == DialogResult.Yes)
            {
                backgroundWorker2.RunWorkerAsync();
            }
        }

        private void GorusmeSil()
        {
            using (dbEntities veritabanim = new dbEntities())
            {
                var silinecekObje = veritabanim.gorusmeOgrenci.RemoveRange(veritabanim.gorusmeOgrenci);
                var silinecek1 = veritabanim.analizGorusmeKonulari.RemoveRange(veritabanim.analizGorusmeKonulari);
                var silinecek2 = veritabanim.analizSonucCumleleri.RemoveRange(veritabanim.analizSonucCumleleri);
                var silinecek3 = veritabanim.analizSorunAlanlari.RemoveRange(veritabanim.analizSorunAlanlari);
                var silinecek4 = veritabanim.analizYapilacakCalismalar.RemoveRange(veritabanim.analizYapilacakCalismalar);
                try
                {
                    veritabanim.SaveChanges();
                }
                catch (Exception)
                {
                    MessageBox.Show("Veritabanı hatası. Tekrar Deneyiniz.", "Kayıtlar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void VeliGorusmeSil()
        {
            using (dbEntities veritabanim = new dbEntities())
            {
                var silinecekObjeZiyaret = veritabanim.gorusmeVeliZiyaret.RemoveRange(veritabanim.gorusmeVeliZiyaret);
                var silinecekObje = veritabanim.gorusmeVeli.RemoveRange(veritabanim.gorusmeVeli);
                var silinecek1 = veritabanim.analizVeliGorusmeKonulari.RemoveRange(veritabanim.analizVeliGorusmeKonulari);
                var silinecek2 = veritabanim.analizVeliSonucCumleleri.RemoveRange(veritabanim.analizVeliSonucCumleleri);
                var silinecek3 = veritabanim.analizVeliSorunAlanlari.RemoveRange(veritabanim.analizVeliSorunAlanlari);
                var silinecek4 = veritabanim.analizVeliYapilacakCalismalar.RemoveRange(veritabanim.analizVeliYapilacakCalismalar);
                try
                {
                    veritabanim.SaveChanges();
                }
                catch (Exception)
                {
                    MessageBox.Show("Veritabanı hatası. Tekrar Deneyiniz.", "Kayıtlar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void OgretmenGorusmeSil()
        {
            using (dbEntities veritabanim = new dbEntities())
            {
                var silinecekObje = veritabanim.gorusmeOgretmen.RemoveRange(veritabanim.gorusmeOgretmen);
                var silinecek1 = veritabanim.analizOgretmenGorusmeKonulari.RemoveRange(veritabanim.analizOgretmenGorusmeKonulari);
                var silinecek2 = veritabanim.analizOgretmenSonucCumleleri.RemoveRange(veritabanim.analizOgretmenSonucCumleleri);
                var silinecek3 = veritabanim.analizOgretmenSorunAlanlari.RemoveRange(veritabanim.analizOgretmenSorunAlanlari);
                var silinecek4 = veritabanim.analizOgretmenYapilacakCalismalar.RemoveRange(veritabanim.analizOgretmenYapilacakCalismalar);

                try
                {
                    veritabanim.SaveChanges();

                }
                catch (Exception)
                {
                    MessageBox.Show("Veritabanı hatası. Tekrar Deneyiniz.", "Kayıtlar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void OgrenciBilgilerSil(long ogrNo)
        {
            using (dbEntities veritabanim = new dbEntities())
            {
                var silinecekObje = veritabanim.UYELER.RemoveRange(veritabanim.UYELER.Where(s=>s.NO==ogrNo));
                var silinecek1 = veritabanim.tanitimFisiAnneBilgisi.RemoveRange(veritabanim.tanitimFisiAnneBilgisi.Where(s => s.ogrNo == ogrNo));
                var silinecek2 = veritabanim.tanitimFisiBabaBilgisi.RemoveRange(veritabanim.tanitimFisiBabaBilgisi.Where(s => s.ogrNo == ogrNo));
                var silinecek3 = veritabanim.tanitimFisiKardesBilgisi.RemoveRange(veritabanim.tanitimFisiKardesBilgisi.Where(s => s.ogrNo == ogrNo));
                var silinecek4 = veritabanim.tanitimFisiVeliBilgisi.RemoveRange(veritabanim.tanitimFisiVeliBilgisi.Where(s => s.ogrNo == ogrNo));
                var silinecek5 = veritabanim.tanitimFisiEkAciklamalar.RemoveRange(veritabanim.tanitimFisiEkAciklamalar.Where(s => s.ogrNo == ogrNo));
                var silinecek6 = veritabanim.tanitimFisiGenelDurum.RemoveRange(veritabanim.tanitimFisiGenelDurum.Where(s => s.ogrNo == ogrNo));
                var silinecek7 = veritabanim.tanitimFisiOgrenimDurumu.RemoveRange(veritabanim.tanitimFisiOgrenimDurumu.Where(s => s.ogrNo == ogrNo));
                var silinecek8 = veritabanim.tanitimFisiSaglikDurumu.RemoveRange(veritabanim.tanitimFisiSaglikDurumu.Where(s => s.ogrNo == ogrNo));               

                try
                {
                    veritabanim.SaveChanges();
                }
                catch (Exception)
                {
                    MessageBox.Show("Veritabanı hatası. Tekrar Deneyiniz.", "Ayarlar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }


        private void OgrenciBilgilerSil()
        {
            using (dbEntities veritabanim = new dbEntities())
            {
                var silinecekObje = veritabanim.UYELER.RemoveRange(veritabanim.UYELER);
                var silinecek1 = veritabanim.tanitimFisiAnneBilgisi.RemoveRange(veritabanim.tanitimFisiAnneBilgisi);
                var silinecek2 = veritabanim.tanitimFisiBabaBilgisi.RemoveRange(veritabanim.tanitimFisiBabaBilgisi);
                var silinecek3 = veritabanim.tanitimFisiKardesBilgisi.RemoveRange(veritabanim.tanitimFisiKardesBilgisi);
                var silinecek4 = veritabanim.tanitimFisiVeliBilgisi.RemoveRange(veritabanim.tanitimFisiVeliBilgisi);
                var silinecek5 = veritabanim.tanitimFisiEkAciklamalar.RemoveRange(veritabanim.tanitimFisiEkAciklamalar);
                var silinecek6 = veritabanim.tanitimFisiGenelDurum.RemoveRange(veritabanim.tanitimFisiGenelDurum);
                var silinecek7 = veritabanim.tanitimFisiOgrenimDurumu.RemoveRange(veritabanim.tanitimFisiOgrenimDurumu);
                var silinecek8 = veritabanim.tanitimFisiSaglikDurumu.RemoveRange(veritabanim.tanitimFisiSaglikDurumu);



                try
                {
                    veritabanim.SaveChanges();
                }
                catch (Exception)
                {
                    MessageBox.Show("Veritabanı hatası. Tekrar Deneyiniz.", "Ayarlar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnSinifAtlat_Click(object sender, EventArgs e)
        {
            DialogResult cevap = MessageBox.Show("Sınıf atlatma işlemini onaylıyor musunuz?", "Sınıf Atlatma İşlemi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (cevap == DialogResult.Yes)
            {
                using (dbEntities veritabanim = new dbEntities())
                {
                    var ogrListesi = veritabanim.UYELER.ToList();
                    for (int i = 0; i < ogrListesi.Count; i++)
                    {
                        string sinif = ogrListesi[i].SINIF.Substring(0, 1);
                        string sube = ogrListesi[i].SINIF.Substring(1, 2);

                        if (sinif == "8" || sinif == "12")
                        {
                            ogrListesi[i].SINIF = "GENEL";
                        }
                        else if (sinif == "G")
                        {

                        }
                        else
                        {
                            int yeniSinif = Convert.ToInt32(sinif) + 1;
                            ogrListesi[i].SINIF = yeniSinif.ToString() + sube;
                        }
                    }
                    try
                    {
                        veritabanim.SaveChanges();
                        MessageBox.Show("Sınıflar başarıyla yükseltildi.", "Ayarlar", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Veritabanı hatası. Tekrar Deneyiniz.", "Ayarlar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    //silme işlemleri. sınıfı atlatılan öğrencilerin görüşme kayıtları siliniyor.
                    var silinecekOgrListesi = veritabanim.UYELER.Where(s => s.SINIF == "GENEL").ToList();
                    IList silinecekIDler;
                    for (int i = 0; i < silinecekOgrListesi.Count; i++)
                    {
                        long deger = silinecekOgrListesi[i].NO;

                        silinecekIDler = veritabanim.gorusmeOgrenci.Where(n => n.ogrNo == deger).Select(sc => sc.gorusmeID).ToList();
                        if (silinecekIDler.Count > 0)
                        {
                            for (int j = 0; j < silinecekIDler.Count; j++)
                            {
                                GorusmeSil(silinecekIDler[j].ToString());
                            }
                        }

                        OgrenciBilgilerSil(deger);
                    }
                }
            }
        }

        private void GorusmeSil(string silinecekID)
        {
            long numericGorID = Convert.ToInt64(silinecekID);

            using (dbEntities veritabanim = new dbEntities())
            {

                var silinecekObje = veritabanim.gorusmeOgrenci.Where(s => s.gorusmeID == numericGorID).SingleOrDefault();
                if (silinecekObje!=null)
                {
                    veritabanim.gorusmeOgrenci.Remove(silinecekObje);
                }

                var silinecek1 = veritabanim.analizGorusmeKonulari.Where(s => s.gorusmeID == numericGorID).SingleOrDefault();
                if (silinecek1 != null)
                {
                    veritabanim.analizGorusmeKonulari.Remove(silinecek1);
                }
                                                

                var silinecek2 = veritabanim.analizSonucCumleleri.Where(s => s.gorusmeID == numericGorID).ToList();
                if (silinecek2 != null)
                {
                    veritabanim.analizSonucCumleleri.RemoveRange(silinecek2);
                }
                
                var silinecek3 = veritabanim.analizSorunAlanlari.Where(s => s.gorusmeID == numericGorID).ToList();
                if (silinecek3 != null)
                {
                    veritabanim.analizSorunAlanlari.RemoveRange(silinecek3);
                }
                
                var silinecek4 = veritabanim.analizYapilacakCalismalar.Where(s => s.gorusmeID == numericGorID).ToList();
                if (silinecek4 != null)
                {
                    veritabanim.analizYapilacakCalismalar.RemoveRange(silinecek4);
                }               

                try
                {
                    veritabanim.SaveChanges();
                }
                catch (Exception)
                {
                }
            }
        }
        
        private void TemelVeriGetir()
        {
            using (dbEntities veritabanim = new dbEntities())
            {
                var sorgu = veritabanim.ayarlar.Where(ay => ay.ID == 1).SingleOrDefault();
                txtOkulAdi.Text = sorgu.OKULADI;
            }
        }

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            TemelVeriGetir();
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            label4.Visible = false;
        }

        private void backgroundWorker2_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            VeliGorusmeSil();
            GorusmeSil();
            OgretmenGorusmeSil();
            OgrenciBilgilerSil();
            MessageBox.Show("Veritabanındaki veriler silindi.", "Silme İşlemi", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }
    }
}
