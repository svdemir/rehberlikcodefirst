﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace rbsProje.Kontroller.Model
{
    public class UyeGorusmeMEB
    {

        public int genelGorusmeID { get; set; }

        public long OkulNo { get; set; }
        public List<string> BasvurmaSekli { get; set; }
        public List<string> GorusmeKonusu { get; set; }
        public List<string> YapılanCalisma { get; set; }
        public List<string> Sonuc { get; set; }
        public List<string> YonlendirilenKurum { get; set; }
        public long GorusmeYapan { get; set; }
        public string CalismaOzeti { get; set; }
        public string GorusmeTarihi { get; set; }


    }
}
