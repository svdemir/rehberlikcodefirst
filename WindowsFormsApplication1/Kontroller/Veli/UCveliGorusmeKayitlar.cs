﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;

namespace rbsProje.Kontroller.Veli
{
    public partial class UCveliGorusmeKayitlar : UserControl
    {
        public UCveliGorusmeKayitlar()
        {
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
        }

        private void btnGetir_Click(object sender, EventArgs e)
        {
            btnGuncelle.Enabled = false;
            btnSil.Enabled = false;
            GridTemizle();
            try
            {
                backgroundWorker2.RunWorkerAsync();

            }
            catch (Exception)
            {
                MessageBox.Show("Veritabanı hatası. Tekrar Deneyiniz.", "Kayıtlar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnHepsiniGetir_Click(object sender, EventArgs e)
        {
            btnGuncelle.Enabled = false;
            btnSil.Enabled = false;
            GridTemizle();
            try
            {
                backgroundWorker1.RunWorkerAsync();
            }
            catch (Exception)
            {
                MessageBox.Show("Veritabanı hatası. Tekrar Deneyiniz.", "Kayıtlar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnGuncelle_Click(object sender, EventArgs e)
        {
            try
            {
                frmVeliGor guncellenecekGorusme = new frmVeliGor(Convert.ToInt64(dataGridView1.SelectedRows[0].Cells[5].Value), true);
                guncellenecekGorusme.Show();
            }
            catch (Exception)
            {
                MessageBox.Show("Veritabanı hatası. Tekrar Deneyiniz.", "Kayıtlar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSil_Click(object sender, EventArgs e)
        {
            string silinecekID = dataGridView1.SelectedRows[0].Cells[5].Value.ToString();

            long numericGorID = Convert.ToInt64(silinecekID);

            using (dbEntities veritabanim = new dbEntities())
            {
                var silinecekObje = veritabanim.gorusmeVeli.Where(s => s.gorusmeID == numericGorID).SingleOrDefault();
                veritabanim.gorusmeVeli.Remove(silinecekObje);

                var silinecek1 = veritabanim.analizVeliGorusmeKonulari.Where(s => s.gorusmeID == numericGorID).SingleOrDefault();
                veritabanim.analizVeliGorusmeKonulari.Remove(silinecek1);

                var silinecek2 = veritabanim.analizVeliSonucCumleleri.Where(s => s.gorusmeID == numericGorID).ToList();
                veritabanim.analizVeliSonucCumleleri.RemoveRange(silinecek2);

                var silinecek3 = veritabanim.analizVeliSorunAlanlari.Where(s => s.gorusmeID == numericGorID).ToList();
                veritabanim.analizVeliSorunAlanlari.RemoveRange(silinecek3);

                var silinecek4 = veritabanim.analizVeliYapilacakCalismalar.Where(s => s.gorusmeID == numericGorID).ToList();
                veritabanim.analizVeliYapilacakCalismalar.RemoveRange(silinecek4);

                try
                {
                    veritabanim.SaveChanges();
                    MessageBox.Show("Silme işlemi başarılı.", "Kayıtlar", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                catch (Exception)
                {
                    MessageBox.Show("Veritabanı hatası. Tekrar Deneyiniz.", "Kayıtlar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            GridTemizle();
        }

        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtOgrNo.Text))
            {
                long numericOgrNo = Convert.ToInt64(txtOgrNo.Text);
                DbVeriGetir(numericOgrNo);
            }
            else
            {
                MessageBox.Show("Numara alanı boş olamaz.", "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            DbVeriGetir();
        }

        private void UCveliGorusmeKayitlar_Load(object sender, EventArgs e)
        {
            txtOgrNo.Focus();
        }

        private void txtOgrNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnGetir.PerformClick();
            }
        }

        private void GridTemizle()
        {
            dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();
            dataGridView1.Refresh();
            gridZiyaret.DataSource = null;
            gridZiyaret.Rows.Clear();
            gridZiyaret.Refresh();
        }

        private void txtOgrNo_TextChanged(object sender, EventArgs e)
        {
            btnGetir.Enabled = true;
        }

        private void DbVeriGetir(long ogrNo)
        {
            try
            {
                using (dbEntities veritabanim = new dbEntities())
                {
                    var sorgu = (from g in veritabanim.gorusmeVeli
                                 join u in veritabanim.UYELER on g.ogrNo equals u.NO
                                 join r in veritabanim.rehberOgretmenler on g.calismayiYapan equals r.rehberID
                                 join ve in veritabanim.tanitimFisiVeliBilgisi on g.ogrNo equals ve.ogrNo
                                 select new
                                 {
                                     g.ogrNo,
                                     g.gorusmeTarihi,
                                     u.SINIF,
                                     AdiSoyadi = u.AD + " " + u.SOYAD,
                                     ve.adSoyad,
                                     r.rehberOgretmenAdSoyad,
                                     g.gorusmeID
                                 }).Where(g => g.ogrNo == ogrNo).ToList();

                    if (sorgu.Count > 0)
                    {
                        for (int i = 0; i < sorgu.Count; i++)
                        {
                            string format = "yyyyMMdd";
                            DateTime dt;
                            var z = DateTime.TryParseExact(sorgu[i].gorusmeTarihi, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
                            dataGridView1.Rows.Add(dt.ToString("D"), sorgu[i].SINIF, sorgu[i].AdiSoyadi, sorgu[i].adSoyad, sorgu[i].rehberOgretmenAdSoyad);
                        }
                        btnGuncelle.Enabled = true;
                        btnSil.Enabled = true;
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Veritabanı hatası. Tekrar Deneyiniz.", "Kayıtlar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void DbVeriGetir()
        {
            try
            {
                using (dbEntities veritabanim = new dbEntities())
                {
                    var sorgu = (from g in veritabanim.gorusmeVeli
                                 join u in veritabanim.UYELER on g.ogrNo equals u.NO
                                 join r in veritabanim.rehberOgretmenler on g.calismayiYapan equals r.rehberID
                                 join ve in veritabanim.tanitimFisiVeliBilgisi on g.ogrNo equals ve.ogrNo
                                 select new
                                 {
                                     g.gorusmeTarihi,
                                     u.SINIF,
                                     AdiSoyadi = u.AD + " " + u.SOYAD,
                                     ve.adSoyad,
                                     r.rehberOgretmenAdSoyad,
                                     g.gorusmeID
                                 }).ToList();

                    if (sorgu.Count > 0)
                    {
                        for (int i = 0; i < sorgu.Count; i++)
                        {
                            string format = "yyyyMMdd";
                            DateTime dt;
                            var z = DateTime.TryParseExact(sorgu[i].gorusmeTarihi, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
                            dataGridView1.Rows.Add(dt.ToString("D"), sorgu[i].SINIF, sorgu[i].AdiSoyadi, sorgu[i].adSoyad, sorgu[i].rehberOgretmenAdSoyad, sorgu[i].gorusmeID);
                        }
                        btnGuncelle.Enabled = true;
                        btnSil.Enabled = true;
                    }


                }
            }
            catch (Exception)
            {
                MessageBox.Show("Veritabanı hatası. Tekrar Deneyiniz.", "Kayıtlar", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

        }


        //** ziyaret işlemleri

        private void btnHepGetirVeliZiy_Click(object sender, EventArgs e)
        {
            btnGuncelleZiyaret.Enabled = false;
            btnSilZiyaret.Enabled = false;
            GridTemizle();
            try
            {
                backgroundWorker3.RunWorkerAsync();
            }
            catch (Exception)
            {
                MessageBox.Show("Veritabanı hatası. Tekrar Deneyiniz.", "Kayıtlar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void DbVeriGetirZiyaret()
        {
            try
            {
                using (dbEntities veritabanim = new dbEntities())
                {
                    var sorgu = (from g in veritabanim.gorusmeVeliZiyaret
                                 join u in veritabanim.UYELER on g.ogrNo equals u.NO
                                 join ve in veritabanim.tanitimFisiVeliBilgisi on g.ogrNo equals ve.ogrNo
                                 select new
                                 {
                                     g.tarih,
                                     AdiSoyadi = u.AD + " " + u.SOYAD,
                                     ve.adSoyad,
                                     g.id
                                 }).ToList();

                    if (sorgu.Count > 0)
                    {
                        for (int i = 0; i < sorgu.Count; i++)
                        {
                            string format = "yyyyMMdd";
                            DateTime dt;
                            var z = DateTime.TryParseExact(sorgu[i].tarih, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
                            gridZiyaret.Rows.Add(dt.ToString("D"), sorgu[i].AdiSoyadi, sorgu[i].adSoyad, sorgu[i].id);
                        }
                        btnGuncelleZiyaret.Enabled = true;
                        btnSilZiyaret.Enabled = true;
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Veritabanı hatası. Tekrar Deneyiniz.", "Kayıtlar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void DbVeriGetirZiyaret(long ogrenciNo)
        {
            try
            {
                using (dbEntities veritabanim = new dbEntities())
                {
                    var sorgu = (from g in veritabanim.gorusmeVeliZiyaret
                                 join u in veritabanim.UYELER on g.ogrNo equals u.NO
                                 join ve in veritabanim.tanitimFisiVeliBilgisi on g.ogrNo equals ve.ogrNo
                                 select new
                                 {
                                     g.ogrNo,
                                     g.tarih,
                                     AdiSoyadi = u.AD + " " + u.SOYAD,
                                     ve.adSoyad,
                                     g.id
                                 }).Where(g => g.ogrNo == ogrenciNo).ToList();

                    if (sorgu.Count > 0)
                    {
                        for (int i = 0; i < sorgu.Count; i++)
                        {
                            string format = "yyyyMMdd";
                            DateTime dt;
                            var z = DateTime.TryParseExact(sorgu[i].tarih, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
                            gridZiyaret.Rows.Add(dt.ToString("D"), sorgu[i].AdiSoyadi, sorgu[i].adSoyad, sorgu[i].id);
                        }
                        btnGuncelleZiyaret.Enabled = true;
                        btnSilZiyaret.Enabled = true;
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Veritabanı hatası. Tekrar Deneyiniz.", "Kayıtlar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void backgroundWorker3_DoWork(object sender, DoWorkEventArgs e)
        {
            DbVeriGetirZiyaret();
        }

        private void btnGetirVeliZiy_Click(object sender, EventArgs e)
        {
            btnGuncelleZiyaret.Enabled = false;
            btnSilZiyaret.Enabled = false;
            GridTemizle();
            try
            {
                backgroundWorker4.RunWorkerAsync();
            }
            catch (Exception)
            {
                MessageBox.Show("Veritabanı hatası. Tekrar Deneyiniz.", "Kayıtlar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void backgroundWorker4_DoWork(object sender, DoWorkEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtZiyaretNo.Text))
            {
                long numericOgrNo = Convert.ToInt64(txtOgrNo.Text);
                DbVeriGetirZiyaret(numericOgrNo);
            }
            else
            {
                MessageBox.Show("Numara alanı boş olamaz.", "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtZiyaretNo_TextChanged(object sender, EventArgs e)
        {
            btnGetirVeliZiy.Enabled = true;

        }

        private void txtZiyaretNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnGetirVeliZiy.PerformClick();
            }
        }

        private void btnSilZiyaret_Click(object sender, EventArgs e)
        {
            string silinecekID = gridZiyaret.SelectedRows[0].Cells[3].Value.ToString();
            long numericGorID = Convert.ToInt64(silinecekID);
            using (dbEntities veritabanim = new dbEntities())
            {
                var silinecekObje = veritabanim.gorusmeVeliZiyaret.Where(z => z.id == numericGorID).SingleOrDefault();
                veritabanim.gorusmeVeliZiyaret.Remove(silinecekObje);
                try
                {
                    veritabanim.SaveChanges();
                    MessageBox.Show("Silme işlemi başarılı.", "Kayıtlar", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                catch (Exception)
                {
                    MessageBox.Show("Veritabanı hatası. Tekrar Deneyiniz.", "Kayıtlar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            GridTemizle();
        }

        private void btnGuncelleZiyaret_Click(object sender, EventArgs e)
        {
            try
            {
                frmVeliZiyaret guncellenecekZiyaret = new frmVeliZiyaret(Convert.ToInt64(gridZiyaret.SelectedRows[0].Cells[3].Value));
                guncellenecekZiyaret.Show();
            }
            catch (Exception)
            {
                MessageBox.Show("Veritabanı hatası. Tekrar Deneyiniz.", "Kayıtlar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }




}




