﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Drawing.Printing;

namespace rbsProje.Kontroller.Veli
{
    public partial class UCveliGorusme : Uyeler.UCuyeListele2
    {
        public UCveliGorusme()
        {
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
        }

        private void DBkaynakVerileriGetir()
        {
            using (dbEntities veritabanim = new dbEntities())
            {
                cmbGorusmeyiYapan.DataSource = veritabanim.rehberOgretmenler.OrderBy(p => p.rehberOgretmenAdSoyad).ToList();
                cmbGorusmeyiYapan.DisplayMember = "rehberOgretmenAdSoyad";
                cmbGorusmeyiYapan.ValueMember = "rehberID";

                clistSorunAlanlari.DataSource = veritabanim.kaynakVeliSorunAlanlari.OrderBy(s => s.sorunAlani).ToList();
                clistSorunAlanlari.DisplayMember = "sorunAlani";
                clistSorunAlanlari.ValueMember = "alanID";

                clistYapilacakCalismalar.DataSource = veritabanim.kaynakVeliYapilacakCalismalar.OrderBy(y => y.yapilacakCalisma).ToList();
                clistYapilacakCalismalar.DisplayMember = "yapilacakCalisma";
                clistYapilacakCalismalar.ValueMember = "yapilacakID";

                clistGorusmeSonuc.DataSource = veritabanim.kaynakVeliSonucCumleleri.OrderBy(s => s.sonucCumlesi).ToList();
                clistGorusmeSonuc.DisplayMember = "sonucCumlesi";
                clistGorusmeSonuc.ValueMember = "cumleID";
            }
        }

        private void btnGorusmeKaydet_Click(object sender, EventArgs e)
        {
            GorusmeKaydetme(null);
            KontrolTemizleme();

        }

        long genelVeliGorusmeID;

        public void GorusmeKaydetme(string pramGorusmeID)
        {

            if (string.IsNullOrEmpty(pramGorusmeID))
            {
                genelVeliGorusmeID = Convert.ToInt64(DateTime.Now.ToString("yyyyMMddHHmmssfff"));
            }
            else
            {
                genelVeliGorusmeID = Convert.ToInt64(pramGorusmeID);
            }

            long ogrNoNumeric = Convert.ToInt64(txtNo.Text);

            using (dbEntities veritabanim = new dbEntities())
            {
                try
                {
                    gorusmeVeli veliGorusmem = new gorusmeVeli();
                    veliGorusmem.gorusmeID = genelVeliGorusmeID;
                    veliGorusmem.calismayiYapan = Convert.ToInt64(cmbGorusmeyiYapan.SelectedValue);


                    analizVeliGorusmeKonulari konum = new analizVeliGorusmeKonulari();
                    konum.gorusmeID = genelVeliGorusmeID;
                    konum.ogrNo = ogrNoNumeric;
                    konum.gorusmeKonusu = richTextBox1.Text;
                    veritabanim.analizVeliGorusmeKonulari.Add(konum);

                    for (int i = 0; i < clistGorusmeSonuc.CheckedItems.Count; i++)
                    {
                        analizVeliSonucCumleleri asc = new analizVeliSonucCumleleri();
                        asc.gorusmeID = genelVeliGorusmeID;
                        asc.ogrNo = ogrNoNumeric;
                        asc.sonucCumlesi = ((kaynakVeliSonucCumleleri)clistGorusmeSonuc.CheckedItems[i]).cumleID.ToString();
                        veritabanim.analizVeliSonucCumleleri.Add(asc);
                    }

                    veliGorusmem.gorusmeTarihi = dateTimePicker1.Value.ToString("yyyyMMdd");
                    veliGorusmem.ogrNo = ogrNoNumeric;

                    for (int i = 0; i < clistSorunAlanlari.CheckedItems.Count; i++)
                    {
                        analizVeliSorunAlanlari sorunAlanim = new analizVeliSorunAlanlari();
                        sorunAlanim.gorusmeID = genelVeliGorusmeID;
                        sorunAlanim.ogrNo = ogrNoNumeric;
                        sorunAlanim.sorunAlani = ((kaynakVeliSorunAlanlari)clistSorunAlanlari.CheckedItems[i]).alanID.ToString();
                        veritabanim.analizVeliSorunAlanlari.Add(sorunAlanim);
                    }

                    for (int i = 0; i < clistYapilacakCalismalar.CheckedItems.Count; i++)
                    {
                        analizVeliYapilacakCalismalar islemlerim = new analizVeliYapilacakCalismalar();
                        islemlerim.gorusmeID = genelVeliGorusmeID;
                        islemlerim.ogrNo = ogrNoNumeric;
                        islemlerim.yapilacakCalisma = ((kaynakVeliYapilacakCalismalar)clistYapilacakCalismalar.CheckedItems[i]).yapilacakID.ToString();
                        veritabanim.analizVeliYapilacakCalismalar.Add(islemlerim);
                    }
                    veritabanim.gorusmeVeli.Add(veliGorusmem);
                    veritabanim.SaveChanges();
                    MessageBox.Show("Görüşme kayıt işlemi başarılı.", "Görüşme Kayıt", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception)
                {
                    MessageBox.Show("Hata. Alanlara dikkat ediniz.", "Görüşme Kayıt", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }


        }

        private void btnOgrenciGorusmeYazdir_Click(object sender, EventArgs e)
        {
            PrintDocument doc = new PrintDocument();
            doc.PrintPage += new PrintPageEventHandler(doc_PrintPage);
            PrintPreviewDialog pdg = new PrintPreviewDialog();
            pdg.Document = doc;
            pdg.Height = 700;
            pdg.Width = 900;
            pdg.Show();
        }

        private void doc_PrintPage(object sender, PrintPageEventArgs e)
        {
            e.Graphics.DrawRectangle(new Pen(Color.Black, 3), new Rectangle(60, 80, 700, 1040));  //Genel Çerçeve
            e.Graphics.DrawString("VELİ GÖRÜŞME / PSİKOLOJİK DANIŞMA RAPORU", new System.Drawing.Font(new FontFamily("Segoe UI"), 16, FontStyle.Bold), Brushes.Black, new RectangleF(160, 100, 620, 365));

            string okulAdim;
            using (dbEntities veritabanim = new dbEntities())
            {
                okulAdim = veritabanim.ayarlar.FirstOrDefault().OKULADI;
            }

            StringFormat format = new StringFormat();
            format.LineAlignment = StringAlignment.Center;
            format.Alignment = StringAlignment.Center;
            e.Graphics.DrawString(okulAdim, new System.Drawing.Font(new FontFamily("Segoe UI"), 12, FontStyle.Bold), Brushes.Black, new RectangleF(200, 120, 400, 50), format); //***METİN ORTALAMA

            string ogrenciBilgi = "Öğrencinin Numarası :   " + txtNo.Text + "                                         " + "Görüşme Tarihi: " + dateTimePicker1.Text + "\n" + "\n" + "Öğrencinin Adı Soyadı :   " + txtAd.Text + "             " + "Öğrencinin Sınıfı :  " + txtSinif.Text + "\n" + "\n" + "Velinin Adı Soyadı :  " + txtVeliAdSoyad.Text + "\n" + "\n" + "Velinin Yakınlığı :  " + txtVeliYakinligi.Text;
            e.Graphics.DrawString(ogrenciBilgi, new System.Drawing.Font(new FontFamily("Segoe UI"), 9, FontStyle.Bold), Brushes.Black, new RectangleF(100, 180, 620, 365));

            StringFormat format2 = new StringFormat();
            format2.LineAlignment = StringAlignment.Center;

            string sorunAlaniCumlesi = "";
            for (int i = 0; i < (clistSorunAlanlari.CheckedItems.Count); i++)
            {
                sorunAlaniCumlesi += "\t\u2022 " + ((kaynakVeliSorunAlanlari)clistSorunAlanlari.CheckedItems[i]).sorunAlani + "\n";
            }
            e.Graphics.DrawRectangle(new Pen(Color.Black, 1), new Rectangle(85, 330, 650, 140));  //Sorun Alanları  
            e.Graphics.DrawString("Sorun Alanları", new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(85, 310, 650, 140));
            e.Graphics.DrawString(sorunAlaniCumlesi, new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(90, 340, 650, 130), format2);

            e.Graphics.DrawRectangle(new Pen(Color.Black, 1), new Rectangle(85, 495, 650, 180));  //Görüşme Konuları
            e.Graphics.DrawString("Görüşme Konusu", new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(85, 475, 650, 180));
            e.Graphics.DrawString(richTextBox1.Text, new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(90, 505, 650, 170), format2);

            string yapilacakCalismaCumlesi = "";
            for (int i = 0; i < (clistYapilacakCalismalar.CheckedItems.Count); i++)
            {
                yapilacakCalismaCumlesi += "\t\u2022 " + ((kaynakVeliYapilacakCalismalar)clistYapilacakCalismalar.CheckedItems[i]).yapilacakCalisma + "\n";
            }

            e.Graphics.DrawRectangle(new Pen(Color.Black, 1), new Rectangle(85, 700, 650, 140));  //Yapılacak Çalışmalar
            e.Graphics.DrawString("Yapılacak Çalışmalar", new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(85, 680, 650, 140));
            e.Graphics.DrawString(yapilacakCalismaCumlesi, new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(90, 710, 650, 130), format2);

            string sonucCumlesi = "";
            for (int i = 0; i < (clistGorusmeSonuc.CheckedItems.Count); i++)
            {
                sonucCumlesi += "\t\u2022 " + ((kaynakVeliSonucCumleleri)clistGorusmeSonuc.CheckedItems[i]).sonucCumlesi + "\n";
            }
            e.Graphics.DrawRectangle(new Pen(Color.Black, 1), new Rectangle(85, 870, 650, 180));  //Sonuçlar  
            e.Graphics.DrawString("Sonuç, Görüş ve Öneriler", new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(85, 850, 650, 18));
            e.Graphics.DrawString(sonucCumlesi, new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(90, 880, 620, 150), format2);


            string gorusmeYapanKisi = "Görüşmeyi Yapan" + "\n" + cmbGorusmeyiYapan.Text;
            e.Graphics.DrawString(gorusmeYapanKisi, new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(635, 1070, 130, 130));

        }

        private void UCveliGorusme_Load(object sender, EventArgs e)
        {
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            DBkaynakVerileriGetir();
        }

        private void txtNo_TextChanged(object sender, EventArgs e)
        {
            btnGorusmeKaydet.Enabled = true;
            btnOgrenciGorusmeYazdir.Enabled = true;
        }

        private void KontrolTemizleme()
        {
            dateTimePicker1.ResetText();
            cmbGorusmeyiYapan.SelectedIndex = 0;
            richTextBox1.ResetText();

            CheckedTemizle(clistGorusmeSonuc);
            CheckedTemizle(clistSorunAlanlari);
            CheckedTemizle(clistYapilacakCalismalar);
        }

        private void CheckedTemizle(CheckedListBox kontrol)
        {
            kontrol.ClearSelected();
            for (int i = 0; i < kontrol.Items.Count; i++)
            {
                kontrol.SetItemChecked(i, false);
            }
        }


        public void AsenkronVerileriGetir()
        {
            backgroundWorker1.RunWorkerAsync();
        }

    }
}
