﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace rbsProje.Kontroller.Uyeler
{
    public partial class UCuyeGorusmeKayitlar : UserControl
    {
        public UCuyeGorusmeKayitlar()
        {
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            DbVeriGetir();
        }

        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtOgrNo.Text))
            {
                long numericOgrNo = Convert.ToInt64(txtOgrNo.Text);
                DbVeriGetir(numericOgrNo);
            }
            else
            {
                MessageBox.Show("Numara alanı bş olamaz.", "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        private void DbVeriGetir(long ogrNo)
        {
            try
            {
                using (dbEntities veritabanim = new dbEntities())
                {
                    var sorgu = veritabanim.gorusmeOgrenci
                            .Join(veritabanim.UYELER,
                                                  c => c.ogrNo,
                                                  cm => cm.NO,
                          (c, cm) => new { geciciGorusme = c, geciciOgrenci = cm })
                          .Join(veritabanim.rehberOgretmenler,
                                                  geciciC => geciciC.geciciGorusme.calismayiYapan,
                                                  og => og.rehberID,
                          (geciciC, og) => new { Gorusme = geciciC.geciciGorusme, Ogrenci = geciciC.geciciOgrenci, Ogretmen = og })
                          .Where(p => p.Gorusme.ogrNo == ogrNo)
                          .Select(x => new { x.Gorusme.gorusmeTarihi, x.Ogrenci.NO, x.Ogrenci.SINIF, x.Ogrenci.AD, x.Ogrenci.SOYAD, x.Ogretmen.rehberOgretmenAdSoyad }).ToList();
                    for (int i = 0; i < sorgu.Count; i++)
                    {
                        string format = "yyyyMMdd";
                        DateTime dt;
                        var z = DateTime.TryParseExact(sorgu[i].gorusmeTarihi, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
                        dataGridView1.Rows.Add(dt.ToString("D"), sorgu[i].SINIF, sorgu[i].AD + " " + sorgu[i].SOYAD, sorgu[i].rehberOgretmenAdSoyad);
                    }
                }
                btnGuncelle.Enabled = true;
                btnSil.Enabled = true;
            }
            catch (Exception)
            {
                MessageBox.Show("Veritabanı hatası. Tekrar Deneyiniz.", "Kayıtlar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void DbVeriGetir()
        {
            try
            {
                using (dbEntities veritabanim = new dbEntities())
                {
                    var sorgu = veritabanim.gorusmeOgrenci
                            .Join(veritabanim.UYELER,
                                                  c => c.ogrNo,
                                                  cm => cm.NO,
                          (c, cm) => new { geciciGorusme = c, geciciOgrenci = cm })
                          .Join(veritabanim.rehberOgretmenler,
                                                  geciciC => geciciC.geciciGorusme.calismayiYapan,
                                                  og => og.rehberID,
                          (geciciC, og) => new { Gorusme = geciciC.geciciGorusme, Ogrenci = geciciC.geciciOgrenci, Ogretmen = og })
                          .Select(x => new { x.Gorusme.gorusmeTarihi, x.Ogrenci.NO, x.Ogrenci.SINIF, x.Ogrenci.AD, x.Ogrenci.SOYAD, x.Ogretmen.rehberOgretmenAdSoyad, x.Gorusme.gorusmeID }).ToList();
                    for (int i = 0; i < sorgu.Count; i++)
                    {
                        string format = "yyyyMMdd";
                        DateTime dt;
                        var z = DateTime.TryParseExact(sorgu[i].gorusmeTarihi, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
                        dataGridView1.Rows.Add(dt.ToString("D"), sorgu[i].SINIF, sorgu[i].AD + " " + sorgu[i].SOYAD, sorgu[i].rehberOgretmenAdSoyad, sorgu[i].gorusmeID);
                    }
                }
                if (dataGridView1.Rows.Count>0)
                {
                    btnGuncelle.Enabled = true;
                    btnSil.Enabled = true;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Veritabanı hatası. Tekrar Deneyiniz.", "Kayıtlar", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

        }

        private void btnGetir_Click(object sender, EventArgs e)
        {
            GridTemizle();
            try
            {
                backgroundWorker2.RunWorkerAsync();

            }
            catch (Exception)
            {
                MessageBox.Show("Veritabanı hatası. Tekrar Deneyiniz.", "Kayıtlar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void btnHepsiniGetir_Click(object sender, EventArgs e)
        {
            GridTemizle();
            try
            {
                backgroundWorker1.RunWorkerAsync();
            }
            catch (Exception)
            {
                MessageBox.Show("Veritabanı hatası. Tekrar Deneyiniz.", "Kayıtlar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtOgrNo_TextChanged(object sender, EventArgs e)
        {
            btnGetir.Enabled = true;
        }
        
        private void GridTemizle()
        {
            dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();

            dataGridView1.Refresh();
        }

        private void txtOgrNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnGetir.PerformClick();
            }
        }

        private void UCuyeGorusmeKayitlar_Load(object sender, EventArgs e)
        {
            txtOgrNo.Focus();
        }

        private void btnSil_Click(object sender, EventArgs e)
        {
            string silinecekID = dataGridView1.SelectedRows[0].Cells[4].Value.ToString();

            long numericGorID = Convert.ToInt64(silinecekID);

            using (dbEntities veritabanim = new dbEntities())
            {
                var silinecekObje = veritabanim.gorusmeOgrenci.Where(s => s.gorusmeID == numericGorID).SingleOrDefault();
                if (silinecekObje != null)
                {
                    veritabanim.gorusmeOgrenci.Remove(silinecekObje);
                }

                var silinecek1 = veritabanim.analizGorusmeKonulari.Where(s => s.gorusmeID == numericGorID).SingleOrDefault();
                if (silinecek1 != null)
                {
                    veritabanim.analizGorusmeKonulari.Remove(silinecek1);
                }


                var silinecek2 = veritabanim.analizSonucCumleleri.Where(s => s.gorusmeID == numericGorID).ToList();
                if (silinecek2 != null)
                {
                    veritabanim.analizSonucCumleleri.RemoveRange(silinecek2);
                }

                var silinecek3 = veritabanim.analizSorunAlanlari.Where(s => s.gorusmeID == numericGorID).ToList();
                if (silinecek3 != null)
                {
                    veritabanim.analizSorunAlanlari.RemoveRange(silinecek3);
                }

                var silinecek4 = veritabanim.analizYapilacakCalismalar.Where(s => s.gorusmeID == numericGorID).ToList();
                if (silinecek4 != null)
                {
                    veritabanim.analizYapilacakCalismalar.RemoveRange(silinecek4);
                }

                try
                {
                    veritabanim.SaveChanges();
                    MessageBox.Show("Silme işlemi başarılı.", "Kayıtlar", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                catch (Exception)
                {
                    MessageBox.Show("Veritabanı hatası. Tekrar Deneyiniz.", "Kayıtlar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            GridTemizle();
        }

        private void btnGuncelle_Click(object sender, EventArgs e)
        {
            try
            {
                frmUyeGorusme guncellenecekGorusme = new frmUyeGorusme(Convert.ToInt64(dataGridView1.SelectedRows[0].Cells[4].Value), true);
                guncellenecekGorusme.Show();
            }
            catch (Exception)
            {
                MessageBox.Show("Veritabanı hatası. Tekrar Deneyiniz.", "Kayıtlar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }

}

