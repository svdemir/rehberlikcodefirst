﻿namespace rbsProje.Kontroller.Uyeler
{
    partial class UCuyeListele2
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.btnTumunuGetir = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtKanGrubu = new System.Windows.Forms.TextBox();
            this.txtNo = new System.Windows.Forms.TextBox();
            this.btnDetayGetir = new System.Windows.Forms.Button();
            this.txtDogumIlce = new System.Windows.Forms.TextBox();
            this.txtDogumIL = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDogumYil = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtSinif = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtRehberOgr = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCinsiyet = new System.Windows.Forms.TextBox();
            this.txtAd = new System.Windows.Forms.TextBox();
            this.btnResimDuzenle = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.grbVeli = new System.Windows.Forms.GroupBox();
            this.richTextAdres = new System.Windows.Forms.RichTextBox();
            this.lblVeliAd = new System.Windows.Forms.Label();
            this.txtVeliAdSoyad = new System.Windows.Forms.TextBox();
            this.lblVeliAdres = new System.Windows.Forms.Label();
            this.txtVeliMeslek = new System.Windows.Forms.TextBox();
            this.lblVeliTel = new System.Windows.Forms.Label();
            this.lblVeliNeslek = new System.Windows.Forms.Label();
            this.lblVeliYakin = new System.Windows.Forms.Label();
            this.txtVeliYakinligi = new System.Windows.Forms.TextBox();
            this.txtVeliTel = new System.Windows.Forms.TextBox();
            this.grbAnne = new System.Windows.Forms.GroupBox();
            this.cmbAnneSag = new System.Windows.Forms.ComboBox();
            this.cmbAnneOz = new System.Windows.Forms.ComboBox();
            this.cmbAnneEngel = new System.Windows.Forms.ComboBox();
            this.cmAnneBirlik = new System.Windows.Forms.ComboBox();
            this.lblAnneAd = new System.Windows.Forms.Label();
            this.lblAnneEngel = new System.Windows.Forms.Label();
            this.lblAnneBirlik = new System.Windows.Forms.Label();
            this.lblAnneTel = new System.Windows.Forms.Label();
            this.txtAnneAd = new System.Windows.Forms.TextBox();
            this.txtAnneMeslek = new System.Windows.Forms.TextBox();
            this.lblAnneMeslek = new System.Windows.Forms.Label();
            this.lblAnneSag = new System.Windows.Forms.Label();
            this.lblAnneOz = new System.Windows.Forms.Label();
            this.txtAnneTel = new System.Windows.Forms.TextBox();
            this.grbBaba = new System.Windows.Forms.GroupBox();
            this.cmbSagOlu = new System.Windows.Forms.ComboBox();
            this.cmbOzUvey = new System.Windows.Forms.ComboBox();
            this.cmbBabaEngel = new System.Windows.Forms.ComboBox();
            this.cmbBirlik = new System.Windows.Forms.ComboBox();
            this.lblBabaAd = new System.Windows.Forms.Label();
            this.lblBabaEngel = new System.Windows.Forms.Label();
            this.lblBirliktelik = new System.Windows.Forms.Label();
            this.lblBabaTel = new System.Windows.Forms.Label();
            this.txtBabaAd = new System.Windows.Forms.TextBox();
            this.txtBabaMeslek = new System.Windows.Forms.TextBox();
            this.lblBabaMes = new System.Windows.Forms.Label();
            this.lblBabaSag = new System.Windows.Forms.Label();
            this.lblBabaOz = new System.Windows.Forms.Label();
            this.txtBabaTel = new System.Windows.Forms.TextBox();
            this.btnAileKaydet = new System.Windows.Forms.Button();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.lblAileCiddi = new System.Windows.Forms.Label();
            this.btnGenelKaydet = new System.Windows.Forms.Button();
            this.txtMeslekAlan = new System.Windows.Forms.TextBox();
            this.txtOncekiOkul = new System.Windows.Forms.TextBox();
            this.richOkulRahatsiz = new System.Windows.Forms.RichTextBox();
            this.cmbOda = new System.Windows.Forms.ComboBox();
            this.cmbDestek = new System.Windows.Forms.ComboBox();
            this.cmbMisafir = new System.Windows.Forms.ComboBox();
            this.cmbYetenek = new System.Windows.Forms.ComboBox();
            this.cmbSinifTekrari = new System.Windows.Forms.ComboBox();
            this.cmbEvIsinma = new System.Windows.Forms.ComboBox();
            this.cmbEv = new System.Windows.Forms.ComboBox();
            this.cmbAileCiddi = new System.Windows.Forms.ComboBox();
            this.cmbOgrenciCiddi = new System.Windows.Forms.ComboBox();
            this.lblYonelim = new System.Windows.Forms.Label();
            this.lblOncekiOkul = new System.Windows.Forms.Label();
            this.cmbAileProblem = new System.Windows.Forms.ComboBox();
            this.lblOkulRahatsiz = new System.Windows.Forms.Label();
            this.lblOda = new System.Windows.Forms.Label();
            this.lblOkulDisiDestek = new System.Windows.Forms.Label();
            this.lblMisafir = new System.Windows.Forms.Label();
            this.lblYetenek = new System.Windows.Forms.Label();
            this.cmbZararli = new System.Windows.Forms.ComboBox();
            this.lblSinifTekrari = new System.Windows.Forms.Label();
            this.lblEvIsinma = new System.Windows.Forms.Label();
            this.lblEv = new System.Windows.Forms.Label();
            this.lblOgrenciCiddi = new System.Windows.Forms.Label();
            this.lblAileProblem = new System.Windows.Forms.Label();
            this.lblZararlı = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.btnSaglikKaydet = new System.Windows.Forms.Button();
            this.cmbProtez = new System.Windows.Forms.ComboBox();
            this.cmbKaza = new System.Windows.Forms.ComboBox();
            this.cmbEngel = new System.Windows.Forms.ComboBox();
            this.lblProtez = new System.Windows.Forms.Label();
            this.lblKaza = new System.Windows.Forms.Label();
            this.lblEngel = new System.Windows.Forms.Label();
            this.lblKilo = new System.Windows.Forms.Label();
            this.lblBoy = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.btnOgrDurumuKaydet = new System.Windows.Forms.Button();
            this.richKulup = new System.Windows.Forms.RichTextBox();
            this.richBosZaman = new System.Windows.Forms.RichTextBox();
            this.txtBasarisizDers = new System.Windows.Forms.TextBox();
            this.txtBasariliDers = new System.Windows.Forms.TextBox();
            this.txtSinifRehber = new System.Windows.Forms.TextBox();
            this.lblKulup = new System.Windows.Forms.Label();
            this.lblBosZaman = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblSinifRehber = new System.Windows.Forms.Label();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.btnKardesKaydet = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.btnEkKaydet = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cOzBakim = new System.Windows.Forms.CheckBox();
            this.cDersGec = new System.Windows.Forms.CheckBox();
            this.cOkulKurallari = new System.Windows.Forms.CheckBox();
            this.cSinifKurallari = new System.Windows.Forms.CheckBox();
            this.cOgrtSaygisiz = new System.Windows.Forms.CheckBox();
            this.cArkadasSaygisiz = new System.Windows.Forms.CheckBox();
            this.cDikkat = new System.Windows.Forms.CheckBox();
            this.cArgo = new System.Windows.Forms.CheckBox();
            this.cKapanik = new System.Windows.Forms.CheckBox();
            this.cDersKonusuyor = new System.Windows.Forms.CheckBox();
            this.cHircin = new System.Windows.Forms.CheckBox();
            this.cSaldırgan = new System.Windows.Forms.CheckBox();
            this.cSakin = new System.Windows.Forms.CheckBox();
            this.cHaylaz = new System.Windows.Forms.CheckBox();
            this.cDaginik = new System.Windows.Forms.CheckBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnAra = new System.Windows.Forms.Button();
            this.lblListeleBaslik = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.grbVeli.SuspendLayout();
            this.grbAnne.SuspendLayout();
            this.grbBaba.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // treeView1
            // 
            this.treeView1.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.treeView1.Location = new System.Drawing.Point(36, 115);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(229, 450);
            this.treeView1.TabIndex = 3;
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            // 
            // btnTumunuGetir
            // 
            this.btnTumunuGetir.Location = new System.Drawing.Point(36, 75);
            this.btnTumunuGetir.Name = "btnTumunuGetir";
            this.btnTumunuGetir.Size = new System.Drawing.Size(229, 28);
            this.btnTumunuGetir.TabIndex = 4;
            this.btnTumunuGetir.Text = "Tüm Öğrencileri Getir";
            this.btnTumunuGetir.UseVisualStyleBackColor = true;
            this.btnTumunuGetir.Click += new System.EventHandler(this.btnTumunuGetir_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.btnResimDuzenle);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.treeView1);
            this.groupBox1.Controls.Add(this.tabControl1);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.btnAra);
            this.groupBox1.Controls.Add(this.btnTumunuGetir);
            this.groupBox1.Location = new System.Drawing.Point(10, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(915, 575);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtKanGrubu);
            this.groupBox2.Controls.Add(this.txtNo);
            this.groupBox2.Controls.Add(this.btnDetayGetir);
            this.groupBox2.Controls.Add(this.txtDogumIlce);
            this.groupBox2.Controls.Add(this.txtDogumIL);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtDogumYil);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.txtSinif);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.txtRehberOgr);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtCinsiyet);
            this.groupBox2.Controls.Add(this.txtAd);
            this.groupBox2.Location = new System.Drawing.Point(288, 30);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(441, 237);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label4.Location = new System.Drawing.Point(327, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 16);
            this.label4.TabIndex = 10;
            this.label4.Text = "No";
            // 
            // txtKanGrubu
            // 
            this.txtKanGrubu.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtKanGrubu.Location = new System.Drawing.Point(86, 136);
            this.txtKanGrubu.Name = "txtKanGrubu";
            this.txtKanGrubu.ReadOnly = true;
            this.txtKanGrubu.Size = new System.Drawing.Size(77, 25);
            this.txtKanGrubu.TabIndex = 18;
            // 
            // txtNo
            // 
            this.txtNo.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtNo.Location = new System.Drawing.Point(359, 19);
            this.txtNo.Name = "txtNo";
            this.txtNo.ReadOnly = true;
            this.txtNo.Size = new System.Drawing.Size(76, 25);
            this.txtNo.TabIndex = 9;
            // 
            // btnDetayGetir
            // 
            this.btnDetayGetir.Enabled = false;
            this.btnDetayGetir.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnDetayGetir.Location = new System.Drawing.Point(25, 181);
            this.btnDetayGetir.Name = "btnDetayGetir";
            this.btnDetayGetir.Size = new System.Drawing.Size(116, 41);
            this.btnDetayGetir.TabIndex = 9;
            this.btnDetayGetir.Text = "Detaylı Bilgileri Göster";
            this.btnDetayGetir.UseVisualStyleBackColor = true;
            this.btnDetayGetir.Click += new System.EventHandler(this.btnDetayGetir_Click);
            // 
            // txtDogumIlce
            // 
            this.txtDogumIlce.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtDogumIlce.Location = new System.Drawing.Point(317, 95);
            this.txtDogumIlce.Name = "txtDogumIlce";
            this.txtDogumIlce.ReadOnly = true;
            this.txtDogumIlce.Size = new System.Drawing.Size(118, 25);
            this.txtDogumIlce.TabIndex = 17;
            // 
            // txtDogumIL
            // 
            this.txtDogumIL.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtDogumIL.Location = new System.Drawing.Point(86, 95);
            this.txtDogumIL.Name = "txtDogumIL";
            this.txtDogumIL.ReadOnly = true;
            this.txtDogumIL.Size = new System.Drawing.Size(118, 25);
            this.txtDogumIL.TabIndex = 16;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label9.Location = new System.Drawing.Point(264, 97);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(39, 16);
            this.label9.TabIndex = 15;
            this.label9.Text = "İlçesi";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label8.Location = new System.Drawing.Point(235, 136);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 16);
            this.label8.TabIndex = 10;
            this.label8.Text = "Doğum Yılı";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label7.Location = new System.Drawing.Point(6, 98);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 16);
            this.label7.TabIndex = 10;
            this.label7.Text = "Doğum Yeri";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label6.Location = new System.Drawing.Point(264, 58);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 16);
            this.label6.TabIndex = 10;
            this.label6.Text = "Cinsiyeti";
            // 
            // txtDogumYil
            // 
            this.txtDogumYil.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtDogumYil.Location = new System.Drawing.Point(317, 133);
            this.txtDogumYil.Name = "txtDogumYil";
            this.txtDogumYil.ReadOnly = true;
            this.txtDogumYil.Size = new System.Drawing.Size(118, 25);
            this.txtDogumYil.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label5.Location = new System.Drawing.Point(46, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 16);
            this.label5.TabIndex = 10;
            this.label5.Text = "Sınıfı";
            // 
            // txtSinif
            // 
            this.txtSinif.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtSinif.Location = new System.Drawing.Point(86, 55);
            this.txtSinif.Name = "txtSinif";
            this.txtSinif.ReadOnly = true;
            this.txtSinif.Size = new System.Drawing.Size(55, 25);
            this.txtSinif.TabIndex = 9;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label10.Location = new System.Drawing.Point(176, 177);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(95, 16);
            this.label10.TabIndex = 10;
            this.label10.Text = "Sınıf Reh. Öğrt.";
            // 
            // txtRehberOgr
            // 
            this.txtRehberOgr.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtRehberOgr.Location = new System.Drawing.Point(277, 174);
            this.txtRehberOgr.Name = "txtRehberOgr";
            this.txtRehberOgr.ReadOnly = true;
            this.txtRehberOgr.Size = new System.Drawing.Size(158, 25);
            this.txtRehberOgr.TabIndex = 9;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label11.Location = new System.Drawing.Point(10, 139);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(70, 16);
            this.label11.TabIndex = 10;
            this.label11.Text = "Kan Grubu";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label3.Location = new System.Drawing.Point(12, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 16);
            this.label3.TabIndex = 10;
            this.label3.Text = "Adı Soyadı";
            // 
            // txtCinsiyet
            // 
            this.txtCinsiyet.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtCinsiyet.Location = new System.Drawing.Point(328, 55);
            this.txtCinsiyet.Name = "txtCinsiyet";
            this.txtCinsiyet.ReadOnly = true;
            this.txtCinsiyet.Size = new System.Drawing.Size(107, 25);
            this.txtCinsiyet.TabIndex = 9;
            // 
            // txtAd
            // 
            this.txtAd.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtAd.Location = new System.Drawing.Point(86, 19);
            this.txtAd.Name = "txtAd";
            this.txtAd.ReadOnly = true;
            this.txtAd.Size = new System.Drawing.Size(236, 25);
            this.txtAd.TabIndex = 9;
            // 
            // btnResimDuzenle
            // 
            this.btnResimDuzenle.Location = new System.Drawing.Point(766, 211);
            this.btnResimDuzenle.Name = "btnResimDuzenle";
            this.btnResimDuzenle.Size = new System.Drawing.Size(96, 23);
            this.btnResimDuzenle.TabIndex = 9;
            this.btnResimDuzenle.Text = "Resim Düzenle";
            this.btnResimDuzenle.UseVisualStyleBackColor = true;
            this.btnResimDuzenle.Click += new System.EventHandler(this.btnResimDuzenle_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.Location = new System.Drawing.Point(28, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 16);
            this.label2.TabIndex = 8;
            this.label2.Text = "Öğrenci Adı";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.textBox1.Location = new System.Drawing.Point(110, 38);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(118, 22);
            this.textBox1.TabIndex = 7;
            this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.tabControl1.Location = new System.Drawing.Point(286, 273);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(623, 296);
            this.tabControl1.TabIndex = 6;
            this.tabControl1.Visible = false;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.Silver;
            this.tabPage3.Controls.Add(this.grbVeli);
            this.tabPage3.Controls.Add(this.grbAnne);
            this.tabPage3.Controls.Add(this.grbBaba);
            this.tabPage3.Controls.Add(this.btnAileKaydet);
            this.tabPage3.Location = new System.Drawing.Point(4, 24);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(615, 268);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "    Aile";
            // 
            // grbVeli
            // 
            this.grbVeli.Controls.Add(this.richTextAdres);
            this.grbVeli.Controls.Add(this.lblVeliAd);
            this.grbVeli.Controls.Add(this.txtVeliAdSoyad);
            this.grbVeli.Controls.Add(this.lblVeliAdres);
            this.grbVeli.Controls.Add(this.txtVeliMeslek);
            this.grbVeli.Controls.Add(this.lblVeliTel);
            this.grbVeli.Controls.Add(this.lblVeliNeslek);
            this.grbVeli.Controls.Add(this.lblVeliYakin);
            this.grbVeli.Controls.Add(this.txtVeliYakinligi);
            this.grbVeli.Controls.Add(this.txtVeliTel);
            this.grbVeli.Location = new System.Drawing.Point(3, 13);
            this.grbVeli.Name = "grbVeli";
            this.grbVeli.Size = new System.Drawing.Size(187, 242);
            this.grbVeli.TabIndex = 11;
            this.grbVeli.TabStop = false;
            this.grbVeli.Text = "VELİ";
            // 
            // richTextAdres
            // 
            this.richTextAdres.Location = new System.Drawing.Point(54, 167);
            this.richTextAdres.Name = "richTextAdres";
            this.richTextAdres.Size = new System.Drawing.Size(127, 69);
            this.richTextAdres.TabIndex = 2;
            this.richTextAdres.Text = "";
            // 
            // lblVeliAd
            // 
            this.lblVeliAd.AutoSize = true;
            this.lblVeliAd.Location = new System.Drawing.Point(11, 25);
            this.lblVeliAd.Name = "lblVeliAd";
            this.lblVeliAd.Size = new System.Drawing.Size(64, 15);
            this.lblVeliAd.TabIndex = 1;
            this.lblVeliAd.Text = "Adı Soyadı";
            // 
            // txtVeliAdSoyad
            // 
            this.txtVeliAdSoyad.Location = new System.Drawing.Point(75, 22);
            this.txtVeliAdSoyad.Name = "txtVeliAdSoyad";
            this.txtVeliAdSoyad.Size = new System.Drawing.Size(108, 23);
            this.txtVeliAdSoyad.TabIndex = 0;
            // 
            // lblVeliAdres
            // 
            this.lblVeliAdres.AutoSize = true;
            this.lblVeliAdres.Location = new System.Drawing.Point(-1, 190);
            this.lblVeliAdres.Name = "lblVeliAdres";
            this.lblVeliAdres.Size = new System.Drawing.Size(55, 15);
            this.lblVeliAdres.TabIndex = 1;
            this.lblVeliAdres.Text = "Ev Adresi";
            // 
            // txtVeliMeslek
            // 
            this.txtVeliMeslek.Location = new System.Drawing.Point(75, 96);
            this.txtVeliMeslek.Name = "txtVeliMeslek";
            this.txtVeliMeslek.Size = new System.Drawing.Size(108, 23);
            this.txtVeliMeslek.TabIndex = 0;
            // 
            // lblVeliTel
            // 
            this.lblVeliTel.AutoSize = true;
            this.lblVeliTel.Location = new System.Drawing.Point(11, 133);
            this.lblVeliTel.Name = "lblVeliTel";
            this.lblVeliTel.Size = new System.Drawing.Size(46, 15);
            this.lblVeliTel.TabIndex = 1;
            this.lblVeliTel.Text = "Telefon";
            // 
            // lblVeliNeslek
            // 
            this.lblVeliNeslek.AutoSize = true;
            this.lblVeliNeslek.Location = new System.Drawing.Point(11, 99);
            this.lblVeliNeslek.Name = "lblVeliNeslek";
            this.lblVeliNeslek.Size = new System.Drawing.Size(44, 15);
            this.lblVeliNeslek.TabIndex = 1;
            this.lblVeliNeslek.Text = "Meslek";
            // 
            // lblVeliYakin
            // 
            this.lblVeliYakin.AutoSize = true;
            this.lblVeliYakin.Location = new System.Drawing.Point(11, 59);
            this.lblVeliYakin.Name = "lblVeliYakin";
            this.lblVeliYakin.Size = new System.Drawing.Size(51, 15);
            this.lblVeliYakin.TabIndex = 1;
            this.lblVeliYakin.Text = "Yakınlığı";
            // 
            // txtVeliYakinligi
            // 
            this.txtVeliYakinligi.Location = new System.Drawing.Point(75, 56);
            this.txtVeliYakinligi.Name = "txtVeliYakinligi";
            this.txtVeliYakinligi.Size = new System.Drawing.Size(108, 23);
            this.txtVeliYakinligi.TabIndex = 0;
            // 
            // txtVeliTel
            // 
            this.txtVeliTel.Location = new System.Drawing.Point(75, 130);
            this.txtVeliTel.Name = "txtVeliTel";
            this.txtVeliTel.Size = new System.Drawing.Size(108, 23);
            this.txtVeliTel.TabIndex = 0;
            // 
            // grbAnne
            // 
            this.grbAnne.Controls.Add(this.cmbAnneSag);
            this.grbAnne.Controls.Add(this.cmbAnneOz);
            this.grbAnne.Controls.Add(this.cmbAnneEngel);
            this.grbAnne.Controls.Add(this.cmAnneBirlik);
            this.grbAnne.Controls.Add(this.lblAnneAd);
            this.grbAnne.Controls.Add(this.lblAnneEngel);
            this.grbAnne.Controls.Add(this.lblAnneBirlik);
            this.grbAnne.Controls.Add(this.lblAnneTel);
            this.grbAnne.Controls.Add(this.txtAnneAd);
            this.grbAnne.Controls.Add(this.txtAnneMeslek);
            this.grbAnne.Controls.Add(this.lblAnneMeslek);
            this.grbAnne.Controls.Add(this.lblAnneSag);
            this.grbAnne.Controls.Add(this.lblAnneOz);
            this.grbAnne.Controls.Add(this.txtAnneTel);
            this.grbAnne.Location = new System.Drawing.Point(400, 13);
            this.grbAnne.Name = "grbAnne";
            this.grbAnne.Size = new System.Drawing.Size(198, 218);
            this.grbAnne.TabIndex = 10;
            this.grbAnne.TabStop = false;
            this.grbAnne.Text = "ANNE";
            // 
            // cmbAnneSag
            // 
            this.cmbAnneSag.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAnneSag.FormattingEnabled = true;
            this.cmbAnneSag.Items.AddRange(new object[] {
            "SAĞ",
            "ÖLÜ"});
            this.cmbAnneSag.Location = new System.Drawing.Point(70, 78);
            this.cmbAnneSag.Name = "cmbAnneSag";
            this.cmbAnneSag.Size = new System.Drawing.Size(85, 23);
            this.cmbAnneSag.TabIndex = 2;
            // 
            // cmbAnneOz
            // 
            this.cmbAnneOz.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAnneOz.FormattingEnabled = true;
            this.cmbAnneOz.Items.AddRange(new object[] {
            "ÖZ",
            "ÜVEY"});
            this.cmbAnneOz.Location = new System.Drawing.Point(70, 47);
            this.cmbAnneOz.Name = "cmbAnneOz";
            this.cmbAnneOz.Size = new System.Drawing.Size(66, 23);
            this.cmbAnneOz.TabIndex = 2;
            // 
            // cmbAnneEngel
            // 
            this.cmbAnneEngel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAnneEngel.FormattingEnabled = true;
            this.cmbAnneEngel.Items.AddRange(new object[] {
            "YOK",
            "VAR"});
            this.cmbAnneEngel.Location = new System.Drawing.Point(96, 189);
            this.cmbAnneEngel.Name = "cmbAnneEngel";
            this.cmbAnneEngel.Size = new System.Drawing.Size(92, 23);
            this.cmbAnneEngel.TabIndex = 2;
            // 
            // cmAnneBirlik
            // 
            this.cmAnneBirlik.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmAnneBirlik.FormattingEnabled = true;
            this.cmAnneBirlik.Items.AddRange(new object[] {
            "BİRLİKTE",
            "AYRI"});
            this.cmAnneBirlik.Location = new System.Drawing.Point(96, 162);
            this.cmAnneBirlik.Name = "cmAnneBirlik";
            this.cmAnneBirlik.Size = new System.Drawing.Size(92, 23);
            this.cmAnneBirlik.TabIndex = 2;
            // 
            // lblAnneAd
            // 
            this.lblAnneAd.AutoSize = true;
            this.lblAnneAd.Location = new System.Drawing.Point(7, 22);
            this.lblAnneAd.Name = "lblAnneAd";
            this.lblAnneAd.Size = new System.Drawing.Size(64, 15);
            this.lblAnneAd.TabIndex = 1;
            this.lblAnneAd.Text = "Adı Soyadı";
            // 
            // lblAnneEngel
            // 
            this.lblAnneEngel.AutoSize = true;
            this.lblAnneEngel.Location = new System.Drawing.Point(7, 192);
            this.lblAnneEngel.Name = "lblAnneEngel";
            this.lblAnneEngel.Size = new System.Drawing.Size(90, 15);
            this.lblAnneEngel.TabIndex = 1;
            this.lblAnneEngel.Text = "Engelli Durumu";
            // 
            // lblAnneBirlik
            // 
            this.lblAnneBirlik.AutoSize = true;
            this.lblAnneBirlik.Location = new System.Drawing.Point(21, 165);
            this.lblAnneBirlik.Name = "lblAnneBirlik";
            this.lblAnneBirlik.Size = new System.Drawing.Size(69, 15);
            this.lblAnneBirlik.TabIndex = 1;
            this.lblAnneBirlik.Text = "Birlikte/Ayrı";
            // 
            // lblAnneTel
            // 
            this.lblAnneTel.AutoSize = true;
            this.lblAnneTel.Location = new System.Drawing.Point(7, 137);
            this.lblAnneTel.Name = "lblAnneTel";
            this.lblAnneTel.Size = new System.Drawing.Size(46, 15);
            this.lblAnneTel.TabIndex = 1;
            this.lblAnneTel.Text = "Telefon";
            // 
            // txtAnneAd
            // 
            this.txtAnneAd.Location = new System.Drawing.Point(72, 19);
            this.txtAnneAd.Name = "txtAnneAd";
            this.txtAnneAd.Size = new System.Drawing.Size(121, 23);
            this.txtAnneAd.TabIndex = 0;
            // 
            // txtAnneMeslek
            // 
            this.txtAnneMeslek.Location = new System.Drawing.Point(60, 105);
            this.txtAnneMeslek.Name = "txtAnneMeslek";
            this.txtAnneMeslek.Size = new System.Drawing.Size(128, 23);
            this.txtAnneMeslek.TabIndex = 0;
            // 
            // lblAnneMeslek
            // 
            this.lblAnneMeslek.AutoSize = true;
            this.lblAnneMeslek.Location = new System.Drawing.Point(7, 108);
            this.lblAnneMeslek.Name = "lblAnneMeslek";
            this.lblAnneMeslek.Size = new System.Drawing.Size(44, 15);
            this.lblAnneMeslek.TabIndex = 1;
            this.lblAnneMeslek.Text = "Meslek";
            // 
            // lblAnneSag
            // 
            this.lblAnneSag.AutoSize = true;
            this.lblAnneSag.Location = new System.Drawing.Point(9, 81);
            this.lblAnneSag.Name = "lblAnneSag";
            this.lblAnneSag.Size = new System.Drawing.Size(51, 15);
            this.lblAnneSag.TabIndex = 1;
            this.lblAnneSag.Text = "Sağ/Ölü";
            // 
            // lblAnneOz
            // 
            this.lblAnneOz.AutoSize = true;
            this.lblAnneOz.Location = new System.Drawing.Point(9, 50);
            this.lblAnneOz.Name = "lblAnneOz";
            this.lblAnneOz.Size = new System.Drawing.Size(53, 15);
            this.lblAnneOz.TabIndex = 1;
            this.lblAnneOz.Text = "Öz/Üvey";
            // 
            // txtAnneTel
            // 
            this.txtAnneTel.Location = new System.Drawing.Point(60, 134);
            this.txtAnneTel.Name = "txtAnneTel";
            this.txtAnneTel.Size = new System.Drawing.Size(128, 23);
            this.txtAnneTel.TabIndex = 0;
            // 
            // grbBaba
            // 
            this.grbBaba.Controls.Add(this.cmbSagOlu);
            this.grbBaba.Controls.Add(this.cmbOzUvey);
            this.grbBaba.Controls.Add(this.cmbBabaEngel);
            this.grbBaba.Controls.Add(this.cmbBirlik);
            this.grbBaba.Controls.Add(this.lblBabaAd);
            this.grbBaba.Controls.Add(this.lblBabaEngel);
            this.grbBaba.Controls.Add(this.lblBirliktelik);
            this.grbBaba.Controls.Add(this.lblBabaTel);
            this.grbBaba.Controls.Add(this.txtBabaAd);
            this.grbBaba.Controls.Add(this.txtBabaMeslek);
            this.grbBaba.Controls.Add(this.lblBabaMes);
            this.grbBaba.Controls.Add(this.lblBabaSag);
            this.grbBaba.Controls.Add(this.lblBabaOz);
            this.grbBaba.Controls.Add(this.txtBabaTel);
            this.grbBaba.Location = new System.Drawing.Point(196, 13);
            this.grbBaba.Name = "grbBaba";
            this.grbBaba.Size = new System.Drawing.Size(198, 218);
            this.grbBaba.TabIndex = 10;
            this.grbBaba.TabStop = false;
            this.grbBaba.Text = "BABA";
            // 
            // cmbSagOlu
            // 
            this.cmbSagOlu.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSagOlu.FormattingEnabled = true;
            this.cmbSagOlu.Items.AddRange(new object[] {
            "SAĞ",
            "ÖLÜ"});
            this.cmbSagOlu.Location = new System.Drawing.Point(70, 77);
            this.cmbSagOlu.Name = "cmbSagOlu";
            this.cmbSagOlu.Size = new System.Drawing.Size(85, 23);
            this.cmbSagOlu.TabIndex = 2;
            // 
            // cmbOzUvey
            // 
            this.cmbOzUvey.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOzUvey.FormattingEnabled = true;
            this.cmbOzUvey.Items.AddRange(new object[] {
            "ÖZ",
            "ÜVEY"});
            this.cmbOzUvey.Location = new System.Drawing.Point(70, 46);
            this.cmbOzUvey.Name = "cmbOzUvey";
            this.cmbOzUvey.Size = new System.Drawing.Size(66, 23);
            this.cmbOzUvey.TabIndex = 2;
            // 
            // cmbBabaEngel
            // 
            this.cmbBabaEngel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBabaEngel.FormattingEnabled = true;
            this.cmbBabaEngel.Items.AddRange(new object[] {
            "YOK",
            "VAR"});
            this.cmbBabaEngel.Location = new System.Drawing.Point(95, 189);
            this.cmbBabaEngel.Name = "cmbBabaEngel";
            this.cmbBabaEngel.Size = new System.Drawing.Size(92, 23);
            this.cmbBabaEngel.TabIndex = 2;
            // 
            // cmbBirlik
            // 
            this.cmbBirlik.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBirlik.FormattingEnabled = true;
            this.cmbBirlik.Items.AddRange(new object[] {
            "BİRLİKTE",
            "AYRI"});
            this.cmbBirlik.Location = new System.Drawing.Point(95, 161);
            this.cmbBirlik.Name = "cmbBirlik";
            this.cmbBirlik.Size = new System.Drawing.Size(92, 23);
            this.cmbBirlik.TabIndex = 2;
            // 
            // lblBabaAd
            // 
            this.lblBabaAd.AutoSize = true;
            this.lblBabaAd.Location = new System.Drawing.Point(7, 22);
            this.lblBabaAd.Name = "lblBabaAd";
            this.lblBabaAd.Size = new System.Drawing.Size(64, 15);
            this.lblBabaAd.TabIndex = 1;
            this.lblBabaAd.Text = "Adı Soyadı";
            // 
            // lblBabaEngel
            // 
            this.lblBabaEngel.AutoSize = true;
            this.lblBabaEngel.Location = new System.Drawing.Point(7, 192);
            this.lblBabaEngel.Name = "lblBabaEngel";
            this.lblBabaEngel.Size = new System.Drawing.Size(90, 15);
            this.lblBabaEngel.TabIndex = 1;
            this.lblBabaEngel.Text = "Engelli Durumu";
            // 
            // lblBirliktelik
            // 
            this.lblBirliktelik.AutoSize = true;
            this.lblBirliktelik.Location = new System.Drawing.Point(20, 164);
            this.lblBirliktelik.Name = "lblBirliktelik";
            this.lblBirliktelik.Size = new System.Drawing.Size(69, 15);
            this.lblBirliktelik.TabIndex = 1;
            this.lblBirliktelik.Text = "Birlikte/Ayrı";
            // 
            // lblBabaTel
            // 
            this.lblBabaTel.AutoSize = true;
            this.lblBabaTel.Location = new System.Drawing.Point(7, 136);
            this.lblBabaTel.Name = "lblBabaTel";
            this.lblBabaTel.Size = new System.Drawing.Size(46, 15);
            this.lblBabaTel.TabIndex = 1;
            this.lblBabaTel.Text = "Telefon";
            // 
            // txtBabaAd
            // 
            this.txtBabaAd.Location = new System.Drawing.Point(72, 19);
            this.txtBabaAd.Name = "txtBabaAd";
            this.txtBabaAd.Size = new System.Drawing.Size(121, 23);
            this.txtBabaAd.TabIndex = 0;
            // 
            // txtBabaMeslek
            // 
            this.txtBabaMeslek.Location = new System.Drawing.Point(60, 104);
            this.txtBabaMeslek.Name = "txtBabaMeslek";
            this.txtBabaMeslek.Size = new System.Drawing.Size(128, 23);
            this.txtBabaMeslek.TabIndex = 0;
            // 
            // lblBabaMes
            // 
            this.lblBabaMes.AutoSize = true;
            this.lblBabaMes.Location = new System.Drawing.Point(7, 107);
            this.lblBabaMes.Name = "lblBabaMes";
            this.lblBabaMes.Size = new System.Drawing.Size(44, 15);
            this.lblBabaMes.TabIndex = 1;
            this.lblBabaMes.Text = "Meslek";
            // 
            // lblBabaSag
            // 
            this.lblBabaSag.AutoSize = true;
            this.lblBabaSag.Location = new System.Drawing.Point(9, 80);
            this.lblBabaSag.Name = "lblBabaSag";
            this.lblBabaSag.Size = new System.Drawing.Size(51, 15);
            this.lblBabaSag.TabIndex = 1;
            this.lblBabaSag.Text = "Sağ/Ölü";
            // 
            // lblBabaOz
            // 
            this.lblBabaOz.AutoSize = true;
            this.lblBabaOz.Location = new System.Drawing.Point(7, 52);
            this.lblBabaOz.Name = "lblBabaOz";
            this.lblBabaOz.Size = new System.Drawing.Size(53, 15);
            this.lblBabaOz.TabIndex = 1;
            this.lblBabaOz.Text = "Öz/Üvey";
            // 
            // txtBabaTel
            // 
            this.txtBabaTel.Location = new System.Drawing.Point(60, 133);
            this.txtBabaTel.Name = "txtBabaTel";
            this.txtBabaTel.Size = new System.Drawing.Size(128, 23);
            this.txtBabaTel.TabIndex = 0;
            // 
            // btnAileKaydet
            // 
            this.btnAileKaydet.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnAileKaydet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAileKaydet.Location = new System.Drawing.Point(519, 231);
            this.btnAileKaydet.Name = "btnAileKaydet";
            this.btnAileKaydet.Size = new System.Drawing.Size(69, 31);
            this.btnAileKaydet.TabIndex = 9;
            this.btnAileKaydet.Text = "Kaydet";
            this.btnAileKaydet.UseVisualStyleBackColor = true;
            this.btnAileKaydet.Click += new System.EventHandler(this.btnAileKaydet_Click);
            // 
            // tabPage6
            // 
            this.tabPage6.BackColor = System.Drawing.Color.Silver;
            this.tabPage6.Controls.Add(this.lblAileCiddi);
            this.tabPage6.Controls.Add(this.btnGenelKaydet);
            this.tabPage6.Controls.Add(this.txtMeslekAlan);
            this.tabPage6.Controls.Add(this.txtOncekiOkul);
            this.tabPage6.Controls.Add(this.richOkulRahatsiz);
            this.tabPage6.Controls.Add(this.cmbOda);
            this.tabPage6.Controls.Add(this.cmbDestek);
            this.tabPage6.Controls.Add(this.cmbMisafir);
            this.tabPage6.Controls.Add(this.cmbYetenek);
            this.tabPage6.Controls.Add(this.cmbSinifTekrari);
            this.tabPage6.Controls.Add(this.cmbEvIsinma);
            this.tabPage6.Controls.Add(this.cmbEv);
            this.tabPage6.Controls.Add(this.cmbAileCiddi);
            this.tabPage6.Controls.Add(this.cmbOgrenciCiddi);
            this.tabPage6.Controls.Add(this.lblYonelim);
            this.tabPage6.Controls.Add(this.lblOncekiOkul);
            this.tabPage6.Controls.Add(this.cmbAileProblem);
            this.tabPage6.Controls.Add(this.lblOkulRahatsiz);
            this.tabPage6.Controls.Add(this.lblOda);
            this.tabPage6.Controls.Add(this.lblOkulDisiDestek);
            this.tabPage6.Controls.Add(this.lblMisafir);
            this.tabPage6.Controls.Add(this.lblYetenek);
            this.tabPage6.Controls.Add(this.cmbZararli);
            this.tabPage6.Controls.Add(this.lblSinifTekrari);
            this.tabPage6.Controls.Add(this.lblEvIsinma);
            this.tabPage6.Controls.Add(this.lblEv);
            this.tabPage6.Controls.Add(this.lblOgrenciCiddi);
            this.tabPage6.Controls.Add(this.lblAileProblem);
            this.tabPage6.Controls.Add(this.lblZararlı);
            this.tabPage6.Location = new System.Drawing.Point(4, 24);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(615, 268);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Genel Durumu";
            // 
            // lblAileCiddi
            // 
            this.lblAileCiddi.AutoSize = true;
            this.lblAileCiddi.Location = new System.Drawing.Point(11, 199);
            this.lblAileCiddi.Name = "lblAileCiddi";
            this.lblAileCiddi.Size = new System.Drawing.Size(116, 15);
            this.lblAileCiddi.TabIndex = 0;
            this.lblAileCiddi.Text = "Aile Ciddi Rahatsızlık";
            // 
            // btnGenelKaydet
            // 
            this.btnGenelKaydet.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnGenelKaydet.Image = global::rbsProje.Properties.Resources.diskette;
            this.btnGenelKaydet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGenelKaydet.Location = new System.Drawing.Point(476, 28);
            this.btnGenelKaydet.Name = "btnGenelKaydet";
            this.btnGenelKaydet.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.btnGenelKaydet.Size = new System.Drawing.Size(90, 48);
            this.btnGenelKaydet.TabIndex = 10;
            this.btnGenelKaydet.Text = "         Kaydet";
            this.btnGenelKaydet.UseVisualStyleBackColor = true;
            this.btnGenelKaydet.Click += new System.EventHandler(this.btnGenelKaydet_Click);
            // 
            // txtMeslekAlan
            // 
            this.txtMeslekAlan.Location = new System.Drawing.Point(349, 129);
            this.txtMeslekAlan.Name = "txtMeslekAlan";
            this.txtMeslekAlan.Size = new System.Drawing.Size(147, 23);
            this.txtMeslekAlan.TabIndex = 3;
            // 
            // txtOncekiOkul
            // 
            this.txtOncekiOkul.Location = new System.Drawing.Point(349, 101);
            this.txtOncekiOkul.Name = "txtOncekiOkul";
            this.txtOncekiOkul.Size = new System.Drawing.Size(147, 23);
            this.txtOncekiOkul.TabIndex = 3;
            // 
            // richOkulRahatsiz
            // 
            this.richOkulRahatsiz.Location = new System.Drawing.Point(389, 164);
            this.richOkulRahatsiz.Name = "richOkulRahatsiz";
            this.richOkulRahatsiz.Size = new System.Drawing.Size(199, 60);
            this.richOkulRahatsiz.TabIndex = 2;
            this.richOkulRahatsiz.Text = "";
            // 
            // cmbOda
            // 
            this.cmbOda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOda.FormattingEnabled = true;
            this.cmbOda.Items.AddRange(new object[] {
            "YOK",
            "VAR"});
            this.cmbOda.Location = new System.Drawing.Point(106, 161);
            this.cmbOda.Name = "cmbOda";
            this.cmbOda.Size = new System.Drawing.Size(96, 23);
            this.cmbOda.TabIndex = 1;
            // 
            // cmbDestek
            // 
            this.cmbDestek.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDestek.FormattingEnabled = true;
            this.cmbDestek.Items.AddRange(new object[] {
            "YOK",
            "VAR"});
            this.cmbDestek.Location = new System.Drawing.Point(352, 43);
            this.cmbDestek.Name = "cmbDestek";
            this.cmbDestek.Size = new System.Drawing.Size(56, 23);
            this.cmbDestek.TabIndex = 1;
            // 
            // cmbMisafir
            // 
            this.cmbMisafir.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMisafir.FormattingEnabled = true;
            this.cmbMisafir.Items.AddRange(new object[] {
            "YOK",
            "VAR"});
            this.cmbMisafir.Location = new System.Drawing.Point(352, 72);
            this.cmbMisafir.Name = "cmbMisafir";
            this.cmbMisafir.Size = new System.Drawing.Size(56, 23);
            this.cmbMisafir.TabIndex = 1;
            // 
            // cmbYetenek
            // 
            this.cmbYetenek.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbYetenek.FormattingEnabled = true;
            this.cmbYetenek.Items.AddRange(new object[] {
            "YOK",
            "VAR"});
            this.cmbYetenek.Location = new System.Drawing.Point(352, 11);
            this.cmbYetenek.Name = "cmbYetenek";
            this.cmbYetenek.Size = new System.Drawing.Size(56, 23);
            this.cmbYetenek.TabIndex = 1;
            // 
            // cmbSinifTekrari
            // 
            this.cmbSinifTekrari.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSinifTekrari.FormattingEnabled = true;
            this.cmbSinifTekrari.Items.AddRange(new object[] {
            "YOK",
            "VAR"});
            this.cmbSinifTekrari.Location = new System.Drawing.Point(106, 132);
            this.cmbSinifTekrari.Name = "cmbSinifTekrari";
            this.cmbSinifTekrari.Size = new System.Drawing.Size(96, 23);
            this.cmbSinifTekrari.TabIndex = 1;
            // 
            // cmbEvIsinma
            // 
            this.cmbEvIsinma.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEvIsinma.FormattingEnabled = true;
            this.cmbEvIsinma.Items.AddRange(new object[] {
            "Kalorifer",
            "Soba"});
            this.cmbEvIsinma.Location = new System.Drawing.Point(106, 99);
            this.cmbEvIsinma.Name = "cmbEvIsinma";
            this.cmbEvIsinma.Size = new System.Drawing.Size(96, 23);
            this.cmbEvIsinma.TabIndex = 1;
            // 
            // cmbEv
            // 
            this.cmbEv.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEv.FormattingEnabled = true;
            this.cmbEv.Items.AddRange(new object[] {
            "Ev Sahibi",
            "Kira"});
            this.cmbEv.Location = new System.Drawing.Point(106, 68);
            this.cmbEv.Name = "cmbEv";
            this.cmbEv.Size = new System.Drawing.Size(96, 23);
            this.cmbEv.TabIndex = 1;
            // 
            // cmbAileCiddi
            // 
            this.cmbAileCiddi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAileCiddi.FormattingEnabled = true;
            this.cmbAileCiddi.Items.AddRange(new object[] {
            "YOK",
            "VAR"});
            this.cmbAileCiddi.Location = new System.Drawing.Point(146, 196);
            this.cmbAileCiddi.Name = "cmbAileCiddi";
            this.cmbAileCiddi.Size = new System.Drawing.Size(56, 23);
            this.cmbAileCiddi.TabIndex = 1;
            // 
            // cmbOgrenciCiddi
            // 
            this.cmbOgrenciCiddi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOgrenciCiddi.FormattingEnabled = true;
            this.cmbOgrenciCiddi.Items.AddRange(new object[] {
            "YOK",
            "VAR"});
            this.cmbOgrenciCiddi.Location = new System.Drawing.Point(176, 225);
            this.cmbOgrenciCiddi.Name = "cmbOgrenciCiddi";
            this.cmbOgrenciCiddi.Size = new System.Drawing.Size(54, 23);
            this.cmbOgrenciCiddi.TabIndex = 1;
            // 
            // lblYonelim
            // 
            this.lblYonelim.AutoSize = true;
            this.lblYonelim.Location = new System.Drawing.Point(210, 135);
            this.lblYonelim.Name = "lblYonelim";
            this.lblYonelim.Size = new System.Drawing.Size(131, 15);
            this.lblYonelim.TabIndex = 0;
            this.lblYonelim.Text = "Yöneleceği meslek/alan";
            // 
            // lblOncekiOkul
            // 
            this.lblOncekiOkul.AutoSize = true;
            this.lblOncekiOkul.Location = new System.Drawing.Point(210, 107);
            this.lblOncekiOkul.Name = "lblOncekiOkul";
            this.lblOncekiOkul.Size = new System.Drawing.Size(72, 15);
            this.lblOncekiOkul.TabIndex = 0;
            this.lblOncekiOkul.Text = "Önceki Okul";
            // 
            // cmbAileProblem
            // 
            this.cmbAileProblem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAileProblem.FormattingEnabled = true;
            this.cmbAileProblem.Items.AddRange(new object[] {
            "YOK",
            "VAR"});
            this.cmbAileProblem.Location = new System.Drawing.Point(125, 40);
            this.cmbAileProblem.Name = "cmbAileProblem";
            this.cmbAileProblem.Size = new System.Drawing.Size(79, 23);
            this.cmbAileProblem.TabIndex = 1;
            // 
            // lblOkulRahatsiz
            // 
            this.lblOkulRahatsiz.AutoSize = true;
            this.lblOkulRahatsiz.Location = new System.Drawing.Point(210, 184);
            this.lblOkulRahatsiz.Name = "lblOkulRahatsiz";
            this.lblOkulRahatsiz.Size = new System.Drawing.Size(173, 15);
            this.lblOkulRahatsiz.TabIndex = 0;
            this.lblOkulRahatsiz.Text = "Okulda Rahatsız eden durumlar";
            // 
            // lblOda
            // 
            this.lblOda.AutoSize = true;
            this.lblOda.Location = new System.Drawing.Point(11, 164);
            this.lblOda.Name = "lblOda";
            this.lblOda.Size = new System.Drawing.Size(79, 15);
            this.lblOda.TabIndex = 0;
            this.lblOda.Text = "Çalışma odası";
            // 
            // lblOkulDisiDestek
            // 
            this.lblOkulDisiDestek.AutoSize = true;
            this.lblOkulDisiDestek.Location = new System.Drawing.Point(210, 48);
            this.lblOkulDisiDestek.Name = "lblOkulDisiDestek";
            this.lblOkulDisiDestek.Size = new System.Drawing.Size(110, 15);
            this.lblOkulDisiDestek.TabIndex = 0;
            this.lblOkulDisiDestek.Text = "Okul dışında destek";
            // 
            // lblMisafir
            // 
            this.lblMisafir.AutoSize = true;
            this.lblMisafir.Location = new System.Drawing.Point(210, 75);
            this.lblMisafir.Name = "lblMisafir";
            this.lblMisafir.Size = new System.Drawing.Size(139, 15);
            this.lblMisafir.TabIndex = 0;
            this.lblMisafir.Text = "Evde aile dışında yaşayan";
            // 
            // lblYetenek
            // 
            this.lblYetenek.AutoSize = true;
            this.lblYetenek.Location = new System.Drawing.Point(210, 17);
            this.lblYetenek.Name = "lblYetenek";
            this.lblYetenek.Size = new System.Drawing.Size(48, 15);
            this.lblYetenek.TabIndex = 0;
            this.lblYetenek.Text = "Yetenek";
            // 
            // cmbZararli
            // 
            this.cmbZararli.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbZararli.FormattingEnabled = true;
            this.cmbZararli.Items.AddRange(new object[] {
            "YOK",
            "VAR"});
            this.cmbZararli.Location = new System.Drawing.Point(125, 11);
            this.cmbZararli.Name = "cmbZararli";
            this.cmbZararli.Size = new System.Drawing.Size(79, 23);
            this.cmbZararli.TabIndex = 1;
            // 
            // lblSinifTekrari
            // 
            this.lblSinifTekrari.AutoSize = true;
            this.lblSinifTekrari.Location = new System.Drawing.Point(11, 135);
            this.lblSinifTekrari.Name = "lblSinifTekrari";
            this.lblSinifTekrari.Size = new System.Drawing.Size(69, 15);
            this.lblSinifTekrari.TabIndex = 0;
            this.lblSinifTekrari.Text = "Sınıf Tekrarı";
            // 
            // lblEvIsinma
            // 
            this.lblEvIsinma.AutoSize = true;
            this.lblEvIsinma.Location = new System.Drawing.Point(11, 102);
            this.lblEvIsinma.Name = "lblEvIsinma";
            this.lblEvIsinma.Size = new System.Drawing.Size(91, 15);
            this.lblEvIsinma.TabIndex = 0;
            this.lblEvIsinma.Text = "Isınma Durumu";
            // 
            // lblEv
            // 
            this.lblEv.AutoSize = true;
            this.lblEv.Location = new System.Drawing.Point(11, 76);
            this.lblEv.Name = "lblEv";
            this.lblEv.Size = new System.Drawing.Size(65, 15);
            this.lblEv.TabIndex = 0;
            this.lblEv.Text = "Yaşadığı Ev";
            // 
            // lblOgrenciCiddi
            // 
            this.lblOgrenciCiddi.AutoSize = true;
            this.lblOgrenciCiddi.Location = new System.Drawing.Point(11, 231);
            this.lblOgrenciCiddi.Name = "lblOgrenciCiddi";
            this.lblOgrenciCiddi.Size = new System.Drawing.Size(159, 15);
            this.lblOgrenciCiddi.TabIndex = 0;
            this.lblOgrenciCiddi.Text = "Öğrencinin Ciddi Rahatsızlığı";
            // 
            // lblAileProblem
            // 
            this.lblAileProblem.AutoSize = true;
            this.lblAileProblem.Location = new System.Drawing.Point(11, 45);
            this.lblAileProblem.Name = "lblAileProblem";
            this.lblAileProblem.Size = new System.Drawing.Size(91, 15);
            this.lblAileProblem.TabIndex = 0;
            this.lblAileProblem.Text = "Aile Problemleri";
            // 
            // lblZararlı
            // 
            this.lblZararlı.AutoSize = true;
            this.lblZararlı.Location = new System.Drawing.Point(11, 14);
            this.lblZararlı.Name = "lblZararlı";
            this.lblZararlı.Size = new System.Drawing.Size(109, 15);
            this.lblZararlı.TabIndex = 0;
            this.lblZararlı.Text = "Zararlı Alışkanlıkları";
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.Silver;
            this.tabPage4.Controls.Add(this.numericUpDown2);
            this.tabPage4.Controls.Add(this.numericUpDown1);
            this.tabPage4.Controls.Add(this.btnSaglikKaydet);
            this.tabPage4.Controls.Add(this.cmbProtez);
            this.tabPage4.Controls.Add(this.cmbKaza);
            this.tabPage4.Controls.Add(this.cmbEngel);
            this.tabPage4.Controls.Add(this.lblProtez);
            this.tabPage4.Controls.Add(this.lblKaza);
            this.tabPage4.Controls.Add(this.lblEngel);
            this.tabPage4.Controls.Add(this.lblKilo);
            this.tabPage4.Controls.Add(this.lblBoy);
            this.tabPage4.Location = new System.Drawing.Point(4, 24);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(615, 268);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Sağlık Durumu";
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Location = new System.Drawing.Point(111, 89);
            this.numericUpDown2.Maximum = new decimal(new int[] {
            250,
            0,
            0,
            0});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(71, 23);
            this.numericUpDown2.TabIndex = 11;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(111, 50);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            250,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(71, 23);
            this.numericUpDown1.TabIndex = 11;
            // 
            // btnSaglikKaydet
            // 
            this.btnSaglikKaydet.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnSaglikKaydet.Image = global::rbsProje.Properties.Resources.kaydet2;
            this.btnSaglikKaydet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSaglikKaydet.Location = new System.Drawing.Point(177, 170);
            this.btnSaglikKaydet.Name = "btnSaglikKaydet";
            this.btnSaglikKaydet.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.btnSaglikKaydet.Size = new System.Drawing.Size(109, 48);
            this.btnSaglikKaydet.TabIndex = 10;
            this.btnSaglikKaydet.Text = "           Kaydet";
            this.btnSaglikKaydet.UseVisualStyleBackColor = true;
            this.btnSaglikKaydet.Click += new System.EventHandler(this.btnSaglikKaydet_Click);
            // 
            // cmbProtez
            // 
            this.cmbProtez.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProtez.FormattingEnabled = true;
            this.cmbProtez.Items.AddRange(new object[] {
            "YOK",
            "VAR"});
            this.cmbProtez.Location = new System.Drawing.Point(366, 121);
            this.cmbProtez.Name = "cmbProtez";
            this.cmbProtez.Size = new System.Drawing.Size(65, 23);
            this.cmbProtez.TabIndex = 2;
            // 
            // cmbKaza
            // 
            this.cmbKaza.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbKaza.FormattingEnabled = true;
            this.cmbKaza.Items.AddRange(new object[] {
            "YOK",
            "VAR"});
            this.cmbKaza.Location = new System.Drawing.Point(366, 85);
            this.cmbKaza.Name = "cmbKaza";
            this.cmbKaza.Size = new System.Drawing.Size(65, 23);
            this.cmbKaza.TabIndex = 2;
            // 
            // cmbEngel
            // 
            this.cmbEngel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEngel.FormattingEnabled = true;
            this.cmbEngel.Items.AddRange(new object[] {
            "YOK",
            "VAR"});
            this.cmbEngel.Location = new System.Drawing.Point(366, 49);
            this.cmbEngel.Name = "cmbEngel";
            this.cmbEngel.Size = new System.Drawing.Size(65, 23);
            this.cmbEngel.TabIndex = 2;
            // 
            // lblProtez
            // 
            this.lblProtez.AutoSize = true;
            this.lblProtez.Location = new System.Drawing.Point(223, 124);
            this.lblProtez.Name = "lblProtez";
            this.lblProtez.Size = new System.Drawing.Size(131, 15);
            this.lblProtez.TabIndex = 0;
            this.lblProtez.Text = "Kullandığı Cihaz/Protez";
            // 
            // lblKaza
            // 
            this.lblKaza.AutoSize = true;
            this.lblKaza.Location = new System.Drawing.Point(223, 88);
            this.lblKaza.Name = "lblKaza";
            this.lblKaza.Size = new System.Drawing.Size(134, 15);
            this.lblKaza.TabIndex = 0;
            this.lblKaza.Text = "Geçirdiği Kaza/Ameliyat";
            // 
            // lblEngel
            // 
            this.lblEngel.AutoSize = true;
            this.lblEngel.Location = new System.Drawing.Point(223, 52);
            this.lblEngel.Name = "lblEngel";
            this.lblEngel.Size = new System.Drawing.Size(108, 15);
            this.lblEngel.TabIndex = 0;
            this.lblEngel.Text = "Herhangi bir engeli";
            // 
            // lblKilo
            // 
            this.lblKilo.AutoSize = true;
            this.lblKilo.Location = new System.Drawing.Point(78, 91);
            this.lblKilo.Name = "lblKilo";
            this.lblKilo.Size = new System.Drawing.Size(27, 15);
            this.lblKilo.TabIndex = 0;
            this.lblKilo.Text = "Kilo";
            // 
            // lblBoy
            // 
            this.lblBoy.AutoSize = true;
            this.lblBoy.Location = new System.Drawing.Point(78, 52);
            this.lblBoy.Name = "lblBoy";
            this.lblBoy.Size = new System.Drawing.Size(27, 15);
            this.lblBoy.TabIndex = 0;
            this.lblBoy.Text = "Boy";
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.Color.Silver;
            this.tabPage5.Controls.Add(this.btnOgrDurumuKaydet);
            this.tabPage5.Controls.Add(this.richKulup);
            this.tabPage5.Controls.Add(this.richBosZaman);
            this.tabPage5.Controls.Add(this.txtBasarisizDers);
            this.tabPage5.Controls.Add(this.txtBasariliDers);
            this.tabPage5.Controls.Add(this.txtSinifRehber);
            this.tabPage5.Controls.Add(this.lblKulup);
            this.tabPage5.Controls.Add(this.lblBosZaman);
            this.tabPage5.Controls.Add(this.label27);
            this.tabPage5.Controls.Add(this.label12);
            this.tabPage5.Controls.Add(this.lblSinifRehber);
            this.tabPage5.Location = new System.Drawing.Point(4, 24);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(615, 268);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Öğrenim Durumu";
            // 
            // btnOgrDurumuKaydet
            // 
            this.btnOgrDurumuKaydet.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnOgrDurumuKaydet.Image = global::rbsProje.Properties.Resources.floppy_disk;
            this.btnOgrDurumuKaydet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOgrDurumuKaydet.Location = new System.Drawing.Point(435, 154);
            this.btnOgrDurumuKaydet.Name = "btnOgrDurumuKaydet";
            this.btnOgrDurumuKaydet.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.btnOgrDurumuKaydet.Size = new System.Drawing.Size(109, 48);
            this.btnOgrDurumuKaydet.TabIndex = 11;
            this.btnOgrDurumuKaydet.Text = "           Kaydet";
            this.btnOgrDurumuKaydet.UseVisualStyleBackColor = true;
            this.btnOgrDurumuKaydet.Click += new System.EventHandler(this.btnOgrDurumuKaydet_Click);
            // 
            // richKulup
            // 
            this.richKulup.Location = new System.Drawing.Point(341, 35);
            this.richKulup.Name = "richKulup";
            this.richKulup.Size = new System.Drawing.Size(203, 84);
            this.richKulup.TabIndex = 2;
            this.richKulup.Text = "";
            // 
            // richBosZaman
            // 
            this.richBosZaman.Location = new System.Drawing.Point(191, 147);
            this.richBosZaman.Name = "richBosZaman";
            this.richBosZaman.Size = new System.Drawing.Size(210, 96);
            this.richBosZaman.TabIndex = 2;
            this.richBosZaman.Text = "";
            // 
            // txtBasarisizDers
            // 
            this.txtBasarisizDers.Location = new System.Drawing.Point(164, 97);
            this.txtBasarisizDers.Name = "txtBasarisizDers";
            this.txtBasarisizDers.Size = new System.Drawing.Size(120, 23);
            this.txtBasarisizDers.TabIndex = 1;
            // 
            // txtBasariliDers
            // 
            this.txtBasariliDers.Location = new System.Drawing.Point(164, 59);
            this.txtBasariliDers.Name = "txtBasariliDers";
            this.txtBasariliDers.Size = new System.Drawing.Size(120, 23);
            this.txtBasariliDers.TabIndex = 1;
            // 
            // txtSinifRehber
            // 
            this.txtSinifRehber.Location = new System.Drawing.Point(164, 23);
            this.txtSinifRehber.Name = "txtSinifRehber";
            this.txtSinifRehber.Size = new System.Drawing.Size(120, 23);
            this.txtSinifRehber.TabIndex = 1;
            // 
            // lblKulup
            // 
            this.lblKulup.AutoSize = true;
            this.lblKulup.Location = new System.Drawing.Point(348, 17);
            this.lblKulup.Name = "lblKulup";
            this.lblKulup.Size = new System.Drawing.Size(184, 15);
            this.lblKulup.TabIndex = 0;
            this.lblKulup.Text = "Katılmak istediği kulüp etkinlikleri";
            // 
            // lblBosZaman
            // 
            this.lblBosZaman.AutoSize = true;
            this.lblBosZaman.Location = new System.Drawing.Point(17, 187);
            this.lblBosZaman.Name = "lblBosZaman";
            this.lblBosZaman.Size = new System.Drawing.Size(157, 15);
            this.lblBosZaman.TabIndex = 0;
            this.lblBosZaman.Text = "Boş zamanlardaki etkinlikleri";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(17, 100);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(131, 15);
            this.label27.TabIndex = 0;
            this.label27.Text = "Başarısız olduğu dersler";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(17, 62);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(123, 15);
            this.label12.TabIndex = 0;
            this.label12.Text = "Başarılı olduğu dersler";
            // 
            // lblSinifRehber
            // 
            this.lblSinifRehber.AutoSize = true;
            this.lblSinifRehber.Location = new System.Drawing.Point(17, 26);
            this.lblSinifRehber.Name = "lblSinifRehber";
            this.lblSinifRehber.Size = new System.Drawing.Size(131, 15);
            this.lblSinifRehber.TabIndex = 0;
            this.lblSinifRehber.Text = "Sınıf Rehber Öğretmeni";
            // 
            // tabPage7
            // 
            this.tabPage7.BackColor = System.Drawing.Color.LightGray;
            this.tabPage7.Controls.Add(this.btnKardesKaydet);
            this.tabPage7.Controls.Add(this.dataGridView1);
            this.tabPage7.Location = new System.Drawing.Point(4, 24);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(615, 268);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Kardeşleri";
            // 
            // btnKardesKaydet
            // 
            this.btnKardesKaydet.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnKardesKaydet.Image = global::rbsProje.Properties.Resources.kaydet3;
            this.btnKardesKaydet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnKardesKaydet.Location = new System.Drawing.Point(398, 190);
            this.btnKardesKaydet.Name = "btnKardesKaydet";
            this.btnKardesKaydet.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.btnKardesKaydet.Size = new System.Drawing.Size(109, 48);
            this.btnKardesKaydet.TabIndex = 12;
            this.btnKardesKaydet.Text = "           Kaydet";
            this.btnKardesKaydet.UseVisualStyleBackColor = true;
            this.btnKardesKaydet.Click += new System.EventHandler(this.btnKardesKaydet_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7});
            this.dataGridView1.Location = new System.Drawing.Point(23, 18);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(531, 150);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Adı Soyadı";
            this.Column1.Name = "Column1";
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Doğum Yılı";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Öz/Üvey";
            this.Column3.Items.AddRange(new object[] {
            "ÖZ",
            "ÜVEY"});
            this.Column3.Name = "Column3";
            this.Column3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column3.Width = 60;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Sağ/Ölü";
            this.Column4.Items.AddRange(new object[] {
            "SAĞ",
            "ÖLÜ"});
            this.Column4.Name = "Column4";
            this.Column4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column4.Width = 60;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Okul";
            this.Column5.Name = "Column5";
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Sınıf";
            this.Column6.Name = "Column6";
            this.Column6.Width = 50;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "Sil";
            this.Column7.Name = "Column7";
            this.Column7.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column7.Text = "SİL";
            this.Column7.UseColumnTextForButtonValue = true;
            this.Column7.Width = 50;
            // 
            // tabPage8
            // 
            this.tabPage8.BackColor = System.Drawing.Color.LightGray;
            this.tabPage8.Controls.Add(this.btnEkKaydet);
            this.tabPage8.Controls.Add(this.label24);
            this.tabPage8.Controls.Add(this.label23);
            this.tabPage8.Controls.Add(this.label22);
            this.tabPage8.Controls.Add(this.label21);
            this.tabPage8.Controls.Add(this.label20);
            this.tabPage8.Controls.Add(this.label26);
            this.tabPage8.Controls.Add(this.label19);
            this.tabPage8.Controls.Add(this.label25);
            this.tabPage8.Controls.Add(this.label13);
            this.tabPage8.Controls.Add(this.label18);
            this.tabPage8.Controls.Add(this.label17);
            this.tabPage8.Controls.Add(this.label16);
            this.tabPage8.Controls.Add(this.label14);
            this.tabPage8.Controls.Add(this.label15);
            this.tabPage8.Controls.Add(this.label1);
            this.tabPage8.Controls.Add(this.cOzBakim);
            this.tabPage8.Controls.Add(this.cDersGec);
            this.tabPage8.Controls.Add(this.cOkulKurallari);
            this.tabPage8.Controls.Add(this.cSinifKurallari);
            this.tabPage8.Controls.Add(this.cOgrtSaygisiz);
            this.tabPage8.Controls.Add(this.cArkadasSaygisiz);
            this.tabPage8.Controls.Add(this.cDikkat);
            this.tabPage8.Controls.Add(this.cArgo);
            this.tabPage8.Controls.Add(this.cKapanik);
            this.tabPage8.Controls.Add(this.cDersKonusuyor);
            this.tabPage8.Controls.Add(this.cHircin);
            this.tabPage8.Controls.Add(this.cSaldırgan);
            this.tabPage8.Controls.Add(this.cSakin);
            this.tabPage8.Controls.Add(this.cHaylaz);
            this.tabPage8.Controls.Add(this.cDaginik);
            this.tabPage8.Location = new System.Drawing.Point(4, 24);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(615, 268);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "Ek Açıklamalar";
            // 
            // btnEkKaydet
            // 
            this.btnEkKaydet.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnEkKaydet.Image = global::rbsProje.Properties.Resources.kaydet4;
            this.btnEkKaydet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEkKaydet.Location = new System.Drawing.Point(438, 66);
            this.btnEkKaydet.Name = "btnEkKaydet";
            this.btnEkKaydet.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.btnEkKaydet.Size = new System.Drawing.Size(109, 48);
            this.btnEkKaydet.TabIndex = 13;
            this.btnEkKaydet.Text = "           Kaydet";
            this.btnEkKaydet.UseVisualStyleBackColor = true;
            this.btnEkKaydet.Click += new System.EventHandler(this.button1_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(236, 130);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(112, 15);
            this.label24.TabIndex = 1;
            this.label24.Text = "Derslere geç kalıyor.";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(236, 102);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(143, 15);
            this.label23.TabIndex = 1;
            this.label23.Text = "Okul kurallarına uymuyor.";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(236, 75);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(142, 15);
            this.label22.TabIndex = 1;
            this.label22.Text = "Sınıf kurallarına uymuyor.";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(34, 130);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(164, 15);
            this.label21.TabIndex = 1;
            this.label21.Text = "Öğretmenlerine karşı saygısız.";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(34, 99);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(152, 15);
            this.label20.TabIndex = 1;
            this.label20.Text = "Arkadaşlarına karşı saygısız.";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(135, 69);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(68, 15);
            this.label26.TabIndex = 1;
            this.label26.Text = "İçe Kapanık";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(34, 68);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(57, 15);
            this.label19.TabIndex = 1;
            this.label19.Text = "Saldırgan";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(135, 45);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(39, 15);
            this.label25.TabIndex = 1;
            this.label25.Text = "Hırçın";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(34, 44);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(43, 15);
            this.label13.TabIndex = 1;
            this.label13.Text = "Haylaz";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(236, 156);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(172, 15);
            this.label18.TabIndex = 1;
            this.label18.Text = "Özbakım becerileri gelişmemiş.";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(236, 19);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(102, 15);
            this.label17.TabIndex = 1;
            this.label17.Text = "Derste konuşuyor.";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(34, 158);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(164, 15);
            this.label16.TabIndex = 1;
            this.label16.Text = "Derste dikkatini toplayamıyor.";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(236, 45);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(141, 15);
            this.label14.TabIndex = 1;
            this.label14.Text = "Argo kelimeler kullanıyor.";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(135, 20);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(36, 15);
            this.label15.TabIndex = 1;
            this.label15.Text = "Sakin";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Dağınık";
            // 
            // cOzBakim
            // 
            this.cOzBakim.AutoSize = true;
            this.cOzBakim.Location = new System.Drawing.Point(221, 157);
            this.cOzBakim.Name = "cOzBakim";
            this.cOzBakim.Size = new System.Drawing.Size(15, 14);
            this.cOzBakim.TabIndex = 0;
            this.cOzBakim.UseVisualStyleBackColor = true;
            // 
            // cDersGec
            // 
            this.cDersGec.AutoSize = true;
            this.cDersGec.Location = new System.Drawing.Point(221, 131);
            this.cDersGec.Name = "cDersGec";
            this.cDersGec.Size = new System.Drawing.Size(15, 14);
            this.cDersGec.TabIndex = 0;
            this.cDersGec.UseVisualStyleBackColor = true;
            // 
            // cOkulKurallari
            // 
            this.cOkulKurallari.AutoSize = true;
            this.cOkulKurallari.Location = new System.Drawing.Point(221, 104);
            this.cOkulKurallari.Name = "cOkulKurallari";
            this.cOkulKurallari.Size = new System.Drawing.Size(15, 14);
            this.cOkulKurallari.TabIndex = 0;
            this.cOkulKurallari.UseVisualStyleBackColor = true;
            // 
            // cSinifKurallari
            // 
            this.cSinifKurallari.AutoSize = true;
            this.cSinifKurallari.Location = new System.Drawing.Point(221, 75);
            this.cSinifKurallari.Name = "cSinifKurallari";
            this.cSinifKurallari.Size = new System.Drawing.Size(15, 14);
            this.cSinifKurallari.TabIndex = 0;
            this.cSinifKurallari.UseVisualStyleBackColor = true;
            // 
            // cOgrtSaygisiz
            // 
            this.cOgrtSaygisiz.AutoSize = true;
            this.cOgrtSaygisiz.Location = new System.Drawing.Point(19, 130);
            this.cOgrtSaygisiz.Name = "cOgrtSaygisiz";
            this.cOgrtSaygisiz.Size = new System.Drawing.Size(15, 14);
            this.cOgrtSaygisiz.TabIndex = 0;
            this.cOgrtSaygisiz.UseVisualStyleBackColor = true;
            // 
            // cArkadasSaygisiz
            // 
            this.cArkadasSaygisiz.AutoSize = true;
            this.cArkadasSaygisiz.Location = new System.Drawing.Point(19, 100);
            this.cArkadasSaygisiz.Name = "cArkadasSaygisiz";
            this.cArkadasSaygisiz.Size = new System.Drawing.Size(15, 14);
            this.cArkadasSaygisiz.TabIndex = 0;
            this.cArkadasSaygisiz.UseVisualStyleBackColor = true;
            // 
            // cDikkat
            // 
            this.cDikkat.AutoSize = true;
            this.cDikkat.Location = new System.Drawing.Point(19, 158);
            this.cDikkat.Name = "cDikkat";
            this.cDikkat.Size = new System.Drawing.Size(15, 14);
            this.cDikkat.TabIndex = 0;
            this.cDikkat.UseVisualStyleBackColor = true;
            // 
            // cArgo
            // 
            this.cArgo.AutoSize = true;
            this.cArgo.Location = new System.Drawing.Point(221, 47);
            this.cArgo.Name = "cArgo";
            this.cArgo.Size = new System.Drawing.Size(15, 14);
            this.cArgo.TabIndex = 0;
            this.cArgo.UseVisualStyleBackColor = true;
            // 
            // cKapanik
            // 
            this.cKapanik.AutoSize = true;
            this.cKapanik.Location = new System.Drawing.Point(120, 70);
            this.cKapanik.Name = "cKapanik";
            this.cKapanik.Size = new System.Drawing.Size(15, 14);
            this.cKapanik.TabIndex = 0;
            this.cKapanik.UseVisualStyleBackColor = true;
            // 
            // cDersKonusuyor
            // 
            this.cDersKonusuyor.AutoSize = true;
            this.cDersKonusuyor.Location = new System.Drawing.Point(221, 20);
            this.cDersKonusuyor.Name = "cDersKonusuyor";
            this.cDersKonusuyor.Size = new System.Drawing.Size(15, 14);
            this.cDersKonusuyor.TabIndex = 0;
            this.cDersKonusuyor.UseVisualStyleBackColor = true;
            // 
            // cHircin
            // 
            this.cHircin.AutoSize = true;
            this.cHircin.Location = new System.Drawing.Point(120, 46);
            this.cHircin.Name = "cHircin";
            this.cHircin.Size = new System.Drawing.Size(15, 14);
            this.cHircin.TabIndex = 0;
            this.cHircin.UseVisualStyleBackColor = true;
            // 
            // cSaldırgan
            // 
            this.cSaldırgan.AutoSize = true;
            this.cSaldırgan.Location = new System.Drawing.Point(19, 69);
            this.cSaldırgan.Name = "cSaldırgan";
            this.cSaldırgan.Size = new System.Drawing.Size(15, 14);
            this.cSaldırgan.TabIndex = 0;
            this.cSaldırgan.UseVisualStyleBackColor = true;
            // 
            // cSakin
            // 
            this.cSakin.AutoSize = true;
            this.cSakin.Location = new System.Drawing.Point(120, 21);
            this.cSakin.Name = "cSakin";
            this.cSakin.Size = new System.Drawing.Size(15, 14);
            this.cSakin.TabIndex = 0;
            this.cSakin.UseVisualStyleBackColor = true;
            // 
            // cHaylaz
            // 
            this.cHaylaz.AutoSize = true;
            this.cHaylaz.Location = new System.Drawing.Point(19, 45);
            this.cHaylaz.Name = "cHaylaz";
            this.cHaylaz.Size = new System.Drawing.Size(15, 14);
            this.cHaylaz.TabIndex = 0;
            this.cHaylaz.UseVisualStyleBackColor = true;
            // 
            // cDaginik
            // 
            this.cDaginik.AutoSize = true;
            this.cDaginik.Location = new System.Drawing.Point(19, 20);
            this.cDaginik.Name = "cDaginik";
            this.cDaginik.Size = new System.Drawing.Size(15, 14);
            this.cDaginik.TabIndex = 0;
            this.cDaginik.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(745, 34);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(133, 171);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // btnAra
            // 
            this.btnAra.Location = new System.Drawing.Point(234, 37);
            this.btnAra.Name = "btnAra";
            this.btnAra.Size = new System.Drawing.Size(31, 25);
            this.btnAra.TabIndex = 4;
            this.btnAra.Text = "Ara";
            this.btnAra.UseVisualStyleBackColor = true;
            this.btnAra.Click += new System.EventHandler(this.btnAra_Click);
            // 
            // lblListeleBaslik
            // 
            this.lblListeleBaslik.AutoSize = true;
            this.lblListeleBaslik.Font = new System.Drawing.Font("Segoe UI Semibold", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblListeleBaslik.Location = new System.Drawing.Point(13, 6);
            this.lblListeleBaslik.Name = "lblListeleBaslik";
            this.lblListeleBaslik.Size = new System.Drawing.Size(148, 25);
            this.lblListeleBaslik.TabIndex = 7;
            this.lblListeleBaslik.Text = "Öğrenci Bilgileri";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // UCuyeListele2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblListeleBaslik);
            this.Controls.Add(this.groupBox1);
            this.Name = "UCuyeListele2";
            this.Size = new System.Drawing.Size(928, 597);
            this.Load += new System.EventHandler(this.UCuyeListele2_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.grbVeli.ResumeLayout(false);
            this.grbVeli.PerformLayout();
            this.grbAnne.ResumeLayout(false);
            this.grbAnne.PerformLayout();
            this.grbBaba.ResumeLayout(false);
            this.grbBaba.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.Button btnResimDuzenle;
        public System.Windows.Forms.Button btnDetayGetir;
        public System.Windows.Forms.Label lblListeleBaslik;
        public System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.TreeView treeView1;
        public System.Windows.Forms.Button btnTumunuGetir;
        public System.Windows.Forms.TextBox textBox1;
        public System.Windows.Forms.Button btnAra;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.TextBox txtNo;
        public System.Windows.Forms.TextBox txtSinif;
        public System.Windows.Forms.TextBox txtAd;
        public System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox cOzBakim;
        private System.Windows.Forms.CheckBox cDersGec;
        private System.Windows.Forms.CheckBox cOkulKurallari;
        private System.Windows.Forms.CheckBox cSinifKurallari;
        private System.Windows.Forms.CheckBox cOgrtSaygisiz;
        private System.Windows.Forms.CheckBox cArkadasSaygisiz;
        private System.Windows.Forms.CheckBox cDikkat;
        private System.Windows.Forms.CheckBox cArgo;
        private System.Windows.Forms.CheckBox cKapanik;
        private System.Windows.Forms.CheckBox cDersKonusuyor;
        private System.Windows.Forms.CheckBox cHircin;
        private System.Windows.Forms.CheckBox cSaldırgan;
        private System.Windows.Forms.CheckBox cSakin;
        private System.Windows.Forms.CheckBox cHaylaz;
        private System.Windows.Forms.CheckBox cDaginik;
        private System.Windows.Forms.DataGridView dataGridView1;
        public System.Windows.Forms.TextBox txtVeliYakinligi;
        public System.Windows.Forms.TextBox txtVeliAdSoyad;
        public System.Windows.Forms.Button btnAileKaydet;
        private System.Windows.Forms.Label lblSinifRehber;
        public System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.TextBox txtVeliTel;
        public System.Windows.Forms.TextBox txtVeliMeslek;
        public System.Windows.Forms.TextBox txtBabaTel;
        public System.Windows.Forms.TextBox txtBabaMeslek;
        public System.Windows.Forms.TextBox txtBabaAd;
        public System.Windows.Forms.TextBox txtAnneAd;
        public System.Windows.Forms.TextBox txtAnneMeslek;
        public System.Windows.Forms.TextBox txtAnneTel;
        public System.Windows.Forms.Label lblVeliYakin;
        public System.Windows.Forms.Label lblVeliAd;
        public System.Windows.Forms.Label lblVeliTel;
        public System.Windows.Forms.Label lblVeliNeslek;
        public System.Windows.Forms.Label lblBabaTel;
        public System.Windows.Forms.Label lblBabaOz;
        public System.Windows.Forms.Label lblBabaMes;
        public System.Windows.Forms.Label lblBabaAd;
        public System.Windows.Forms.GroupBox grbVeli;
        public System.Windows.Forms.Label lblVeliAdres;
        public System.Windows.Forms.GroupBox grbBaba;
        public System.Windows.Forms.Label lblBabaSag;
        public System.Windows.Forms.GroupBox grbAnne;
        public System.Windows.Forms.ComboBox cmbAnneSag;
        public System.Windows.Forms.ComboBox cmbAnneOz;
        public System.Windows.Forms.ComboBox cmAnneBirlik;
        public System.Windows.Forms.Label lblAnneAd;
        public System.Windows.Forms.Label lblAnneBirlik;
        public System.Windows.Forms.Label lblAnneTel;
        public System.Windows.Forms.Label lblAnneMeslek;
        public System.Windows.Forms.Label lblAnneSag;
        public System.Windows.Forms.Label lblAnneOz;
        public System.Windows.Forms.ComboBox cmbSagOlu;
        public System.Windows.Forms.ComboBox cmbOzUvey;
        public System.Windows.Forms.ComboBox cmbBirlik;
        public System.Windows.Forms.Label lblBirliktelik;
        public System.Windows.Forms.RichTextBox richTextAdres;
        private System.Windows.Forms.ComboBox cmbAileCiddi;
        private System.Windows.Forms.ComboBox cmbOgrenciCiddi;
        private System.Windows.Forms.ComboBox cmbAileProblem;
        private System.Windows.Forms.ComboBox cmbZararli;
        private System.Windows.Forms.Label lblAileCiddi;
        private System.Windows.Forms.Label lblOgrenciCiddi;
        private System.Windows.Forms.Label lblAileProblem;
        private System.Windows.Forms.Label lblZararlı;
        private System.Windows.Forms.ComboBox cmbEv;
        private System.Windows.Forms.Label lblEv;
        private System.Windows.Forms.TextBox txtMeslekAlan;
        private System.Windows.Forms.TextBox txtOncekiOkul;
        private System.Windows.Forms.RichTextBox richOkulRahatsiz;
        private System.Windows.Forms.ComboBox cmbOda;
        private System.Windows.Forms.ComboBox cmbDestek;
        private System.Windows.Forms.ComboBox cmbMisafir;
        private System.Windows.Forms.ComboBox cmbYetenek;
        private System.Windows.Forms.ComboBox cmbSinifTekrari;
        private System.Windows.Forms.ComboBox cmbEvIsinma;
        private System.Windows.Forms.Label lblYonelim;
        private System.Windows.Forms.Label lblOncekiOkul;
        private System.Windows.Forms.Label lblOkulRahatsiz;
        private System.Windows.Forms.Label lblOda;
        private System.Windows.Forms.Label lblOkulDisiDestek;
        private System.Windows.Forms.Label lblMisafir;
        private System.Windows.Forms.Label lblYetenek;
        private System.Windows.Forms.Label lblSinifTekrari;
        private System.Windows.Forms.Label lblEvIsinma;
        public System.Windows.Forms.Button btnGenelKaydet;
        private System.Windows.Forms.Label lblKilo;
        private System.Windows.Forms.Label lblBoy;
        private System.Windows.Forms.ComboBox cmbKaza;
        private System.Windows.Forms.ComboBox cmbEngel;
        private System.Windows.Forms.Label lblKaza;
        private System.Windows.Forms.Label lblEngel;
        private System.Windows.Forms.ComboBox cmbProtez;
        private System.Windows.Forms.Label lblProtez;
        public System.Windows.Forms.Button btnSaglikKaydet;
        private System.Windows.Forms.TextBox txtSinifRehber;
        private System.Windows.Forms.TextBox txtBasarisizDers;
        private System.Windows.Forms.TextBox txtBasariliDers;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label12;
        public System.Windows.Forms.Button btnOgrDurumuKaydet;
        private System.Windows.Forms.RichTextBox richKulup;
        private System.Windows.Forms.RichTextBox richBosZaman;
        private System.Windows.Forms.Label lblKulup;
        private System.Windows.Forms.Label lblBosZaman;
        public System.Windows.Forms.Button btnKardesKaydet;
        public System.Windows.Forms.Button btnEkKaydet;
        public System.Windows.Forms.ComboBox cmbAnneEngel;
        public System.Windows.Forms.Label lblAnneEngel;
        public System.Windows.Forms.ComboBox cmbBabaEngel;
        public System.Windows.Forms.Label lblBabaEngel;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column3;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewButtonColumn Column7;
        public System.Windows.Forms.TextBox txtDogumYil;
        public System.Windows.Forms.TextBox txtRehberOgr;
        public System.Windows.Forms.TextBox txtKanGrubu;
        public System.Windows.Forms.TextBox txtDogumIlce;
        public System.Windows.Forms.TextBox txtDogumIL;
        public System.Windows.Forms.TextBox txtCinsiyet;
    }
}
