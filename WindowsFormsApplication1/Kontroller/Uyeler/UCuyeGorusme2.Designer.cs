﻿namespace rbsProje.Kontroller.Uyeler
{
    partial class UCuyeGorusme2
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.btnOgrenciGorusmeYazdir = new System.Windows.Forms.Button();
            this.btnGorusmeKaydet = new System.Windows.Forms.Button();
            this.clistGorusmeSonuc = new System.Windows.Forms.CheckedListBox();
            this.clistYapilacakCalismalar = new System.Windows.Forms.CheckedListBox();
            this.clistSorunAlanlari = new System.Windows.Forms.CheckedListBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.cmbGorusmeyiYapan = new System.Windows.Forms.ComboBox();
            this.cmbPdrAlanlari = new System.Windows.Forms.ComboBox();
            this.cmbGorusmeTuru = new System.Windows.Forms.ComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.label36 = new System.Windows.Forms.Label();
            this.lblAnaBaslik = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnResimDuzenle
            // 
            this.btnResimDuzenle.Location = new System.Drawing.Point(918, 259);
            this.btnResimDuzenle.Size = new System.Drawing.Size(95, 30);
            this.btnResimDuzenle.Visible = false;
            // 
            // btnDetayGetir
            // 
            this.btnDetayGetir.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnDetayGetir.Location = new System.Drawing.Point(161, 205);
            this.btnDetayGetir.Size = new System.Drawing.Size(129, 46);
            // 
            // lblListeleBaslik
            // 
            this.lblListeleBaslik.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblListeleBaslik.Location = new System.Drawing.Point(8, 10);
            this.lblListeleBaslik.Size = new System.Drawing.Size(127, 21);
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(12, 20);
            this.groupBox1.Size = new System.Drawing.Size(1042, 328);
            // 
            // treeView1
            // 
            this.treeView1.LineColor = System.Drawing.Color.Black;
            this.treeView1.Location = new System.Drawing.Point(16, 110);
            this.treeView1.Size = new System.Drawing.Size(248, 209);
            // 
            // btnTumunuGetir
            // 
            this.btnTumunuGetir.Location = new System.Drawing.Point(16, 70);
            this.btnTumunuGetir.Size = new System.Drawing.Size(163, 28);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(121, 33);
            this.textBox1.Size = new System.Drawing.Size(143, 22);
            // 
            // btnAra
            // 
            this.btnAra.Location = new System.Drawing.Point(197, 70);
            this.btnAra.Size = new System.Drawing.Size(67, 25);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(14, 36);
            // 
            // groupBox2
            // 
            this.groupBox2.Location = new System.Drawing.Point(457, 567);
            this.groupBox2.Size = new System.Drawing.Size(441, 257);
            // 
            // txtNo
            // 
            this.txtNo.BackColor = System.Drawing.Color.White;
            this.txtNo.TextChanged += new System.EventHandler(this.txtNo_TextChanged);
            // 
            // txtSinif
            // 
            this.txtSinif.BackColor = System.Drawing.Color.White;
            // 
            // txtAd
            // 
            this.txtAd.BackColor = System.Drawing.Color.White;
            // 
            // tabControl1
            // 
            this.tabControl1.Location = new System.Drawing.Point(281, 33);
            this.tabControl1.Size = new System.Drawing.Size(617, 290);
            this.tabControl1.Visible = true;
            // 
            // btnAileKaydet
            // 
            this.btnAileKaydet.Location = new System.Drawing.Point(355, 78);
            this.btnAileKaydet.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(904, 82);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label47);
            this.groupBox3.Controls.Add(this.label48);
            this.groupBox3.Controls.Add(this.label49);
            this.groupBox3.Controls.Add(this.label46);
            this.groupBox3.Controls.Add(this.label45);
            this.groupBox3.Controls.Add(this.label44);
            this.groupBox3.Controls.Add(this.label43);
            this.groupBox3.Controls.Add(this.label42);
            this.groupBox3.Controls.Add(this.richTextBox1);
            this.groupBox3.Controls.Add(this.btnOgrenciGorusmeYazdir);
            this.groupBox3.Controls.Add(this.btnGorusmeKaydet);
            this.groupBox3.Controls.Add(this.clistGorusmeSonuc);
            this.groupBox3.Controls.Add(this.clistYapilacakCalismalar);
            this.groupBox3.Controls.Add(this.clistSorunAlanlari);
            this.groupBox3.Controls.Add(this.dateTimePicker1);
            this.groupBox3.Controls.Add(this.cmbGorusmeyiYapan);
            this.groupBox3.Controls.Add(this.cmbPdrAlanlari);
            this.groupBox3.Controls.Add(this.cmbGorusmeTuru);
            this.groupBox3.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox3.Location = new System.Drawing.Point(38, 349);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(996, 571);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(7, 57);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(55, 15);
            this.label47.TabIndex = 19;
            this.label47.Text = "Görüşme";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(698, 104);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(82, 15);
            this.label48.TabIndex = 19;
            this.label48.Text = "Sorun Alanları";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(11, 308);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(80, 15);
            this.label49.TabIndex = 19;
            this.label49.Text = "Sonuç Bigileri";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(394, 104);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(113, 15);
            this.label46.TabIndex = 19;
            this.label46.Text = "Yapılacak Çalışmalar";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(361, 19);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(82, 15);
            this.label45.TabIndex = 18;
            this.label45.Text = "Görüşme Türü";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(712, 19);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(60, 15);
            this.label44.TabIndex = 17;
            this.label44.Text = "PDR Alanı";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(712, 57);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(99, 15);
            this.label43.TabIndex = 16;
            this.label43.Text = "Görüşmeyi Yapan";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(28, 24);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(87, 15);
            this.label42.TabIndex = 15;
            this.label42.Text = "Görüşme Tarihi";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(10, 78);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(346, 223);
            this.richTextBox1.TabIndex = 14;
            this.richTextBox1.Text = "";
            // 
            // btnOgrenciGorusmeYazdir
            // 
            this.btnOgrenciGorusmeYazdir.Enabled = false;
            this.btnOgrenciGorusmeYazdir.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnOgrenciGorusmeYazdir.Image = global::rbsProje.Properties.Resources.printer2;
            this.btnOgrenciGorusmeYazdir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOgrenciGorusmeYazdir.Location = new System.Drawing.Point(486, 54);
            this.btnOgrenciGorusmeYazdir.Name = "btnOgrenciGorusmeYazdir";
            this.btnOgrenciGorusmeYazdir.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.btnOgrenciGorusmeYazdir.Size = new System.Drawing.Size(101, 40);
            this.btnOgrenciGorusmeYazdir.TabIndex = 12;
            this.btnOgrenciGorusmeYazdir.Text = "           Yazdır";
            this.btnOgrenciGorusmeYazdir.UseVisualStyleBackColor = true;
            this.btnOgrenciGorusmeYazdir.Click += new System.EventHandler(this.btnOgrenciGorusmeYazdir_Click);
            // 
            // btnGorusmeKaydet
            // 
            this.btnGorusmeKaydet.Enabled = false;
            this.btnGorusmeKaydet.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnGorusmeKaydet.Image = global::rbsProje.Properties.Resources.diskette;
            this.btnGorusmeKaydet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGorusmeKaydet.Location = new System.Drawing.Point(364, 54);
            this.btnGorusmeKaydet.Name = "btnGorusmeKaydet";
            this.btnGorusmeKaydet.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.btnGorusmeKaydet.Size = new System.Drawing.Size(104, 40);
            this.btnGorusmeKaydet.TabIndex = 13;
            this.btnGorusmeKaydet.Text = "       Kaydet";
            this.btnGorusmeKaydet.UseVisualStyleBackColor = true;
            this.btnGorusmeKaydet.Click += new System.EventHandler(this.btnGorusmeKaydet_Click);
            // 
            // clistGorusmeSonuc
            // 
            this.clistGorusmeSonuc.CheckOnClick = true;
            this.clistGorusmeSonuc.FormattingEnabled = true;
            this.clistGorusmeSonuc.HorizontalScrollbar = true;
            this.clistGorusmeSonuc.Location = new System.Drawing.Point(10, 329);
            this.clistGorusmeSonuc.Name = "clistGorusmeSonuc";
            this.clistGorusmeSonuc.Size = new System.Drawing.Size(972, 220);
            this.clistGorusmeSonuc.TabIndex = 3;
            // 
            // clistYapilacakCalismalar
            // 
            this.clistYapilacakCalismalar.CheckOnClick = true;
            this.clistYapilacakCalismalar.FormattingEnabled = true;
            this.clistYapilacakCalismalar.HorizontalScrollbar = true;
            this.clistYapilacakCalismalar.Location = new System.Drawing.Point(393, 124);
            this.clistYapilacakCalismalar.Name = "clistYapilacakCalismalar";
            this.clistYapilacakCalismalar.Size = new System.Drawing.Size(267, 184);
            this.clistYapilacakCalismalar.TabIndex = 3;
            // 
            // clistSorunAlanlari
            // 
            this.clistSorunAlanlari.CheckOnClick = true;
            this.clistSorunAlanlari.FormattingEnabled = true;
            this.clistSorunAlanlari.HorizontalScrollbar = true;
            this.clistSorunAlanlari.Location = new System.Drawing.Point(697, 124);
            this.clistSorunAlanlari.Name = "clistSorunAlanlari";
            this.clistSorunAlanlari.Size = new System.Drawing.Size(287, 184);
            this.clistSorunAlanlari.TabIndex = 3;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "";
            this.dateTimePicker1.Location = new System.Drawing.Point(121, 22);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(186, 23);
            this.dateTimePicker1.TabIndex = 2;
            // 
            // cmbGorusmeyiYapan
            // 
            this.cmbGorusmeyiYapan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGorusmeyiYapan.FormattingEnabled = true;
            this.cmbGorusmeyiYapan.Location = new System.Drawing.Point(823, 54);
            this.cmbGorusmeyiYapan.Name = "cmbGorusmeyiYapan";
            this.cmbGorusmeyiYapan.Size = new System.Drawing.Size(159, 23);
            this.cmbGorusmeyiYapan.TabIndex = 1;
            // 
            // cmbPdrAlanlari
            // 
            this.cmbPdrAlanlari.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPdrAlanlari.FormattingEnabled = true;
            this.cmbPdrAlanlari.Location = new System.Drawing.Point(823, 16);
            this.cmbPdrAlanlari.Name = "cmbPdrAlanlari";
            this.cmbPdrAlanlari.Size = new System.Drawing.Size(159, 23);
            this.cmbPdrAlanlari.TabIndex = 1;
            // 
            // cmbGorusmeTuru
            // 
            this.cmbGorusmeTuru.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGorusmeTuru.FormattingEnabled = true;
            this.cmbGorusmeTuru.Location = new System.Drawing.Point(466, 16);
            this.cmbGorusmeTuru.Name = "cmbGorusmeTuru";
            this.cmbGorusmeTuru.Size = new System.Drawing.Size(171, 23);
            this.cmbGorusmeTuru.TabIndex = 1;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label32.Location = new System.Drawing.Point(661, 99);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(81, 15);
            this.label32.TabIndex = 11;
            this.label32.Text = "Sorun Alanları";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label33.Location = new System.Drawing.Point(6, 318);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(160, 15);
            this.label33.TabIndex = 11;
            this.label33.Text = "Sonuç ve Görüş Öneri Bigileri";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label31.Location = new System.Drawing.Point(381, 99);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(114, 15);
            this.label31.TabIndex = 11;
            this.label31.Text = "Yapılacak Çalışmalar";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label38.Location = new System.Drawing.Point(682, 19);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(59, 15);
            this.label38.TabIndex = 11;
            this.label38.Text = "PDR Alanı";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label37.Location = new System.Drawing.Point(682, 57);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(99, 15);
            this.label37.TabIndex = 11;
            this.label37.Text = "Görüşmeyi Yapan";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label35.Location = new System.Drawing.Point(357, 19);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(83, 15);
            this.label35.TabIndex = 11;
            this.label35.Text = "Görüşme Türü";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label34.Location = new System.Drawing.Point(16, 25);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(87, 15);
            this.label34.TabIndex = 11;
            this.label34.Text = "Görüşme Tarihi";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label30.Location = new System.Drawing.Point(13, 100);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(55, 15);
            this.label30.TabIndex = 11;
            this.label30.Text = "Görüşme";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label29.Location = new System.Drawing.Point(6, -3);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(133, 21);
            this.label29.TabIndex = 10;
            this.label29.Text = "Öğrenci Görüşme";
            // 
            // label20
            // 
            this.label20.Location = new System.Drawing.Point(0, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(100, 23);
            this.label20.TabIndex = 0;
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(0, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(100, 23);
            this.label19.TabIndex = 0;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label21.Location = new System.Drawing.Point(6, -3);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(138, 21);
            this.label21.TabIndex = 0;
            this.label21.Text = "Öğrenci Görüşme";
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(0, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(100, 23);
            this.label18.TabIndex = 0;
            // 
            // label17
            // 
            this.label17.Location = new System.Drawing.Point(0, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(100, 23);
            this.label17.TabIndex = 0;
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(0, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(100, 23);
            this.label16.TabIndex = 0;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(0, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(100, 23);
            this.label15.TabIndex = 0;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 106);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(55, 15);
            this.label14.TabIndex = 0;
            this.label14.Text = "Görüşme";
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(0, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(100, 23);
            this.label13.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 0;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label36.Location = new System.Drawing.Point(12, 57);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(55, 15);
            this.label36.TabIndex = 11;
            this.label36.Text = "Görüşme";
            // 
            // lblAnaBaslik
            // 
            this.lblAnaBaslik.AutoSize = true;
            this.lblAnaBaslik.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblAnaBaslik.Location = new System.Drawing.Point(422, 10);
            this.lblAnaBaslik.Name = "lblAnaBaslik";
            this.lblAnaBaslik.Size = new System.Drawing.Size(161, 25);
            this.lblAnaBaslik.TabIndex = 10;
            this.lblAnaBaslik.Text = "Öğrenci Görüşme";
            // 
            // UCuyeGorusme2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.lblAnaBaslik);
            this.Controls.Add(this.groupBox3);
            this.Name = "UCuyeGorusme2";
            this.Size = new System.Drawing.Size(1064, 928);
            this.Load += new System.EventHandler(this.UCuyeGorusme2_Load);
            this.Controls.SetChildIndex(this.groupBox3, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.lblListeleBaslik, 0);
            this.Controls.SetChildIndex(this.lblAnaBaslik, 0);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckedListBox clistGorusmeSonuc;
        private System.Windows.Forms.CheckedListBox clistSorunAlanlari;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.ComboBox cmbGorusmeyiYapan;
        private System.Windows.Forms.ComboBox cmbPdrAlanlari;
        private System.Windows.Forms.ComboBox cmbGorusmeTuru;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label21;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label lblAnaBaslik;
        public System.Windows.Forms.Button btnOgrenciGorusmeYazdir;
        public System.Windows.Forms.Button btnGorusmeKaydet;
        public System.Windows.Forms.CheckedListBox clistYapilacakCalismalar;
    }
}
