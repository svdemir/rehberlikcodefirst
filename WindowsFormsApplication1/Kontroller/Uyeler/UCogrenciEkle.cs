﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace rbsProje.Kontroller.Uyeler
{
    public partial class UCogrenciEkle : UCuyeListele2
    {
        public UCogrenciEkle()
        {
            InitializeComponent();
        }

        private void btnOgrenciKaydet_Click(object sender, EventArgs e)
        {
            using (dbEntities veritabanim = new dbEntities())
            {
                UYELER yeniOgrenci = new UYELER();
                yeniOgrenci.cinsiyet = txtCinsiyet.Text.ToUpper();
                yeniOgrenci.dogumYeriIL = txtDogumIL.Text.ToUpper();
                yeniOgrenci.dogumYeriIlce = txtDogumIlce.Text.ToUpper();
                if (!string.IsNullOrEmpty(txtDogumYil.Text))
                {
                    try
                    {
                        yeniOgrenci.dogumYili = Convert.ToInt64(txtDogumYil.Text);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Doğum yılı hatalı.", "Kayıt", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                yeniOgrenci.kanGrubu = txtKanGrubu.Text;
                if (!string.IsNullOrEmpty(txtNo.Text))
                {
                    try
                    {
                        yeniOgrenci.NO = Convert.ToInt64(txtNo.Text);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("NO  hatalı.", "Kayıt", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }
                }
                yeniOgrenci.SINIF = txtSinif.Text;
                yeniOgrenci.sinifRehberOgrt = txtRehberOgr.Text;

                string[] adSoyadDizisi = txtAd.Text.Split(' ');
                if (adSoyadDizisi.Length > 2)
                {
                    yeniOgrenci.AD = adSoyadDizisi[0] + " " + adSoyadDizisi[1];
                    yeniOgrenci.SOYAD = adSoyadDizisi[2];
                }
                else if (adSoyadDizisi.Length == 2)
                {
                    yeniOgrenci.AD = adSoyadDizisi[0];
                    yeniOgrenci.SOYAD = adSoyadDizisi[1];
                }
                veritabanim.UYELER.Add(yeniOgrenci);

                try
                {
                    veritabanim.SaveChanges();
                    MessageBox.Show("Kayıt işlemi başarılı.", "Kayıt", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ogrNo= Convert.ToInt64(txtNo.Text);

                }
                catch (Exception)
                {
                    MessageBox.Show("Kayıt hatası. Alanları ve öğrenci numarasını kontrol ediniz.", "Kayıt", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }



            }
        }

        private void UCogrenciEkle_Load(object sender, EventArgs e)
        {
            txtAd.ReadOnly = false;
            txtCinsiyet.ReadOnly = false;
            txtDogumIL.ReadOnly = false;
            txtDogumIlce.ReadOnly = false;
            txtDogumYil.ReadOnly = false;
            txtKanGrubu.ReadOnly = false;
            txtNo.ReadOnly = false;
            txtRehberOgr.ReadOnly = false;
            txtSinif.ReadOnly = false;

        }

    }
}
