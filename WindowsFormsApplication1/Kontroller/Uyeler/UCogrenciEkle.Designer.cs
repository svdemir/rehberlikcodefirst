﻿namespace rbsProje.Kontroller.Uyeler
{
    partial class UCogrenciEkle
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOgrenciKaydet = new System.Windows.Forms.Button();
            this.lblOgrUyari = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.grbVeli.SuspendLayout();
            this.grbBaba.SuspendLayout();
            this.grbAnne.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnResimDuzenle
            // 
            this.btnResimDuzenle.Location = new System.Drawing.Point(494, 222);
            // 
            // btnDetayGetir
            // 
            this.btnDetayGetir.Location = new System.Drawing.Point(25, 183);
            this.btnDetayGetir.Size = new System.Drawing.Size(116, 30);
            this.btnDetayGetir.Visible = false;
            // 
            // lblListeleBaslik
            // 
            this.lblListeleBaslik.Location = new System.Drawing.Point(13, -1);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblOgrUyari);
            this.groupBox1.Location = new System.Drawing.Point(10, 11);
            this.groupBox1.Size = new System.Drawing.Size(643, 558);
            this.groupBox1.Controls.SetChildIndex(this.btnTumunuGetir, 0);
            this.groupBox1.Controls.SetChildIndex(this.btnAra, 0);
            this.groupBox1.Controls.SetChildIndex(this.pictureBox1, 0);
            this.groupBox1.Controls.SetChildIndex(this.tabControl1, 0);
            this.groupBox1.Controls.SetChildIndex(this.treeView1, 0);
            this.groupBox1.Controls.SetChildIndex(this.textBox1, 0);
            this.groupBox1.Controls.SetChildIndex(this.label2, 0);
            this.groupBox1.Controls.SetChildIndex(this.btnResimDuzenle, 0);
            this.groupBox1.Controls.SetChildIndex(this.groupBox2, 0);
            this.groupBox1.Controls.SetChildIndex(this.lblOgrUyari, 0);
            // 
            // treeView1
            // 
            this.treeView1.Enabled = false;
            this.treeView1.LineColor = System.Drawing.Color.Black;
            this.treeView1.Visible = false;
            // 
            // btnTumunuGetir
            // 
            this.btnTumunuGetir.Enabled = false;
            this.btnTumunuGetir.Visible = false;
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(110, 16);
            this.textBox1.Visible = false;
            // 
            // btnAra
            // 
            this.btnAra.Enabled = false;
            this.btnAra.Location = new System.Drawing.Point(234, 60);
            this.btnAra.Visible = false;
            // 
            // label2
            // 
            this.label2.Enabled = false;
            this.label2.Location = new System.Drawing.Point(28, 19);
            this.label2.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnOgrenciKaydet);
            this.groupBox2.Location = new System.Drawing.Point(16, 35);
            this.groupBox2.Size = new System.Drawing.Size(441, 218);
            this.groupBox2.Controls.SetChildIndex(this.txtCinsiyet, 0);
            this.groupBox2.Controls.SetChildIndex(this.txtRehberOgr, 0);
            this.groupBox2.Controls.SetChildIndex(this.txtDogumYil, 0);
            this.groupBox2.Controls.SetChildIndex(this.txtDogumIL, 0);
            this.groupBox2.Controls.SetChildIndex(this.txtDogumIlce, 0);
            this.groupBox2.Controls.SetChildIndex(this.txtKanGrubu, 0);
            this.groupBox2.Controls.SetChildIndex(this.txtAd, 0);
            this.groupBox2.Controls.SetChildIndex(this.txtSinif, 0);
            this.groupBox2.Controls.SetChildIndex(this.btnDetayGetir, 0);
            this.groupBox2.Controls.SetChildIndex(this.txtNo, 0);
            this.groupBox2.Controls.SetChildIndex(this.btnOgrenciKaydet, 0);
            // 
            // txtNo
            // 
            this.txtNo.BackColor = System.Drawing.Color.White;
            // 
            // txtSinif
            // 
            this.txtSinif.BackColor = System.Drawing.Color.White;
            this.txtSinif.Location = new System.Drawing.Point(86, 57);
            // 
            // txtAd
            // 
            this.txtAd.BackColor = System.Drawing.Color.White;
            this.txtAd.Location = new System.Drawing.Point(86, 21);
            // 
            // tabControl1
            // 
            this.tabControl1.Location = new System.Drawing.Point(16, 258);
            this.tabControl1.Visible = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(475, 38);
            // 
            // txtKanGrubu
            // 
            this.txtKanGrubu.Location = new System.Drawing.Point(86, 138);
            // 
            // txtDogumIL
            // 
            this.txtDogumIL.Location = new System.Drawing.Point(86, 97);
            // 
            // btnOgrenciKaydet
            // 
            this.btnOgrenciKaydet.Image = global::rbsProje.Properties.Resources.add;
            this.btnOgrenciKaydet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOgrenciKaydet.Location = new System.Drawing.Point(25, 183);
            this.btnOgrenciKaydet.Name = "btnOgrenciKaydet";
            this.btnOgrenciKaydet.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnOgrenciKaydet.Size = new System.Drawing.Size(116, 30);
            this.btnOgrenciKaydet.TabIndex = 19;
            this.btnOgrenciKaydet.Text = "      Kaydet";
            this.btnOgrenciKaydet.UseVisualStyleBackColor = true;
            this.btnOgrenciKaydet.Click += new System.EventHandler(this.btnOgrenciKaydet_Click);
            // 
            // lblOgrUyari
            // 
            this.lblOgrUyari.AutoSize = true;
            this.lblOgrUyari.Location = new System.Drawing.Point(239, 18);
            this.lblOgrUyari.Name = "lblOgrUyari";
            this.lblOgrUyari.Size = new System.Drawing.Size(369, 13);
            this.lblOgrUyari.TabIndex = 11;
            this.lblOgrUyari.Text = "İlk olarak öğrenciyi kayıt edip daha sonra fotağraf ve diğer bilgileri kaydediniz" +
    ".";
            // 
            // UCogrenciEkle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "UCogrenciEkle";
            this.Size = new System.Drawing.Size(655, 574);
            this.Load += new System.EventHandler(this.UCogrenciEkle_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.grbVeli.ResumeLayout(false);
            this.grbVeli.PerformLayout();
            this.grbBaba.ResumeLayout(false);
            this.grbBaba.PerformLayout();
            this.grbAnne.ResumeLayout(false);
            this.grbAnne.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOgrenciKaydet;
        private System.Windows.Forms.Label lblOgrUyari;
    }
}
