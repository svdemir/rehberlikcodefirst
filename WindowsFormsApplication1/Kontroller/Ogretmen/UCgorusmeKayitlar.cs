﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace rbsProje.Kontroller.Ogretmen
{
    public partial class UCgorusmeKayitlar : UserControl
    {
        public UCgorusmeKayitlar()
        {
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
        }

        private void btnGuncelle_Click(object sender, EventArgs e)
        {
            try
            {
                frmOgretmenGorusme guncellenecekGorusme = new frmOgretmenGorusme(Convert.ToInt64(dataGridView1.SelectedRows[0].Cells[4].Value), true);
                guncellenecekGorusme.Show();
            }
            catch (Exception)
            {
                MessageBox.Show("Veritabanı hatası. Tekrar Deneyiniz.", "Kayıtlar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSil_Click(object sender, EventArgs e)
        {
            string silinecekID = dataGridView1.SelectedRows[0].Cells[4].Value.ToString();

            long numericGorID = Convert.ToInt64(silinecekID);

            using (dbEntities veritabanim = new dbEntities())
            {
                var silinecekObje = veritabanim.gorusmeOgretmen.Where(s => s.gorusmeID == numericGorID).SingleOrDefault();
                veritabanim.gorusmeOgretmen.Remove(silinecekObje);

                var silinecek1 = veritabanim.analizOgretmenGorusmeKonulari.Where(s => s.gorusmeID == numericGorID).SingleOrDefault();
                veritabanim.analizOgretmenGorusmeKonulari.Remove(silinecek1);

                var silinecek2 = veritabanim.analizOgretmenSonucCumleleri.Where(s => s.gorusmeID == numericGorID).ToList();
                veritabanim.analizOgretmenSonucCumleleri.RemoveRange(silinecek2);

                var silinecek3 = veritabanim.analizOgretmenSorunAlanlari.Where(s => s.gorusmeID == numericGorID).ToList();
                veritabanim.analizOgretmenSorunAlanlari.RemoveRange(silinecek3);

                var silinecek4 = veritabanim.analizOgretmenYapilacakCalismalar.Where(s => s.gorusmeID == numericGorID).ToList();
                veritabanim.analizOgretmenYapilacakCalismalar.RemoveRange(silinecek4);

                try
                {
                    veritabanim.SaveChanges();
                    MessageBox.Show("Silme işlemi başarılı.", "Kayıtlar", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                catch (Exception)
                {
                    MessageBox.Show("Veritabanı hatası. Tekrar Deneyiniz.", "Kayıtlar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            GridTemizle();
        }
        
        private void btnHepsiniGetir_Click(object sender, EventArgs e)
        {
            GridTemizle();
            using (dbEntities veritabanim=new dbEntities())
            {
                var sorgu = veritabanim.gorusmeOgretmen.Join(veritabanim.rehberOgretmenler, h => h.calismayiYapan, rh => rh.rehberID, (h, rh) => new { Gorusme = h, Rehberci = rh }).ToList();
                
                if (sorgu.Count > 0)
                {
                    for (int i = 0; i < sorgu.Count; i++)
                    {
                        string format = "yyyyMMdd";
                        DateTime dt;
                        var z = DateTime.TryParseExact(sorgu[i].Gorusme.gorusmeTarihi, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
                        dataGridView1.Rows.Add(dt.ToString("D"), sorgu[i].Gorusme.ogretmenAdi, sorgu[i].Gorusme.ogretmenBransi, sorgu[i].Rehberci.rehberOgretmenAdSoyad, sorgu[i].Gorusme.gorusmeID);
                    }
                    btnGuncelle.Enabled = true;
                    btnSil.Enabled = true;
                }

            }
        }
        
        private void GridTemizle()
        {
            dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();
            dataGridView1.Refresh();
        }

    }
}
