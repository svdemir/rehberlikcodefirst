﻿namespace rbsProje
{
    partial class frmVeliGorusmeMEB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVeliGorusmeMEB));
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnOgrenciGorusmeYazdir = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkGorusmeKonusu10 = new System.Windows.Forms.CheckBox();
            this.chkGorusmeKonusu7 = new System.Windows.Forms.CheckBox();
            this.chkGorusmeKonusu4 = new System.Windows.Forms.CheckBox();
            this.chkGorusmeKonusu9 = new System.Windows.Forms.CheckBox();
            this.chkGorusmeKonusu3 = new System.Windows.Forms.CheckBox();
            this.chkGorusmeKonusu6 = new System.Windows.Forms.CheckBox();
            this.chkGorusmeKonusu8 = new System.Windows.Forms.CheckBox();
            this.chkGorusmeKonusu2 = new System.Windows.Forms.CheckBox();
            this.chkGorusmeKonusu5 = new System.Windows.Forms.CheckBox();
            this.chkGorusmeKonusu1 = new System.Windows.Forms.CheckBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.clistGorusmeSonuc = new System.Windows.Forms.CheckedListBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.cmbGorusmeyiYapan = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.uCuyeListele21 = new rbsProje.Kontroller.Uyeler.UCuyeListele2();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnOgrenciGorusmeYazdir);
            this.groupBox3.Controls.Add(this.groupBox2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.label47);
            this.groupBox3.Controls.Add(this.label49);
            this.groupBox3.Controls.Add(this.label43);
            this.groupBox3.Controls.Add(this.label42);
            this.groupBox3.Controls.Add(this.richTextBox2);
            this.groupBox3.Controls.Add(this.richTextBox1);
            this.groupBox3.Controls.Add(this.clistGorusmeSonuc);
            this.groupBox3.Controls.Add(this.dateTimePicker1);
            this.groupBox3.Controls.Add(this.cmbGorusmeyiYapan);
            this.groupBox3.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox3.Location = new System.Drawing.Point(20, 265);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(900, 642);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            // 
            // btnOgrenciGorusmeYazdir
            // 
            this.btnOgrenciGorusmeYazdir.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnOgrenciGorusmeYazdir.Image = global::rbsProje.Properties.Resources.printer2;
            this.btnOgrenciGorusmeYazdir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOgrenciGorusmeYazdir.Location = new System.Drawing.Point(759, 15);
            this.btnOgrenciGorusmeYazdir.Name = "btnOgrenciGorusmeYazdir";
            this.btnOgrenciGorusmeYazdir.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.btnOgrenciGorusmeYazdir.Size = new System.Drawing.Size(108, 45);
            this.btnOgrenciGorusmeYazdir.TabIndex = 12;
            this.btnOgrenciGorusmeYazdir.Text = "           Yazdır";
            this.btnOgrenciGorusmeYazdir.UseVisualStyleBackColor = true;
            this.btnOgrenciGorusmeYazdir.Click += new System.EventHandler(this.btnOgrenciGorusmeYazdir_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkGorusmeKonusu10);
            this.groupBox2.Controls.Add(this.chkGorusmeKonusu7);
            this.groupBox2.Controls.Add(this.chkGorusmeKonusu4);
            this.groupBox2.Controls.Add(this.chkGorusmeKonusu9);
            this.groupBox2.Controls.Add(this.chkGorusmeKonusu3);
            this.groupBox2.Controls.Add(this.chkGorusmeKonusu6);
            this.groupBox2.Controls.Add(this.chkGorusmeKonusu8);
            this.groupBox2.Controls.Add(this.chkGorusmeKonusu2);
            this.groupBox2.Controls.Add(this.chkGorusmeKonusu5);
            this.groupBox2.Controls.Add(this.chkGorusmeKonusu1);
            this.groupBox2.Location = new System.Drawing.Point(377, 259);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(511, 128);
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Görüşme Konusu";
            // 
            // chkGorusmeKonusu10
            // 
            this.chkGorusmeKonusu10.AutoSize = true;
            this.chkGorusmeKonusu10.Location = new System.Drawing.Point(426, 58);
            this.chkGorusmeKonusu10.Name = "chkGorusmeKonusu10";
            this.chkGorusmeKonusu10.Size = new System.Drawing.Size(55, 19);
            this.chkGorusmeKonusu10.TabIndex = 1;
            this.chkGorusmeKonusu10.Text = "Diğer";
            this.chkGorusmeKonusu10.UseVisualStyleBackColor = true;
            // 
            // chkGorusmeKonusu7
            // 
            this.chkGorusmeKonusu7.AutoSize = true;
            this.chkGorusmeKonusu7.Location = new System.Drawing.Point(302, 30);
            this.chkGorusmeKonusu7.Name = "chkGorusmeKonusu7";
            this.chkGorusmeKonusu7.Size = new System.Drawing.Size(136, 19);
            this.chkGorusmeKonusu7.TabIndex = 0;
            this.chkGorusmeKonusu7.Text = "Okula Çevreye Uyum";
            this.chkGorusmeKonusu7.UseVisualStyleBackColor = true;
            // 
            // chkGorusmeKonusu4
            // 
            this.chkGorusmeKonusu4.AutoSize = true;
            this.chkGorusmeKonusu4.Location = new System.Drawing.Point(176, 30);
            this.chkGorusmeKonusu4.Name = "chkGorusmeKonusu4";
            this.chkGorusmeKonusu4.Size = new System.Drawing.Size(123, 19);
            this.chkGorusmeKonusu4.TabIndex = 0;
            this.chkGorusmeKonusu4.Text = "Akademik Konular";
            this.chkGorusmeKonusu4.UseVisualStyleBackColor = true;
            // 
            // chkGorusmeKonusu9
            // 
            this.chkGorusmeKonusu9.AutoSize = true;
            this.chkGorusmeKonusu9.Location = new System.Drawing.Point(302, 85);
            this.chkGorusmeKonusu9.Name = "chkGorusmeKonusu9";
            this.chkGorusmeKonusu9.Size = new System.Drawing.Size(111, 19);
            this.chkGorusmeKonusu9.TabIndex = 0;
            this.chkGorusmeKonusu9.Text = "Psikolojik Uyum";
            this.chkGorusmeKonusu9.UseVisualStyleBackColor = true;
            // 
            // chkGorusmeKonusu3
            // 
            this.chkGorusmeKonusu3.AutoSize = true;
            this.chkGorusmeKonusu3.Location = new System.Drawing.Point(17, 89);
            this.chkGorusmeKonusu3.Name = "chkGorusmeKonusu3";
            this.chkGorusmeKonusu3.Size = new System.Drawing.Size(137, 19);
            this.chkGorusmeKonusu3.TabIndex = 0;
            this.chkGorusmeKonusu3.Text = "Yöneltme Yerleştirme";
            this.chkGorusmeKonusu3.UseVisualStyleBackColor = true;
            // 
            // chkGorusmeKonusu6
            // 
            this.chkGorusmeKonusu6.AutoSize = true;
            this.chkGorusmeKonusu6.Location = new System.Drawing.Point(176, 85);
            this.chkGorusmeKonusu6.Name = "chkGorusmeKonusu6";
            this.chkGorusmeKonusu6.Size = new System.Drawing.Size(114, 19);
            this.chkGorusmeKonusu6.TabIndex = 0;
            this.chkGorusmeKonusu6.Text = "Davranış Sorunu";
            this.chkGorusmeKonusu6.UseVisualStyleBackColor = true;
            // 
            // chkGorusmeKonusu8
            // 
            this.chkGorusmeKonusu8.AutoSize = true;
            this.chkGorusmeKonusu8.Location = new System.Drawing.Point(302, 58);
            this.chkGorusmeKonusu8.Name = "chkGorusmeKonusu8";
            this.chkGorusmeKonusu8.Size = new System.Drawing.Size(99, 19);
            this.chkGorusmeKonusu8.TabIndex = 0;
            this.chkGorusmeKonusu8.Text = "Ailevi Konular";
            this.chkGorusmeKonusu8.UseVisualStyleBackColor = true;
            // 
            // chkGorusmeKonusu2
            // 
            this.chkGorusmeKonusu2.AutoSize = true;
            this.chkGorusmeKonusu2.Location = new System.Drawing.Point(17, 58);
            this.chkGorusmeKonusu2.Name = "chkGorusmeKonusu2";
            this.chkGorusmeKonusu2.Size = new System.Drawing.Size(155, 19);
            this.chkGorusmeKonusu2.TabIndex = 0;
            this.chkGorusmeKonusu2.Text = "Sosyoekonomik Konular";
            this.chkGorusmeKonusu2.UseVisualStyleBackColor = true;
            // 
            // chkGorusmeKonusu5
            // 
            this.chkGorusmeKonusu5.AutoSize = true;
            this.chkGorusmeKonusu5.Location = new System.Drawing.Point(176, 58);
            this.chkGorusmeKonusu5.Name = "chkGorusmeKonusu5";
            this.chkGorusmeKonusu5.Size = new System.Drawing.Size(95, 19);
            this.chkGorusmeKonusu5.TabIndex = 0;
            this.chkGorusmeKonusu5.Text = "Sosyal Uyum";
            this.chkGorusmeKonusu5.UseVisualStyleBackColor = true;
            // 
            // chkGorusmeKonusu1
            // 
            this.chkGorusmeKonusu1.AutoSize = true;
            this.chkGorusmeKonusu1.Location = new System.Drawing.Point(17, 30);
            this.chkGorusmeKonusu1.Name = "chkGorusmeKonusu1";
            this.chkGorusmeKonusu1.Size = new System.Drawing.Size(137, 19);
            this.chkGorusmeKonusu1.TabIndex = 0;
            this.chkGorusmeKonusu1.Text = "Sağlıkla İlgili Konular";
            this.chkGorusmeKonusu1.UseVisualStyleBackColor = true;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(11, 73);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(55, 15);
            this.label47.TabIndex = 19;
            this.label47.Text = "Görüşme";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(11, 390);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(80, 15);
            this.label49.TabIndex = 19;
            this.label49.Text = "Sonuç Bigileri";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(374, 24);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(99, 15);
            this.label43.TabIndex = 16;
            this.label43.Text = "Görüşmeyi Yapan";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(28, 24);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(87, 15);
            this.label42.TabIndex = 15;
            this.label42.Text = "Görüşme Tarihi";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(14, 92);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(346, 295);
            this.richTextBox1.TabIndex = 14;
            this.richTextBox1.Text = "";
            // 
            // clistGorusmeSonuc
            // 
            this.clistGorusmeSonuc.CheckOnClick = true;
            this.clistGorusmeSonuc.FormattingEnabled = true;
            this.clistGorusmeSonuc.HorizontalScrollbar = true;
            this.clistGorusmeSonuc.Location = new System.Drawing.Point(10, 411);
            this.clistGorusmeSonuc.Name = "clistGorusmeSonuc";
            this.clistGorusmeSonuc.Size = new System.Drawing.Size(878, 220);
            this.clistGorusmeSonuc.TabIndex = 3;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "";
            this.dateTimePicker1.Location = new System.Drawing.Point(121, 22);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(186, 23);
            this.dateTimePicker1.TabIndex = 2;
            // 
            // cmbGorusmeyiYapan
            // 
            this.cmbGorusmeyiYapan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGorusmeyiYapan.FormattingEnabled = true;
            this.cmbGorusmeyiYapan.Location = new System.Drawing.Point(485, 21);
            this.cmbGorusmeyiYapan.Name = "cmbGorusmeyiYapan";
            this.cmbGorusmeyiYapan.Size = new System.Drawing.Size(159, 23);
            this.cmbGorusmeyiYapan.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::rbsProje.Properties.Resources.veliForm;
            this.pictureBox1.Location = new System.Drawing.Point(926, 110);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(431, 642);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // uCuyeListele21
            // 
            this.uCuyeListele21.Location = new System.Drawing.Point(10, 8);
            this.uCuyeListele21.Name = "uCuyeListele21";
            this.uCuyeListele21.Size = new System.Drawing.Size(910, 251);
            this.uCuyeListele21.TabIndex = 12;
            // 
            // richTextBox2
            // 
            this.richTextBox2.Location = new System.Drawing.Point(377, 92);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(490, 139);
            this.richTextBox2.TabIndex = 14;
            this.richTextBox2.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(374, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 15);
            this.label1.TabIndex = 19;
            this.label1.Text = "İşbirliği Yapılacak Kişi / Kurum";
            // 
            // frmVeliGorusmeMEB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(1419, 886);
            this.Controls.Add(this.uCuyeListele21);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox3);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmVeliGorusmeMEB";
            this.Text = "Veli Görüşme";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmUyeGorusmeMEB_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.RichTextBox richTextBox1;
        public System.Windows.Forms.Button btnOgrenciGorusmeYazdir;
        private System.Windows.Forms.CheckedListBox clistGorusmeSonuc;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.ComboBox cmbGorusmeyiYapan;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox chkGorusmeKonusu10;
        private System.Windows.Forms.CheckBox chkGorusmeKonusu7;
        private System.Windows.Forms.CheckBox chkGorusmeKonusu4;
        private System.Windows.Forms.CheckBox chkGorusmeKonusu9;
        private System.Windows.Forms.CheckBox chkGorusmeKonusu3;
        private System.Windows.Forms.CheckBox chkGorusmeKonusu6;
        private System.Windows.Forms.CheckBox chkGorusmeKonusu8;
        private System.Windows.Forms.CheckBox chkGorusmeKonusu2;
        private System.Windows.Forms.CheckBox chkGorusmeKonusu5;
        private System.Windows.Forms.CheckBox chkGorusmeKonusu1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Kontroller.Uyeler.UCuyeListele2 uCuyeListele21;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox richTextBox2;
    }
}