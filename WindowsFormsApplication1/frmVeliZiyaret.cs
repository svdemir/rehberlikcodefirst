﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;

namespace rbsProje
{
    public partial class frmVeliZiyaret : Form
    {
        private static frmVeliZiyaret instance;

        public static frmVeliZiyaret GetForm
        {
            get
            {
                if (instance == null || instance.IsDisposed)
                {
                    instance = new frmVeliZiyaret();
                }
                else
                {
                    instance.BringToFront();
                }
                return instance;
            }
        }

        public frmVeliZiyaret()
        {
            InitializeComponent();
        }

        public frmVeliZiyaret(long ziyaretID)
        {
            InitializeComponent();
            uCveliZiyaret1.btnVeliZiyaretKaydet.Visible = false;
            btnGuncelle.Visible = true;
            GuncellenecekVeriGetir(ziyaretID);

        }

        long GlobalZiyaretID;

        private void GuncellenecekVeriGetir(long ziyaretID)
        {
            GlobalZiyaretID = ziyaretID;
            using (dbEntities veritabanim = new dbEntities())
            {
                var sorgu = veritabanim.gorusmeVeliZiyaret.Where(z => z.id == ziyaretID).SingleOrDefault();
                if (sorgu != null)
                {
                    uCveliZiyaret1.numericUpDown1.Value = Convert.ToDecimal(sorgu.ogrNo);
                    uCveliZiyaret1.btnGetir_Click(null,null);
                    uCveliZiyaret1.richZiyaretNedeni.Text = sorgu.ziyaretNedeni;
                    uCveliZiyaret1.richMevcutDurum.Text = sorgu.mevcutDurum;
                    uCveliZiyaret1.richSonuc.Text = sorgu.sonucDegerlendirme;

                    string format = "yyyyMMdd";
                    DateTime dt;
                    var z = DateTime.TryParseExact(sorgu.tarih, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
                    uCveliZiyaret1.dateTimePicker1.Value = dt;

                    string[] ziyaretciler = sorgu.ziyaretEdenler.Split(',');

                    for (int i = 0; i < ziyaretciler.Length; i++)
                    {
                        if (ziyaretciler[i]!="")
                        {
                            string[] isimUnvan = ziyaretciler[i].Split('/');

                            if (isimUnvan[1]!="")
                            {
                                uCveliZiyaret1.dataGridView1.Rows.Add(isimUnvan[0], isimUnvan[1]);
                            }
                        }                        
                    }
                    btnGuncelle.Enabled = true;
                }
            }
        }

        private void frmVeliZiyaret_SizeChanged(object sender, EventArgs e)
        {
            if (this.Controls.Count > 0)
            {
                this.Controls[1].Left = (this.Width - this.Controls[1].Width) / 2;
                btnGuncelle.Left = this.Controls[1].Left + 450;

            }
        }

        private void btnGuncelle_Click(object sender, EventArgs e)
        {
            using (dbEntities veritabanim = new dbEntities())
            {
                var sorgu = veritabanim.gorusmeVeliZiyaret.Where(z => z.id == GlobalZiyaretID).SingleOrDefault();
                if (sorgu != null)
                {
                    sorgu.ziyaretNedeni = uCveliZiyaret1.richZiyaretNedeni.Text;
                    sorgu.mevcutDurum=uCveliZiyaret1.richMevcutDurum.Text ;
                    sorgu.sonucDegerlendirme = uCveliZiyaret1.richSonuc.Text;
                    
                    sorgu.tarih = uCveliZiyaret1.dateTimePicker1.Value.ToString("yyyyMMdd");                  

                    string ziyaretciler = "";
                    for (int i = 0; i < uCveliZiyaret1.dataGridView1.RowCount; i++)
                    {
                        if (uCveliZiyaret1.dataGridView1.Rows[i].Cells[0].Value == null)
                        {
                            uCveliZiyaret1.dataGridView1.Rows[i].Cells[0].Value = "";
                        }
                        if (uCveliZiyaret1.dataGridView1.Rows[i].Cells[1].Value == null)
                        {
                            uCveliZiyaret1.dataGridView1.Rows[i].Cells[1].Value = "";
                        }
                        if (uCveliZiyaret1.dataGridView1.Rows[i].Cells[0].Value.ToString() == "" && uCveliZiyaret1.dataGridView1.Rows[i].Cells[1].Value.ToString() == "")
                        {

                        }
                        else
                        {
                            ziyaretciler += uCveliZiyaret1.dataGridView1.Rows[i].Cells[0].Value.ToString() + "/ " + uCveliZiyaret1.dataGridView1.Rows[i].Cells[1].Value.ToString() + ",";
                        }
                    }
                    sorgu.ziyaretEdenler = ziyaretciler;
                }
                try
                {
                    veritabanim.SaveChanges();
                    MessageBox.Show("Ziyaret güncelleme işlemi başarılı.", "Ziyaret Güncelleme", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception)
                {
                    MessageBox.Show("Ziyaret güncelleme işlemi hatalı.", "Ziyaret Güncelleme", MessageBoxButtons.OK, MessageBoxIcon.Error);                    
                }
            }
        }
    }
}
