//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace rbsProje
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class gorusmeOgrenci
    {
        [Key]
        public long gorusmeID { get; set; }
        public Nullable<long> ogrNo { get; set; }
        public Nullable<long> gorusmeTuru { get; set; }
        public Nullable<long> pdrAlani { get; set; }
        public Nullable<long> calismayiYapan { get; set; }
        public string gorusmeTarihi { get; set; }
        public string yonlendirilenKurum { get; set; }
    }
}
