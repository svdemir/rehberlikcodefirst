﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;

namespace rbsProje
{
    public partial class frmVeliGor : Form
    {
        private static frmVeliGor instance;

        public static frmVeliGor GetForm
        {
            get
            {
                if (instance == null || instance.IsDisposed)
                {
                    instance = new frmVeliGor();
                }
                else
                {
                    instance.BringToFront();
                }
                return instance;
            }
        }

        public frmVeliGor()
        {
            InitializeComponent();
        }

        long globalGorusmeID;
        bool guncellemeIslemi = false;

        public frmVeliGor(long gorusmeID, bool guncelleme)
        {
            InitializeComponent();
            uCveliGorusme1.Enabled = false;
            btnGuncelle.Visible = true;
            globalGorusmeID = gorusmeID;
            guncellemeIslemi = guncelleme;
        }

        private void VerileriGetir(long gorusmeID)
        {
            try
            {
                using (dbEntities veritabanim = new dbEntities())
                {
                    var guncellenecekGorusmeObje = veritabanim.gorusmeVeli.Where(g => g.gorusmeID == gorusmeID).SingleOrDefault();
                    long geciciNo = guncellenecekGorusmeObje.ogrNo.Value;
                    uCveliGorusme1.TemelOgrenciBilgileriniDoldur(geciciNo);
                    uCveliGorusme1.DetayliOgrenciBilgileriniDoldur(geciciNo);
                    ((ComboBox)uCveliGorusme1.Controls["groupBox3"].Controls["cmbGorusmeyiYapan"]).SelectedValue = guncellenecekGorusmeObje.calismayiYapan;

                    string format = "yyyyMMdd";
                    DateTime dt;
                    var z = DateTime.TryParseExact(guncellenecekGorusmeObje.gorusmeTarihi, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
                    ((DateTimePicker)uCveliGorusme1.Controls["groupBox3"].Controls["dateTimePicker1"]).Value = dt;


                    var guncellenecekKonuObje = veritabanim.analizVeliGorusmeKonulari.Where(g => g.gorusmeID == gorusmeID).SingleOrDefault();
                    ((RichTextBox)uCveliGorusme1.Controls["groupBox3"].Controls["richTextBox1"]).Text = guncellenecekKonuObje.gorusmeKonusu;


                    var guncellenecekYapCalObje = veritabanim.analizVeliYapilacakCalismalar.Where(g => g.gorusmeID == gorusmeID).ToList();
                    if (guncellenecekYapCalObje.Count > 0)
                    {
                        for (int i = 0; i < ((CheckedListBox)uCveliGorusme1.Controls["groupBox3"].Controls["clistYapilacakCalismalar"]).Items.Count; i++)
                        {
                            for (int j = 0; j < guncellenecekYapCalObje.Count; j++)
                            {
                                if (((kaynakVeliYapilacakCalismalar)((CheckedListBox)uCveliGorusme1.Controls["groupBox3"].Controls["clistYapilacakCalismalar"]).Items[i]).yapilacakID == Convert.ToInt64(guncellenecekYapCalObje[j].yapilacakCalisma))
                                {
                                    ((CheckedListBox)uCveliGorusme1.Controls["groupBox3"].Controls["clistYapilacakCalismalar"]).SetItemChecked(i, true);
                                    ((CheckedListBox)uCveliGorusme1.Controls["groupBox3"].Controls["clistYapilacakCalismalar"]).SetSelected(i, true);
                                }
                            }
                        }
                    }

                    var guncellenecekSorunObje = veritabanim.analizVeliSorunAlanlari.Where(g => g.gorusmeID == gorusmeID).ToList();
                    if (guncellenecekSorunObje.Count > 0)
                    {
                        for (int i = 0; i < ((CheckedListBox)uCveliGorusme1.Controls["groupBox3"].Controls["clistSorunAlanlari"]).Items.Count; i++)
                        {
                            for (int j = 0; j < guncellenecekSorunObje.Count; j++)
                            {
                                if (((kaynakVeliSorunAlanlari)((CheckedListBox)uCveliGorusme1.Controls["groupBox3"].Controls["clistSorunAlanlari"]).Items[i]).alanID == Convert.ToInt64(guncellenecekSorunObje[j].sorunAlani))
                                {
                                    ((CheckedListBox)uCveliGorusme1.Controls["groupBox3"].Controls["clistSorunAlanlari"]).SetItemChecked(i, true);
                                    ((CheckedListBox)uCveliGorusme1.Controls["groupBox3"].Controls["clistSorunAlanlari"]).SetSelected(i, true);
                                }
                            }
                        }
                    }

                    var guncellenecekSonucObje = veritabanim.analizVeliSonucCumleleri.Where(g => g.gorusmeID == gorusmeID).ToList();
                    if (guncellenecekSonucObje.Count > 0)
                    {
                        for (int i = 0; i < ((CheckedListBox)uCveliGorusme1.Controls["groupBox3"].Controls["clistGorusmeSonuc"]).Items.Count; i++)
                        {
                            for (int j = 0; j < guncellenecekSonucObje.Count; j++)
                            {
                                if (((kaynakVeliSonucCumleleri)((CheckedListBox)uCveliGorusme1.Controls["groupBox3"].Controls["clistGorusmeSonuc"]).Items[i]).cumleID == Convert.ToInt64(guncellenecekSonucObje[j].sonucCumlesi))
                                {
                                    ((CheckedListBox)uCveliGorusme1.Controls["groupBox3"].Controls["clistGorusmeSonuc"]).SetItemChecked(i, true);
                                    ((CheckedListBox)uCveliGorusme1.Controls["groupBox3"].Controls["clistGorusmeSonuc"]).SetSelected(i, true);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception )
            {
            }

        }

        private void frmVeliGor_SizeChanged(object sender, EventArgs e)
        {
            if (this.Controls.Count > 0)
            {
                btnGuncelle.Left = this.Controls[1].Left + 590;
                this.Controls[1].Left = (this.Width - this.Controls[1].Width) / 2;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (guncellemeIslemi == true)
            {
                if (((ComboBox)uCveliGorusme1.Controls["groupBox3"].Controls["cmbGorusmeyiYapan"]).Items.Count > 0)
                {
                    VerileriGetir(globalGorusmeID);
                    uCveliGorusme1.Enabled = true;
                    btnGuncelle.Enabled = true;
                    uCveliGorusme1.btnAra.Enabled = false;
                    uCveliGorusme1.btnGorusmeKaydet.Enabled = false;
                    uCveliGorusme1.btnTumunuGetir.Enabled = false;
                    uCveliGorusme1.textBox1.Enabled = false;
                    timer1.Enabled = false;
                }
            }
            else
            {
                timer1.Enabled = false;
            }
        }

        private void frmVeliGor_Load(object sender, EventArgs e)
        {
            uCveliGorusme1.AsenkronVerileriGetir();
        }

        private void btnGuncelle_Click(object sender, EventArgs e)
        {
            GorusmeSil(globalGorusmeID);
            uCveliGorusme1.GorusmeKaydetme(globalGorusmeID.ToString());
        }

        private void GorusmeSil(long gorusmeID)
        {
            using (dbEntities veritabanim = new dbEntities())
            {
                var silinecekObje = veritabanim.gorusmeVeli.Where(s => s.gorusmeID == gorusmeID).SingleOrDefault();
                veritabanim.gorusmeVeli.Remove(silinecekObje);

                var silinecek1 = veritabanim.analizVeliGorusmeKonulari.Where(s => s.gorusmeID == gorusmeID).SingleOrDefault();
                veritabanim.analizVeliGorusmeKonulari.Remove(silinecek1);

                var silinecek2 = veritabanim.analizVeliSonucCumleleri.Where(s => s.gorusmeID == gorusmeID).ToList();
                veritabanim.analizVeliSonucCumleleri.RemoveRange(silinecek2);

                var silinecek3 = veritabanim.analizVeliSorunAlanlari.Where(s => s.gorusmeID == gorusmeID).ToList();
                veritabanim.analizVeliSorunAlanlari.RemoveRange(silinecek3);

                var silinecek4 = veritabanim.analizVeliYapilacakCalismalar.Where(s => s.gorusmeID == gorusmeID).ToList();
                veritabanim.analizVeliYapilacakCalismalar.RemoveRange(silinecek4);

                try
                {
                    veritabanim.SaveChanges();
                }
                catch (Exception)
                {
                    MessageBox.Show("Veritabanı hatası. Tekrar Deneyiniz.", "Kayıtlar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

    }
}
