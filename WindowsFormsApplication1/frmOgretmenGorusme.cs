﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Printing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;

namespace rbsProje
{
    public partial class frmOgretmenGorusme : Form
    {
        public frmOgretmenGorusme()
        {
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
        }

        private static frmOgretmenGorusme instance;

        public static frmOgretmenGorusme GetForm
        {
            get
            {
                if (instance == null || instance.IsDisposed)
                {
                    instance = new frmOgretmenGorusme();
                }
                else
                {
                    instance.BringToFront();
                }
                return instance;
            }
        }


        long globalGorusmeID;
        bool guncellemeIslemi = false;
        long genelOgretmenGorusmeID;


        public frmOgretmenGorusme(long gorusmeID, bool guncelleme)
        {
            InitializeComponent();
            groupBox3.Enabled = false;
            btnGuncelle.Visible = true;
            btnGorusmeKaydet.Visible = false;
            globalGorusmeID = gorusmeID;
            guncellemeIslemi = guncelleme;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (guncellemeIslemi == true)
            {
                if (((ComboBox)this.Controls["groupBox1"].Controls["groupBox3"].Controls["cmbGorusmeyiYapan"]).Items.Count > 0)
                {
                    groupBox3.Enabled = true;
                    VerileriGetir(globalGorusmeID);
                    btnGuncelle.Enabled = true;
                    timer1.Enabled = false;
                }
            }
            else
            {
                timer1.Enabled = false;
            }
        }



        private void VerileriGetir(long gorusmeID)
        {
            try
            {
                using (dbEntities veritabanim = new dbEntities())
                {
                    var guncellenecekGorusmeObje = veritabanim.gorusmeOgretmen.Where(g => g.gorusmeID == gorusmeID).SingleOrDefault();

                    ((ComboBox)this.Controls["groupBox1"].Controls["groupBox3"].Controls["cmbGorusmeyiYapan"]).SelectedValue = guncellenecekGorusmeObje.calismayiYapan;

                    txtOgrAd.Text = guncellenecekGorusmeObje.ogretmenAdi;
                    txtOgrBrans.Text = guncellenecekGorusmeObje.ogretmenBransi;

                    string format = "yyyyMMdd";
                    DateTime dt;
                    var z = DateTime.TryParseExact(guncellenecekGorusmeObje.gorusmeTarihi, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
                    ((DateTimePicker)this.Controls["groupBox1"].Controls["groupBox3"].Controls["dateTimePicker1"]).Value = dt;


                    var guncellenecekKonuObje = veritabanim.analizOgretmenGorusmeKonulari.Where(g => g.gorusmeID == gorusmeID).SingleOrDefault();
                    ((RichTextBox)this.Controls["groupBox1"].Controls["groupBox3"].Controls["richTextBox1"]).Text = guncellenecekKonuObje.gorusmeKonusu;


                    var guncellenecekYapCalObje = veritabanim.analizOgretmenYapilacakCalismalar.Where(g => g.gorusmeID == gorusmeID).ToList();
                    if (guncellenecekYapCalObje.Count > 0)
                    {
                        for (int i = 0; i < ((CheckedListBox)this.Controls["groupBox1"].Controls["groupBox3"].Controls["clistYapilacakCalismalar"]).Items.Count; i++)
                        {
                            for (int j = 0; j < guncellenecekYapCalObje.Count; j++)
                            {
                                if (((kaynakOgretmenYapilacakCalismalar)((CheckedListBox)this.Controls["groupBox1"].Controls["groupBox3"].Controls["clistYapilacakCalismalar"]).Items[i]).yapilacakID == Convert.ToInt64(guncellenecekYapCalObje[j].yapilacakCalisma))
                                {
                                    ((CheckedListBox)this.Controls["groupBox1"].Controls["groupBox3"].Controls["clistYapilacakCalismalar"]).SetItemChecked(i, true);
                                    ((CheckedListBox)this.Controls["groupBox1"].Controls["groupBox3"].Controls["clistYapilacakCalismalar"]).SetSelected(i, true);
                                }
                            }
                        }
                    }



                    var guncellenecekSorunObje = veritabanim.analizOgretmenSorunAlanlari.Where(g => g.gorusmeID == gorusmeID).ToList();
                    if (guncellenecekSorunObje.Count > 0)
                    {
                        for (int i = 0; i < ((CheckedListBox)this.Controls["groupBox1"].Controls["groupBox3"].Controls["clistSorunAlanlari"]).Items.Count; i++)
                        {
                            for (int j = 0; j < guncellenecekSorunObje.Count; j++)
                            {
                                if (((kaynakOgretmenSorunAlanlari)((CheckedListBox)this.Controls["groupBox1"].Controls["groupBox3"].Controls["clistSorunAlanlari"]).Items[i]).alanID == Convert.ToInt64(guncellenecekSorunObje[j].sorunAlani))
                                {
                                    ((CheckedListBox)this.Controls["groupBox1"].Controls["groupBox3"].Controls["clistSorunAlanlari"]).SetItemChecked(i, true);
                                    ((CheckedListBox)this.Controls["groupBox1"].Controls["groupBox3"].Controls["clistSorunAlanlari"]).SetSelected(i, true);
                                }
                            }
                        }
                    }

                    var guncellenecekSonucObje = veritabanim.analizOgretmenSonucCumleleri.Where(g => g.gorusmeID == gorusmeID).ToList();
                    if (guncellenecekSonucObje.Count > 0)
                    {
                        for (int i = 0; i < ((CheckedListBox)this.Controls["groupBox1"].Controls["groupBox3"].Controls["clistGorusmeSonuc"]).Items.Count; i++)
                        {
                            for (int j = 0; j < guncellenecekSonucObje.Count; j++)
                            {
                                if (((kaynakOgretmenSonucCumleleri)((CheckedListBox)this.Controls["groupBox1"].Controls["groupBox3"].Controls["clistGorusmeSonuc"]).Items[i]).cumleID == Convert.ToInt64(guncellenecekSonucObje[j].sonucCumlesi))
                                {
                                    ((CheckedListBox)this.Controls["groupBox1"].Controls["groupBox3"].Controls["clistGorusmeSonuc"]).SetItemChecked(i, true);
                                    ((CheckedListBox)this.Controls["groupBox1"].Controls["groupBox3"].Controls["clistGorusmeSonuc"]).SetSelected(i, true);
                                }
                            }
                        }
                    }

                }
            }
            catch (Exception )
            {
            }

        }

        private void GorusmeSil(long gorusmeID)
        {
            using (dbEntities veritabanim = new dbEntities())
            {
                var silinecekObje = veritabanim.gorusmeOgretmen.Where(s => s.gorusmeID == gorusmeID).SingleOrDefault();
                veritabanim.gorusmeOgretmen.Remove(silinecekObje);

                var silinecek1 = veritabanim.analizOgretmenGorusmeKonulari.Where(s => s.gorusmeID == gorusmeID).SingleOrDefault();
                veritabanim.analizOgretmenGorusmeKonulari.Remove(silinecek1);

                var silinecek2 = veritabanim.analizOgretmenSonucCumleleri.Where(s => s.gorusmeID == gorusmeID).ToList();
                veritabanim.analizOgretmenSonucCumleleri.RemoveRange(silinecek2);

                var silinecek3 = veritabanim.analizSorunAlanlari.Where(s => s.gorusmeID == gorusmeID).ToList();
                veritabanim.analizSorunAlanlari.RemoveRange(silinecek3);

                var silinecek4 = veritabanim.analizOgretmenYapilacakCalismalar.Where(s => s.gorusmeID == gorusmeID).ToList();
                veritabanim.analizOgretmenYapilacakCalismalar.RemoveRange(silinecek4);

                try
                {
                    veritabanim.SaveChanges();

                }
                catch (Exception)
                {
                    MessageBox.Show("Veritabanı hatası. Tekrar Deneyiniz.", "Kayıtlar", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }



        private void frmOgretmenGorusme_SizeChanged(object sender, EventArgs e)
        {
            if (this.Controls.Count > 0)
            {
                //this.Controls[0].Top = ((this.Height - this.Controls[0].Height) / 2);
                this.Controls[0].Left = (this.Width - this.Controls[0].Width) / 2;
            }
        }

        private void DBkaynakVerileriGetir()
        {
            using (dbEntities veritabanim = new dbEntities())
            {
                cmbGorusmeyiYapan.DataSource = veritabanim.rehberOgretmenler.OrderBy(p => p.rehberOgretmenAdSoyad).ToList();
                cmbGorusmeyiYapan.DisplayMember = "rehberOgretmenAdSoyad";
                cmbGorusmeyiYapan.ValueMember = "rehberID";

                clistSorunAlanlari.DataSource = veritabanim.kaynakOgretmenSorunAlanlari.OrderBy(s => s.sorunAlani).ToList();
                clistSorunAlanlari.DisplayMember = "sorunAlani";
                clistSorunAlanlari.ValueMember = "alanID";

                clistYapilacakCalismalar.DataSource = veritabanim.kaynakOgretmenYapilacakCalismalar.OrderBy(y => y.yapilacakCalisma).ToList();
                clistYapilacakCalismalar.DisplayMember = "yapilacakCalisma";
                clistYapilacakCalismalar.ValueMember = "yapilacakID";

                clistGorusmeSonuc.DataSource = veritabanim.kaynakOgretmenSonucCumleleri.OrderBy(s => s.sonucCumlesi).ToList();
                clistGorusmeSonuc.DisplayMember = "sonucCumlesi";
                clistGorusmeSonuc.ValueMember = "cumleID";
            }
        }

        public void GorusmeKaydetme(string pramGorusmeID)
        {

            if (string.IsNullOrEmpty(pramGorusmeID))
            {
                genelOgretmenGorusmeID = Convert.ToInt64(DateTime.Now.ToString("yyyyMMddHHmmssfff"));
            }
            else
            {
                genelOgretmenGorusmeID = Convert.ToInt64(pramGorusmeID);
            }

            string ogrtAdi = "";

            if (!string.IsNullOrEmpty(txtOgrAd.Text))
            {
                ogrtAdi = txtOgrAd.Text;
            }
            else
            {
                MessageBox.Show("Öğretmen adı boş geçilemez.", "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            using (dbEntities veritabanim = new dbEntities())
            {
                try
                {
                    gorusmeOgretmen ogrtGorusmem = new gorusmeOgretmen();
                    ogrtGorusmem.gorusmeID = genelOgretmenGorusmeID;
                    ogrtGorusmem.calismayiYapan = Convert.ToInt64(cmbGorusmeyiYapan.SelectedValue);


                    analizOgretmenGorusmeKonulari konum = new analizOgretmenGorusmeKonulari();
                    konum.gorusmeID = genelOgretmenGorusmeID;
                    konum.ogretmenAdi = ogrtAdi;
                    konum.gorusmeKonusu = richTextBox1.Text;
                    veritabanim.analizOgretmenGorusmeKonulari.Add(konum);

                    for (int i = 0; i < clistGorusmeSonuc.CheckedItems.Count; i++)
                    {
                        analizOgretmenSonucCumleleri asc = new analizOgretmenSonucCumleleri();
                        asc.gorusmeID = genelOgretmenGorusmeID;
                        asc.ogretmenAdi = ogrtAdi;
                        asc.sonucCumlesi = ((kaynakOgretmenSonucCumleleri)clistGorusmeSonuc.CheckedItems[i]).cumleID.ToString();
                        veritabanim.analizOgretmenSonucCumleleri.Add(asc);
                    }

                    ogrtGorusmem.gorusmeTarihi = dateTimePicker1.Value.ToString("yyyyMMdd");
                    ogrtGorusmem.ogretmenAdi = ogrtAdi;
                    ogrtGorusmem.ogretmenBransi = txtOgrBrans.Text;

                    for (int i = 0; i < clistSorunAlanlari.CheckedItems.Count; i++)
                    {
                        analizOgretmenSorunAlanlari sorunAlanim = new analizOgretmenSorunAlanlari();
                        sorunAlanim.gorusmeID = genelOgretmenGorusmeID;
                        sorunAlanim.ogretmenAdi = ogrtAdi;
                        sorunAlanim.sorunAlani = ((kaynakOgretmenSorunAlanlari)clistSorunAlanlari.CheckedItems[i]).alanID.ToString();
                        veritabanim.analizOgretmenSorunAlanlari.Add(sorunAlanim);
                    }

                    for (int i = 0; i < clistYapilacakCalismalar.CheckedItems.Count; i++)
                    {
                        analizOgretmenYapilacakCalismalar islemlerim = new analizOgretmenYapilacakCalismalar();
                        islemlerim.gorusmeID = genelOgretmenGorusmeID;
                        islemlerim.ogretmenAdi = ogrtAdi;
                        islemlerim.yapilacakCalisma = ((kaynakOgretmenYapilacakCalismalar)clistYapilacakCalismalar.CheckedItems[i]).yapilacakID.ToString();
                        veritabanim.analizOgretmenYapilacakCalismalar.Add(islemlerim);
                    }
                    veritabanim.gorusmeOgretmen.Add(ogrtGorusmem);
                    veritabanim.SaveChanges();
                    MessageBox.Show("Görüşme kayıt işlemi başarılı.", "Görüşme Kayıt", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception)
                {
                    MessageBox.Show("Hata. Alanlara dikkat ediniz.", "Görüşme Kayıt", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
        }

        private void KontrolTemizleme()
        {
            txtOgrAd.Text = "";
            txtOgrBrans.Text = "";
            dateTimePicker1.ResetText();
            cmbGorusmeyiYapan.SelectedIndex = 0;
            richTextBox1.ResetText();

            CheckedTemizle(clistGorusmeSonuc);
            CheckedTemizle(clistSorunAlanlari);
            CheckedTemizle(clistYapilacakCalismalar);
        }

        private void CheckedTemizle(CheckedListBox kontrol)
        {
            kontrol.ClearSelected();
            for (int i = 0; i < kontrol.Items.Count; i++)
            {
                kontrol.SetItemChecked(i, false);
            }
        }


        private void btnGuncelle_Click(object sender, EventArgs e)
        {
            GorusmeSil(globalGorusmeID);
            GorusmeKaydetme(globalGorusmeID.ToString());
        }

        private void frmOgretmenGorusme_Load(object sender, EventArgs e)
        {
            txtOgrAd.Focus();
            backgroundWorker1.RunWorkerAsync();
        }

        private void txtOgrAd_TextChanged(object sender, EventArgs e)
        {
            btnGorusmeKaydet.Enabled = true;
            btnOgrenciGorusmeYazdir.Enabled = true;
        }

        private void btnGorusmeKaydet_Click(object sender, EventArgs e)
        {
            GorusmeKaydetme(null);
            KontrolTemizleme();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            DBkaynakVerileriGetir();
        }

        private void btnOgrenciGorusmeYazdir_Click(object sender, EventArgs e)
        {
            PrintDocument doc = new PrintDocument();
            doc.PrintPage += new PrintPageEventHandler(doc_PrintPage);
            PrintPreviewDialog pdg = new PrintPreviewDialog();
            pdg.Document = doc;
            pdg.Height = 700;
            pdg.Width = 900;
            pdg.Show();
        }

        private void doc_PrintPage(object sender, PrintPageEventArgs e)
        {
            e.Graphics.DrawRectangle(new Pen(Color.Black, 3), new Rectangle(60, 80, 700, 1040));  //Genel Çerçeve
            e.Graphics.DrawString("ÖĞRETMEN GÖRÜŞME / PSİKOLOJİK DANIŞMA RAPORU", new System.Drawing.Font(new FontFamily("Segoe UI"), 16, FontStyle.Bold), Brushes.Black, new RectangleF(110, 100, 620, 365));

            string okulAdim;
            using (dbEntities veritabanim = new dbEntities())
            {
                okulAdim = veritabanim.ayarlar.FirstOrDefault().OKULADI;
            }

            StringFormat format = new StringFormat();
            format.LineAlignment = StringAlignment.Center;
            format.Alignment = StringAlignment.Center;
            e.Graphics.DrawString(okulAdim, new System.Drawing.Font(new FontFamily("Segoe UI"), 12, FontStyle.Bold), Brushes.Black, new RectangleF(200, 120, 400, 50), format); //***METİN ORTALAMA

            string ogrenciBilgi = "Öğretmenin Adı Soyadı :   " + txtOgrAd.Text + "                                         " + "Görüşme Tarihi: " + dateTimePicker1.Text + "\n" + "\n" + "Öğretmenin Branşı :   " + txtOgrBrans.Text + "             ";
            e.Graphics.DrawString(ogrenciBilgi, new System.Drawing.Font(new FontFamily("Segoe UI"), 9, FontStyle.Bold), Brushes.Black, new RectangleF(100, 180, 620, 365));

            StringFormat format2 = new StringFormat();
            format2.LineAlignment = StringAlignment.Center;

            string sorunAlaniCumlesi = "";
            for (int i = 0; i < (clistSorunAlanlari.CheckedItems.Count); i++)
            {
                sorunAlaniCumlesi += "\t\u2022 " + ((kaynakOgretmenSorunAlanlari)clistSorunAlanlari.CheckedItems[i]).sorunAlani + "\n";
            }
            e.Graphics.DrawRectangle(new Pen(Color.Black, 1), new Rectangle(85, 270, 650, 140));  //Sorun Alanları  
            e.Graphics.DrawString("Sorun Alanları", new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(85, 250, 650, 140));
            e.Graphics.DrawString(sorunAlaniCumlesi, new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(90, 280, 650, 130), format2);

            e.Graphics.DrawRectangle(new Pen(Color.Black, 1), new Rectangle(85, 445, 650, 180));  //Görüşme Konuları
            e.Graphics.DrawString("Görüşme Konusu", new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(85, 425, 650, 180));
            e.Graphics.DrawString(richTextBox1.Text, new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(90, 455, 650, 170), format2);

            string yapilacakCalismaCumlesi = "";
            for (int i = 0; i < (clistYapilacakCalismalar.CheckedItems.Count); i++)
            {
                yapilacakCalismaCumlesi += "\t\u2022 " + ((kaynakOgretmenYapilacakCalismalar)clistYapilacakCalismalar.CheckedItems[i]).yapilacakCalisma + "\n";
            }

            e.Graphics.DrawRectangle(new Pen(Color.Black, 1), new Rectangle(85, 660, 650, 140));  //Yapılacak Çalışmalar
            e.Graphics.DrawString("Yapılacak Çalışmalar", new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(85, 640, 650, 140));
            e.Graphics.DrawString(yapilacakCalismaCumlesi, new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(90, 670, 650, 130), format2);

            string sonucCumlesi = "";
            for (int i = 0; i < (clistGorusmeSonuc.CheckedItems.Count); i++)
            {
                sonucCumlesi += "\t\u2022 " + ((kaynakOgretmenSonucCumleleri)clistGorusmeSonuc.CheckedItems[i]).sonucCumlesi + "\n";
            }
            e.Graphics.DrawRectangle(new Pen(Color.Black, 1), new Rectangle(85, 830, 650, 180));  //Sonuçlar  
            e.Graphics.DrawString("Sonuç, Görüş ve Öneriler", new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(85, 810, 650, 18));
            e.Graphics.DrawString(sonucCumlesi, new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(90, 820, 620, 150), format2);


            string gorusmeYapanKisi = "Görüşmeyi Yapan" + "\n" + cmbGorusmeyiYapan.Text;
            e.Graphics.DrawString(gorusmeYapanKisi, new System.Drawing.Font(new FontFamily("Segoe UI"), 8, FontStyle.Bold), Brushes.Black, new RectangleF(635, 1070, 130, 130));

        }

    }
}
