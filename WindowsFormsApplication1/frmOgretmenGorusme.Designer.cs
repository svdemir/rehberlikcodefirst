﻿namespace rbsProje
{
    partial class frmOgretmenGorusme
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmOgretmenGorusme));
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnGuncelle = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.btnOgrenciGorusmeYazdir = new System.Windows.Forms.Button();
            this.btnGorusmeKaydet = new System.Windows.Forms.Button();
            this.clistGorusmeSonuc = new System.Windows.Forms.CheckedListBox();
            this.label15 = new System.Windows.Forms.Label();
            this.clistYapilacakCalismalar = new System.Windows.Forms.CheckedListBox();
            this.clistSorunAlanlari = new System.Windows.Forms.CheckedListBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.cmbGorusmeyiYapan = new System.Windows.Forms.ComboBox();
            this.lblVeliGorusmeBaslik = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtOgrBrans = new System.Windows.Forms.TextBox();
            this.txtOgrAd = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnGuncelle);
            this.groupBox3.Controls.Add(this.richTextBox1);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.btnOgrenciGorusmeYazdir);
            this.groupBox3.Controls.Add(this.btnGorusmeKaydet);
            this.groupBox3.Controls.Add(this.clistGorusmeSonuc);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.clistYapilacakCalismalar);
            this.groupBox3.Controls.Add(this.clistSorunAlanlari);
            this.groupBox3.Controls.Add(this.dateTimePicker1);
            this.groupBox3.Controls.Add(this.cmbGorusmeyiYapan);
            this.groupBox3.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox3.Location = new System.Drawing.Point(6, 68);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(972, 584);
            this.groupBox3.TabIndex = 19;
            this.groupBox3.TabStop = false;
            // 
            // btnGuncelle
            // 
            this.btnGuncelle.Enabled = false;
            this.btnGuncelle.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnGuncelle.Image = global::rbsProje.Properties.Resources.edit;
            this.btnGuncelle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuncelle.Location = new System.Drawing.Point(557, 22);
            this.btnGuncelle.Name = "btnGuncelle";
            this.btnGuncelle.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.btnGuncelle.Size = new System.Drawing.Size(101, 40);
            this.btnGuncelle.TabIndex = 17;
            this.btnGuncelle.Text = "         Güncelle";
            this.btnGuncelle.UseVisualStyleBackColor = true;
            this.btnGuncelle.Visible = false;
            this.btnGuncelle.Click += new System.EventHandler(this.btnGuncelle_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(10, 92);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(292, 217);
            this.richTextBox1.TabIndex = 14;
            this.richTextBox1.Text = "";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(610, 71);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(82, 15);
            this.label16.TabIndex = 14;
            this.label16.Text = "Sorun Alanları";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(689, 36);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(99, 15);
            this.label17.TabIndex = 13;
            this.label17.Text = "Görüşmeyi Yapan";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 71);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(55, 15);
            this.label14.TabIndex = 16;
            this.label14.Text = "Görüşme";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(7, 314);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(161, 15);
            this.label20.TabIndex = 10;
            this.label20.Text = "Sonuç ve Görüş Öneri Bigileri";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(14, 33);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(87, 15);
            this.label18.TabIndex = 12;
            this.label18.Text = "Görüşme Tarihi";
            // 
            // btnOgrenciGorusmeYazdir
            // 
            this.btnOgrenciGorusmeYazdir.Enabled = false;
            this.btnOgrenciGorusmeYazdir.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnOgrenciGorusmeYazdir.Image = global::rbsProje.Properties.Resources.printer;
            this.btnOgrenciGorusmeYazdir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOgrenciGorusmeYazdir.Location = new System.Drawing.Point(440, 22);
            this.btnOgrenciGorusmeYazdir.Name = "btnOgrenciGorusmeYazdir";
            this.btnOgrenciGorusmeYazdir.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.btnOgrenciGorusmeYazdir.Size = new System.Drawing.Size(101, 40);
            this.btnOgrenciGorusmeYazdir.TabIndex = 12;
            this.btnOgrenciGorusmeYazdir.Text = "           Yazdır";
            this.btnOgrenciGorusmeYazdir.UseVisualStyleBackColor = true;
            this.btnOgrenciGorusmeYazdir.Click += new System.EventHandler(this.btnOgrenciGorusmeYazdir_Click);
            // 
            // btnGorusmeKaydet
            // 
            this.btnGorusmeKaydet.Enabled = false;
            this.btnGorusmeKaydet.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnGorusmeKaydet.Image = global::rbsProje.Properties.Resources.diskette2;
            this.btnGorusmeKaydet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGorusmeKaydet.Location = new System.Drawing.Point(322, 22);
            this.btnGorusmeKaydet.Name = "btnGorusmeKaydet";
            this.btnGorusmeKaydet.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.btnGorusmeKaydet.Size = new System.Drawing.Size(104, 40);
            this.btnGorusmeKaydet.TabIndex = 13;
            this.btnGorusmeKaydet.Text = "         Kaydet";
            this.btnGorusmeKaydet.UseVisualStyleBackColor = true;
            this.btnGorusmeKaydet.Click += new System.EventHandler(this.btnGorusmeKaydet_Click);
            // 
            // clistGorusmeSonuc
            // 
            this.clistGorusmeSonuc.CheckOnClick = true;
            this.clistGorusmeSonuc.FormattingEnabled = true;
            this.clistGorusmeSonuc.HorizontalScrollbar = true;
            this.clistGorusmeSonuc.Location = new System.Drawing.Point(6, 333);
            this.clistGorusmeSonuc.Name = "clistGorusmeSonuc";
            this.clistGorusmeSonuc.Size = new System.Drawing.Size(947, 238);
            this.clistGorusmeSonuc.TabIndex = 3;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(316, 71);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(113, 15);
            this.label15.TabIndex = 15;
            this.label15.Text = "Yapılacak Çalışmalar";
            // 
            // clistYapilacakCalismalar
            // 
            this.clistYapilacakCalismalar.CheckOnClick = true;
            this.clistYapilacakCalismalar.FormattingEnabled = true;
            this.clistYapilacakCalismalar.HorizontalScrollbar = true;
            this.clistYapilacakCalismalar.Location = new System.Drawing.Point(319, 91);
            this.clistYapilacakCalismalar.Name = "clistYapilacakCalismalar";
            this.clistYapilacakCalismalar.Size = new System.Drawing.Size(279, 220);
            this.clistYapilacakCalismalar.TabIndex = 3;
            // 
            // clistSorunAlanlari
            // 
            this.clistSorunAlanlari.CheckOnClick = true;
            this.clistSorunAlanlari.FormattingEnabled = true;
            this.clistSorunAlanlari.HorizontalScrollbar = true;
            this.clistSorunAlanlari.Location = new System.Drawing.Point(613, 91);
            this.clistSorunAlanlari.Name = "clistSorunAlanlari";
            this.clistSorunAlanlari.Size = new System.Drawing.Size(340, 220);
            this.clistSorunAlanlari.TabIndex = 3;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "";
            this.dateTimePicker1.Location = new System.Drawing.Point(107, 31);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(195, 23);
            this.dateTimePicker1.TabIndex = 2;
            // 
            // cmbGorusmeyiYapan
            // 
            this.cmbGorusmeyiYapan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGorusmeyiYapan.FormattingEnabled = true;
            this.cmbGorusmeyiYapan.Location = new System.Drawing.Point(794, 33);
            this.cmbGorusmeyiYapan.Name = "cmbGorusmeyiYapan";
            this.cmbGorusmeyiYapan.Size = new System.Drawing.Size(159, 23);
            this.cmbGorusmeyiYapan.TabIndex = 1;
            // 
            // lblVeliGorusmeBaslik
            // 
            this.lblVeliGorusmeBaslik.AutoSize = true;
            this.lblVeliGorusmeBaslik.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblVeliGorusmeBaslik.Location = new System.Drawing.Point(396, 0);
            this.lblVeliGorusmeBaslik.Name = "lblVeliGorusmeBaslik";
            this.lblVeliGorusmeBaslik.Size = new System.Drawing.Size(181, 25);
            this.lblVeliGorusmeBaslik.TabIndex = 20;
            this.lblVeliGorusmeBaslik.Text = "Öğretmen Görüşme";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtOgrBrans);
            this.groupBox1.Controls.Add(this.lblVeliGorusmeBaslik);
            this.groupBox1.Controls.Add(this.txtOgrAd);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Location = new System.Drawing.Point(12, 37);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(984, 665);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            // 
            // txtOgrBrans
            // 
            this.txtOgrBrans.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtOgrBrans.Location = new System.Drawing.Point(621, 34);
            this.txtOgrBrans.Name = "txtOgrBrans";
            this.txtOgrBrans.Size = new System.Drawing.Size(163, 23);
            this.txtOgrBrans.TabIndex = 21;
            // 
            // txtOgrAd
            // 
            this.txtOgrAd.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtOgrAd.Location = new System.Drawing.Point(333, 34);
            this.txtOgrAd.Name = "txtOgrAd";
            this.txtOgrAd.Size = new System.Drawing.Size(137, 23);
            this.txtOgrAd.TabIndex = 21;
            this.txtOgrAd.TextChanged += new System.EventHandler(this.txtOgrAd_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.Location = new System.Drawing.Point(500, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 17);
            this.label2.TabIndex = 20;
            this.label2.Text = "Öğretmen Branşı";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(185, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 17);
            this.label1.TabIndex = 20;
            this.label1.Text = "Öğretmen Adı Soyadı";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 3000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // frmOgretmenGorusme
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(1008, 732);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmOgretmenGorusme";
            this.Text = "Öğretmen Görüşme";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmOgretmenGorusme_Load);
            this.SizeChanged += new System.EventHandler(this.frmOgretmenGorusme_SizeChanged);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btnOgrenciGorusmeYazdir;
        public System.Windows.Forms.Button btnGorusmeKaydet;
        private System.Windows.Forms.CheckedListBox clistGorusmeSonuc;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.CheckedListBox clistYapilacakCalismalar;
        private System.Windows.Forms.CheckedListBox clistSorunAlanlari;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.ComboBox cmbGorusmeyiYapan;
        private System.Windows.Forms.Label lblVeliGorusmeBaslik;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtOgrBrans;
        private System.Windows.Forms.TextBox txtOgrAd;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button btnGuncelle;
        private System.Windows.Forms.Timer timer1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
    }
}