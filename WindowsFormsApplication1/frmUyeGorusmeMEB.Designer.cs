﻿namespace rbsProje
{
    partial class frmUyeGorusmeMEB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUyeGorusmeMEB));
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.chkYonKurum5 = new System.Windows.Forms.CheckBox();
            this.chkYonKurum3 = new System.Windows.Forms.CheckBox();
            this.chkYonKurum6 = new System.Windows.Forms.CheckBox();
            this.chkYonKurum2 = new System.Windows.Forms.CheckBox();
            this.chkYonKurum4 = new System.Windows.Forms.CheckBox();
            this.chkYonKurum1 = new System.Windows.Forms.CheckBox();
            this.btnKaydet = new System.Windows.Forms.Button();
            this.btnOgrenciGorusmeYazdir = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkGorusmeKonusu10 = new System.Windows.Forms.CheckBox();
            this.chkGorusmeKonusu7 = new System.Windows.Forms.CheckBox();
            this.chkGorusmeKonusu4 = new System.Windows.Forms.CheckBox();
            this.chkGorusmeKonusu9 = new System.Windows.Forms.CheckBox();
            this.chkGorusmeKonusu3 = new System.Windows.Forms.CheckBox();
            this.chkGorusmeKonusu6 = new System.Windows.Forms.CheckBox();
            this.chkGorusmeKonusu8 = new System.Windows.Forms.CheckBox();
            this.chkGorusmeKonusu2 = new System.Windows.Forms.CheckBox();
            this.chkGorusmeKonusu5 = new System.Windows.Forms.CheckBox();
            this.chkGorusmeKonusu1 = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.chkYapilanCalisma4 = new System.Windows.Forms.CheckBox();
            this.chkYapilanCalisma3 = new System.Windows.Forms.CheckBox();
            this.chkYapilanCalisma2 = new System.Windows.Forms.CheckBox();
            this.chkYapilanCalisma1 = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkBasvuru4 = new System.Windows.Forms.CheckBox();
            this.chkBasvuru3 = new System.Windows.Forms.CheckBox();
            this.chkBasvuru6 = new System.Windows.Forms.CheckBox();
            this.chkBasvuru2 = new System.Windows.Forms.CheckBox();
            this.chkBasvuru5 = new System.Windows.Forms.CheckBox();
            this.chkBasvuru1 = new System.Windows.Forms.CheckBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.clistGorusmeSonuc = new System.Windows.Forms.CheckedListBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.cmbGorusmeyiYapan = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.uCuyeListele21 = new rbsProje.Kontroller.Uyeler.UCuyeListele2();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.groupBox5);
            this.groupBox3.Controls.Add(this.btnKaydet);
            this.groupBox3.Controls.Add(this.btnOgrenciGorusmeYazdir);
            this.groupBox3.Controls.Add(this.groupBox2);
            this.groupBox3.Controls.Add(this.groupBox4);
            this.groupBox3.Controls.Add(this.groupBox1);
            this.groupBox3.Controls.Add(this.label47);
            this.groupBox3.Controls.Add(this.label49);
            this.groupBox3.Controls.Add(this.label43);
            this.groupBox3.Controls.Add(this.label42);
            this.groupBox3.Controls.Add(this.richTextBox1);
            this.groupBox3.Controls.Add(this.clistGorusmeSonuc);
            this.groupBox3.Controls.Add(this.dateTimePicker1);
            this.groupBox3.Controls.Add(this.cmbGorusmeyiYapan);
            this.groupBox3.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.groupBox3.Location = new System.Drawing.Point(20, 265);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(900, 642);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.chkYonKurum5);
            this.groupBox5.Controls.Add(this.chkYonKurum3);
            this.groupBox5.Controls.Add(this.chkYonKurum6);
            this.groupBox5.Controls.Add(this.chkYonKurum2);
            this.groupBox5.Controls.Add(this.chkYonKurum4);
            this.groupBox5.Controls.Add(this.chkYonKurum1);
            this.groupBox5.Location = new System.Drawing.Point(377, 345);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(511, 57);
            this.groupBox5.TabIndex = 20;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Varsa Yönlendirildiği Kurum";
            // 
            // chkYonKurum5
            // 
            this.chkYonKurum5.AutoSize = true;
            this.chkYonKurum5.Location = new System.Drawing.Point(340, 25);
            this.chkYonKurum5.Name = "chkYonKurum5";
            this.chkYonKurum5.Size = new System.Drawing.Size(48, 19);
            this.chkYonKurum5.TabIndex = 0;
            this.chkYonKurum5.Text = "ÇİM";
            this.chkYonKurum5.UseVisualStyleBackColor = true;
            // 
            // chkYonKurum3
            // 
            this.chkYonKurum3.AutoSize = true;
            this.chkYonKurum3.Location = new System.Drawing.Point(185, 25);
            this.chkYonKurum3.Name = "chkYonKurum3";
            this.chkYonKurum3.Size = new System.Drawing.Size(63, 19);
            this.chkYonKurum3.TabIndex = 0;
            this.chkYonKurum3.Text = "ASPİM";
            this.chkYonKurum3.UseVisualStyleBackColor = true;
            // 
            // chkYonKurum6
            // 
            this.chkYonKurum6.AutoSize = true;
            this.chkYonKurum6.Location = new System.Drawing.Point(412, 25);
            this.chkYonKurum6.Name = "chkYonKurum6";
            this.chkYonKurum6.Size = new System.Drawing.Size(60, 19);
            this.chkYonKurum6.TabIndex = 0;
            this.chkYonKurum6.Text = "DİĞER";
            this.chkYonKurum6.UseVisualStyleBackColor = true;
            // 
            // chkYonKurum2
            // 
            this.chkYonKurum2.AutoSize = true;
            this.chkYonKurum2.Location = new System.Drawing.Point(98, 25);
            this.chkYonKurum2.Name = "chkYonKurum2";
            this.chkYonKurum2.Size = new System.Drawing.Size(59, 19);
            this.chkYonKurum2.TabIndex = 0;
            this.chkYonKurum2.Text = "HAST.";
            this.chkYonKurum2.UseVisualStyleBackColor = true;
            // 
            // chkYonKurum4
            // 
            this.chkYonKurum4.AutoSize = true;
            this.chkYonKurum4.Location = new System.Drawing.Point(268, 25);
            this.chkYonKurum4.Name = "chkYonKurum4";
            this.chkYonKurum4.Size = new System.Drawing.Size(47, 19);
            this.chkYonKurum4.TabIndex = 0;
            this.chkYonKurum4.Text = "STK";
            this.chkYonKurum4.UseVisualStyleBackColor = true;
            // 
            // chkYonKurum1
            // 
            this.chkYonKurum1.AutoSize = true;
            this.chkYonKurum1.Location = new System.Drawing.Point(26, 25);
            this.chkYonKurum1.Name = "chkYonKurum1";
            this.chkYonKurum1.Size = new System.Drawing.Size(52, 19);
            this.chkYonKurum1.TabIndex = 0;
            this.chkYonKurum1.Text = "RAM";
            this.chkYonKurum1.UseVisualStyleBackColor = true;
            // 
            // btnKaydet
            // 
            this.btnKaydet.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnKaydet.Image = global::rbsProje.Properties.Resources.kaydet4;
            this.btnKaydet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnKaydet.Location = new System.Drawing.Point(666, 21);
            this.btnKaydet.Name = "btnKaydet";
            this.btnKaydet.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.btnKaydet.Size = new System.Drawing.Size(108, 45);
            this.btnKaydet.TabIndex = 12;
            this.btnKaydet.Text = "           Kaydet";
            this.btnKaydet.UseVisualStyleBackColor = true;
            this.btnKaydet.Click += new System.EventHandler(this.btnKaydet_Click);
            // 
            // btnOgrenciGorusmeYazdir
            // 
            this.btnOgrenciGorusmeYazdir.Font = new System.Drawing.Font("Segoe UI Semibold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnOgrenciGorusmeYazdir.Image = global::rbsProje.Properties.Resources.printer2;
            this.btnOgrenciGorusmeYazdir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOgrenciGorusmeYazdir.Location = new System.Drawing.Point(780, 21);
            this.btnOgrenciGorusmeYazdir.Name = "btnOgrenciGorusmeYazdir";
            this.btnOgrenciGorusmeYazdir.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.btnOgrenciGorusmeYazdir.Size = new System.Drawing.Size(108, 45);
            this.btnOgrenciGorusmeYazdir.TabIndex = 12;
            this.btnOgrenciGorusmeYazdir.Text = "           Yazdır";
            this.btnOgrenciGorusmeYazdir.UseVisualStyleBackColor = true;
            this.btnOgrenciGorusmeYazdir.Click += new System.EventHandler(this.btnOgrenciGorusmeYazdir_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkGorusmeKonusu10);
            this.groupBox2.Controls.Add(this.chkGorusmeKonusu7);
            this.groupBox2.Controls.Add(this.chkGorusmeKonusu4);
            this.groupBox2.Controls.Add(this.chkGorusmeKonusu9);
            this.groupBox2.Controls.Add(this.chkGorusmeKonusu3);
            this.groupBox2.Controls.Add(this.chkGorusmeKonusu6);
            this.groupBox2.Controls.Add(this.chkGorusmeKonusu8);
            this.groupBox2.Controls.Add(this.chkGorusmeKonusu2);
            this.groupBox2.Controls.Add(this.chkGorusmeKonusu5);
            this.groupBox2.Controls.Add(this.chkGorusmeKonusu1);
            this.groupBox2.Location = new System.Drawing.Point(377, 211);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(511, 128);
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Görüşme Konusu";
            // 
            // chkGorusmeKonusu10
            // 
            this.chkGorusmeKonusu10.AutoSize = true;
            this.chkGorusmeKonusu10.Location = new System.Drawing.Point(426, 58);
            this.chkGorusmeKonusu10.Name = "chkGorusmeKonusu10";
            this.chkGorusmeKonusu10.Size = new System.Drawing.Size(55, 19);
            this.chkGorusmeKonusu10.TabIndex = 1;
            this.chkGorusmeKonusu10.Text = "Diğer";
            this.chkGorusmeKonusu10.UseVisualStyleBackColor = true;
            // 
            // chkGorusmeKonusu7
            // 
            this.chkGorusmeKonusu7.AutoSize = true;
            this.chkGorusmeKonusu7.Location = new System.Drawing.Point(302, 30);
            this.chkGorusmeKonusu7.Name = "chkGorusmeKonusu7";
            this.chkGorusmeKonusu7.Size = new System.Drawing.Size(136, 19);
            this.chkGorusmeKonusu7.TabIndex = 0;
            this.chkGorusmeKonusu7.Text = "Okula Çevreye Uyum";
            this.chkGorusmeKonusu7.UseVisualStyleBackColor = true;
            // 
            // chkGorusmeKonusu4
            // 
            this.chkGorusmeKonusu4.AutoSize = true;
            this.chkGorusmeKonusu4.Location = new System.Drawing.Point(176, 30);
            this.chkGorusmeKonusu4.Name = "chkGorusmeKonusu4";
            this.chkGorusmeKonusu4.Size = new System.Drawing.Size(123, 19);
            this.chkGorusmeKonusu4.TabIndex = 0;
            this.chkGorusmeKonusu4.Text = "Akademik Konular";
            this.chkGorusmeKonusu4.UseVisualStyleBackColor = true;
            // 
            // chkGorusmeKonusu9
            // 
            this.chkGorusmeKonusu9.AutoSize = true;
            this.chkGorusmeKonusu9.Location = new System.Drawing.Point(302, 85);
            this.chkGorusmeKonusu9.Name = "chkGorusmeKonusu9";
            this.chkGorusmeKonusu9.Size = new System.Drawing.Size(111, 19);
            this.chkGorusmeKonusu9.TabIndex = 0;
            this.chkGorusmeKonusu9.Text = "Psikolojik Uyum";
            this.chkGorusmeKonusu9.UseVisualStyleBackColor = true;
            // 
            // chkGorusmeKonusu3
            // 
            this.chkGorusmeKonusu3.AutoSize = true;
            this.chkGorusmeKonusu3.Location = new System.Drawing.Point(17, 89);
            this.chkGorusmeKonusu3.Name = "chkGorusmeKonusu3";
            this.chkGorusmeKonusu3.Size = new System.Drawing.Size(137, 19);
            this.chkGorusmeKonusu3.TabIndex = 0;
            this.chkGorusmeKonusu3.Text = "Yöneltme Yerleştirme";
            this.chkGorusmeKonusu3.UseVisualStyleBackColor = true;
            // 
            // chkGorusmeKonusu6
            // 
            this.chkGorusmeKonusu6.AutoSize = true;
            this.chkGorusmeKonusu6.Location = new System.Drawing.Point(176, 85);
            this.chkGorusmeKonusu6.Name = "chkGorusmeKonusu6";
            this.chkGorusmeKonusu6.Size = new System.Drawing.Size(114, 19);
            this.chkGorusmeKonusu6.TabIndex = 0;
            this.chkGorusmeKonusu6.Text = "Davranış Sorunu";
            this.chkGorusmeKonusu6.UseVisualStyleBackColor = true;
            // 
            // chkGorusmeKonusu8
            // 
            this.chkGorusmeKonusu8.AutoSize = true;
            this.chkGorusmeKonusu8.Location = new System.Drawing.Point(302, 58);
            this.chkGorusmeKonusu8.Name = "chkGorusmeKonusu8";
            this.chkGorusmeKonusu8.Size = new System.Drawing.Size(99, 19);
            this.chkGorusmeKonusu8.TabIndex = 0;
            this.chkGorusmeKonusu8.Text = "Ailevi Konular";
            this.chkGorusmeKonusu8.UseVisualStyleBackColor = true;
            // 
            // chkGorusmeKonusu2
            // 
            this.chkGorusmeKonusu2.AutoSize = true;
            this.chkGorusmeKonusu2.Location = new System.Drawing.Point(17, 58);
            this.chkGorusmeKonusu2.Name = "chkGorusmeKonusu2";
            this.chkGorusmeKonusu2.Size = new System.Drawing.Size(155, 19);
            this.chkGorusmeKonusu2.TabIndex = 0;
            this.chkGorusmeKonusu2.Text = "Sosyoekonomik Konular";
            this.chkGorusmeKonusu2.UseVisualStyleBackColor = true;
            // 
            // chkGorusmeKonusu5
            // 
            this.chkGorusmeKonusu5.AutoSize = true;
            this.chkGorusmeKonusu5.Location = new System.Drawing.Point(176, 58);
            this.chkGorusmeKonusu5.Name = "chkGorusmeKonusu5";
            this.chkGorusmeKonusu5.Size = new System.Drawing.Size(95, 19);
            this.chkGorusmeKonusu5.TabIndex = 0;
            this.chkGorusmeKonusu5.Text = "Sosyal Uyum";
            this.chkGorusmeKonusu5.UseVisualStyleBackColor = true;
            // 
            // chkGorusmeKonusu1
            // 
            this.chkGorusmeKonusu1.AutoSize = true;
            this.chkGorusmeKonusu1.Location = new System.Drawing.Point(17, 30);
            this.chkGorusmeKonusu1.Name = "chkGorusmeKonusu1";
            this.chkGorusmeKonusu1.Size = new System.Drawing.Size(137, 19);
            this.chkGorusmeKonusu1.TabIndex = 0;
            this.chkGorusmeKonusu1.Text = "Sağlıkla İlgili Konular";
            this.chkGorusmeKonusu1.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.chkYapilanCalisma4);
            this.groupBox4.Controls.Add(this.chkYapilanCalisma3);
            this.groupBox4.Controls.Add(this.chkYapilanCalisma2);
            this.groupBox4.Controls.Add(this.chkYapilanCalisma1);
            this.groupBox4.Location = new System.Drawing.Point(702, 74);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(186, 128);
            this.groupBox4.TabIndex = 20;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Yapılan Çalışma";
            // 
            // chkYapilanCalisma4
            // 
            this.chkYapilanCalisma4.AutoSize = true;
            this.chkYapilanCalisma4.Location = new System.Drawing.Point(13, 103);
            this.chkYapilanCalisma4.Name = "chkYapilanCalisma4";
            this.chkYapilanCalisma4.Size = new System.Drawing.Size(169, 19);
            this.chkYapilanCalisma4.TabIndex = 0;
            this.chkYapilanCalisma4.Text = "Bireysel Psikolojik Danışma";
            this.chkYapilanCalisma4.UseVisualStyleBackColor = true;
            // 
            // chkYapilanCalisma3
            // 
            this.chkYapilanCalisma3.AutoSize = true;
            this.chkYapilanCalisma3.Location = new System.Drawing.Point(13, 79);
            this.chkYapilanCalisma3.Name = "chkYapilanCalisma3";
            this.chkYapilanCalisma3.Size = new System.Drawing.Size(118, 19);
            this.chkYapilanCalisma3.TabIndex = 0;
            this.chkYapilanCalisma3.Text = "Mesleki Rehberlik";
            this.chkYapilanCalisma3.UseVisualStyleBackColor = true;
            // 
            // chkYapilanCalisma2
            // 
            this.chkYapilanCalisma2.AutoSize = true;
            this.chkYapilanCalisma2.Location = new System.Drawing.Point(13, 54);
            this.chkYapilanCalisma2.Name = "chkYapilanCalisma2";
            this.chkYapilanCalisma2.Size = new System.Drawing.Size(112, 19);
            this.chkYapilanCalisma2.TabIndex = 0;
            this.chkYapilanCalisma2.Text = "Eğitsel Rehberlik";
            this.chkYapilanCalisma2.UseVisualStyleBackColor = true;
            // 
            // chkYapilanCalisma1
            // 
            this.chkYapilanCalisma1.AutoSize = true;
            this.chkYapilanCalisma1.Location = new System.Drawing.Point(13, 30);
            this.chkYapilanCalisma1.Name = "chkYapilanCalisma1";
            this.chkYapilanCalisma1.Size = new System.Drawing.Size(110, 19);
            this.chkYapilanCalisma1.TabIndex = 0;
            this.chkYapilanCalisma1.Text = "Kişisel Rehberlik";
            this.chkYapilanCalisma1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkBasvuru4);
            this.groupBox1.Controls.Add(this.chkBasvuru3);
            this.groupBox1.Controls.Add(this.chkBasvuru6);
            this.groupBox1.Controls.Add(this.chkBasvuru2);
            this.groupBox1.Controls.Add(this.chkBasvuru5);
            this.groupBox1.Controls.Add(this.chkBasvuru1);
            this.groupBox1.Location = new System.Drawing.Point(377, 74);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(314, 128);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Başvurma Şekli";
            // 
            // chkBasvuru4
            // 
            this.chkBasvuru4.AutoSize = true;
            this.chkBasvuru4.Location = new System.Drawing.Point(153, 30);
            this.chkBasvuru4.Name = "chkBasvuru4";
            this.chkBasvuru4.Size = new System.Drawing.Size(135, 19);
            this.chkBasvuru4.TabIndex = 0;
            this.chkBasvuru4.Text = "Arkadaşının İsteğiyle";
            this.chkBasvuru4.UseVisualStyleBackColor = true;
            // 
            // chkBasvuru3
            // 
            this.chkBasvuru3.AutoSize = true;
            this.chkBasvuru3.Location = new System.Drawing.Point(11, 89);
            this.chkBasvuru3.Name = "chkBasvuru3";
            this.chkBasvuru3.Size = new System.Drawing.Size(117, 19);
            this.chkBasvuru3.TabIndex = 0;
            this.chkBasvuru3.Text = "Velisinin İsteğiyle";
            this.chkBasvuru3.UseVisualStyleBackColor = true;
            // 
            // chkBasvuru6
            // 
            this.chkBasvuru6.AutoSize = true;
            this.chkBasvuru6.Location = new System.Drawing.Point(153, 89);
            this.chkBasvuru6.Name = "chkBasvuru6";
            this.chkBasvuru6.Size = new System.Drawing.Size(72, 19);
            this.chkBasvuru6.TabIndex = 0;
            this.chkBasvuru6.Text = "Diğer......";
            this.chkBasvuru6.UseVisualStyleBackColor = true;
            // 
            // chkBasvuru2
            // 
            this.chkBasvuru2.AutoSize = true;
            this.chkBasvuru2.Location = new System.Drawing.Point(11, 58);
            this.chkBasvuru2.Name = "chkBasvuru2";
            this.chkBasvuru2.Size = new System.Drawing.Size(137, 19);
            this.chkBasvuru2.TabIndex = 0;
            this.chkBasvuru2.Text = "Öğretmenin İsteğiyle";
            this.chkBasvuru2.UseVisualStyleBackColor = true;
            // 
            // chkBasvuru5
            // 
            this.chkBasvuru5.AutoSize = true;
            this.chkBasvuru5.Location = new System.Drawing.Point(153, 58);
            this.chkBasvuru5.Name = "chkBasvuru5";
            this.chkBasvuru5.Size = new System.Drawing.Size(153, 19);
            this.chkBasvuru5.TabIndex = 0;
            this.chkBasvuru5.Text = "Okul İdaresinin İsteğiyle";
            this.chkBasvuru5.UseVisualStyleBackColor = true;
            // 
            // chkBasvuru1
            // 
            this.chkBasvuru1.AutoSize = true;
            this.chkBasvuru1.Location = new System.Drawing.Point(11, 30);
            this.chkBasvuru1.Name = "chkBasvuru1";
            this.chkBasvuru1.Size = new System.Drawing.Size(103, 19);
            this.chkBasvuru1.TabIndex = 0;
            this.chkBasvuru1.Text = "Kendi İsteğiyle";
            this.chkBasvuru1.UseVisualStyleBackColor = true;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(11, 73);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(55, 15);
            this.label47.TabIndex = 19;
            this.label47.Text = "Görüşme";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(11, 390);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(80, 15);
            this.label49.TabIndex = 19;
            this.label49.Text = "Sonuç Bigileri";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(374, 24);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(99, 15);
            this.label43.TabIndex = 16;
            this.label43.Text = "Görüşmeyi Yapan";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(28, 24);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(87, 15);
            this.label42.TabIndex = 15;
            this.label42.Text = "Görüşme Tarihi";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(14, 92);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(346, 295);
            this.richTextBox1.TabIndex = 14;
            this.richTextBox1.Text = "";
            // 
            // clistGorusmeSonuc
            // 
            this.clistGorusmeSonuc.CheckOnClick = true;
            this.clistGorusmeSonuc.FormattingEnabled = true;
            this.clistGorusmeSonuc.HorizontalScrollbar = true;
            this.clistGorusmeSonuc.Location = new System.Drawing.Point(10, 411);
            this.clistGorusmeSonuc.Name = "clistGorusmeSonuc";
            this.clistGorusmeSonuc.Size = new System.Drawing.Size(878, 220);
            this.clistGorusmeSonuc.TabIndex = 3;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "";
            this.dateTimePicker1.Location = new System.Drawing.Point(121, 22);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(186, 23);
            this.dateTimePicker1.TabIndex = 2;
            // 
            // cmbGorusmeyiYapan
            // 
            this.cmbGorusmeyiYapan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGorusmeyiYapan.FormattingEnabled = true;
            this.cmbGorusmeyiYapan.Location = new System.Drawing.Point(485, 21);
            this.cmbGorusmeyiYapan.Name = "cmbGorusmeyiYapan";
            this.cmbGorusmeyiYapan.Size = new System.Drawing.Size(159, 23);
            this.cmbGorusmeyiYapan.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::rbsProje.Properties.Resources.ogrenciForm;
            this.pictureBox1.Location = new System.Drawing.Point(926, 110);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(431, 642);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // uCuyeListele21
            // 
            this.uCuyeListele21.Location = new System.Drawing.Point(10, 8);
            this.uCuyeListele21.Name = "uCuyeListele21";
            this.uCuyeListele21.Size = new System.Drawing.Size(910, 251);
            this.uCuyeListele21.TabIndex = 12;
            // 
            // frmUyeGorusmeMEB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(1419, 886);
            this.Controls.Add(this.uCuyeListele21);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox3);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmUyeGorusmeMEB";
            this.Text = "Öğrenci Görüşme";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmUyeGorusmeMEB_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.RichTextBox richTextBox1;
        public System.Windows.Forms.Button btnOgrenciGorusmeYazdir;
        private System.Windows.Forms.CheckedListBox clistGorusmeSonuc;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.ComboBox cmbGorusmeyiYapan;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chkBasvuru4;
        private System.Windows.Forms.CheckBox chkBasvuru3;
        private System.Windows.Forms.CheckBox chkBasvuru6;
        private System.Windows.Forms.CheckBox chkBasvuru2;
        private System.Windows.Forms.CheckBox chkBasvuru5;
        private System.Windows.Forms.CheckBox chkBasvuru1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.CheckBox chkYonKurum5;
        private System.Windows.Forms.CheckBox chkYonKurum3;
        private System.Windows.Forms.CheckBox chkYonKurum6;
        private System.Windows.Forms.CheckBox chkYonKurum2;
        private System.Windows.Forms.CheckBox chkYonKurum4;
        private System.Windows.Forms.CheckBox chkYonKurum1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox chkGorusmeKonusu10;
        private System.Windows.Forms.CheckBox chkGorusmeKonusu7;
        private System.Windows.Forms.CheckBox chkGorusmeKonusu4;
        private System.Windows.Forms.CheckBox chkGorusmeKonusu9;
        private System.Windows.Forms.CheckBox chkGorusmeKonusu3;
        private System.Windows.Forms.CheckBox chkGorusmeKonusu6;
        private System.Windows.Forms.CheckBox chkGorusmeKonusu8;
        private System.Windows.Forms.CheckBox chkGorusmeKonusu2;
        private System.Windows.Forms.CheckBox chkGorusmeKonusu5;
        private System.Windows.Forms.CheckBox chkGorusmeKonusu1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox chkYapilanCalisma4;
        private System.Windows.Forms.CheckBox chkYapilanCalisma3;
        private System.Windows.Forms.CheckBox chkYapilanCalisma2;
        private System.Windows.Forms.CheckBox chkYapilanCalisma1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Kontroller.Uyeler.UCuyeListele2 uCuyeListele21;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        public System.Windows.Forms.Button btnKaydet;
    }
}