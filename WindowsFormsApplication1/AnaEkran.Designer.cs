﻿namespace rbsProje
{
    partial class AnaEkran
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AnaEkran));
            this.ribbon1 = new System.Windows.Forms.Ribbon();
            this.ribbonSeparator1 = new System.Windows.Forms.RibbonSeparator();
            this.ribbonSeparator2 = new System.Windows.Forms.RibbonSeparator();
            this.ribbonSeparator3 = new System.Windows.Forms.RibbonSeparator();
            this.ribbonSeparator4 = new System.Windows.Forms.RibbonSeparator();
            this.ribbonSeparator6 = new System.Windows.Forms.RibbonSeparator();
            this.ribbonTab1 = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel1 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel2 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel11 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel17 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel12 = new System.Windows.Forms.RibbonPanel();
            this.ribbonTab2 = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel3 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel4 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel13 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel5 = new System.Windows.Forms.RibbonPanel();
            this.ribbonTab4 = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel6 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel14 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel7 = new System.Windows.Forms.RibbonPanel();
            this.ribbonTab5 = new System.Windows.Forms.RibbonTab();
            this.ribbonPanel10 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel20 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel15 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel16 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel8 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel9 = new System.Windows.Forms.RibbonPanel();
            this.ribbonPanel18 = new System.Windows.Forms.RibbonPanel();
            this.anaPanel = new System.Windows.Forms.Panel();
            this.ribbonOrbMenuItem1 = new System.Windows.Forms.RibbonOrbMenuItem();
            this.ribbonOrbMenuItem2 = new System.Windows.Forms.RibbonOrbMenuItem();
            this.ribbonOrbMenuItem3 = new System.Windows.Forms.RibbonOrbMenuItem();
            this.ribbonOrbMenuItem4 = new System.Windows.Forms.RibbonOrbMenuItem();
            this.ribbonOrbMenuItem6 = new System.Windows.Forms.RibbonOrbMenuItem();
            this.ribbonOrbMenuItem7 = new System.Windows.Forms.RibbonOrbMenuItem();
            this.btnOgrenci = new System.Windows.Forms.RibbonButton();
            this.btnOgrenciGorusme = new System.Windows.Forms.RibbonButton();
            this.btnGorusmeKayitlari = new System.Windows.Forms.RibbonButton();
            this.btnGorusmeMebForm = new System.Windows.Forms.RibbonButton();
            this.btnOgrenciIstatistik = new System.Windows.Forms.RibbonButton();
            this.btnVeliGorusme = new System.Windows.Forms.RibbonButton();
            this.btnVeliGorusmeMEB = new System.Windows.Forms.RibbonButton();
            this.btnVeliZiyaret = new System.Windows.Forms.RibbonButton();
            this.btnVeliKayitlari = new System.Windows.Forms.RibbonButton();
            this.btnVeliIstatistik = new System.Windows.Forms.RibbonButton();
            this.btnOgrtGorusme = new System.Windows.Forms.RibbonButton();
            this.btnOgretmenKayitlari = new System.Windows.Forms.RibbonButton();
            this.btnOgretmenIstatistik = new System.Windows.Forms.RibbonButton();
            this.btnGenelAyarlar = new System.Windows.Forms.RibbonButton();
            this.ribbonButton2 = new System.Windows.Forms.RibbonButton();
            this.btnSabitleriDuzenle = new System.Windows.Forms.RibbonButton();
            this.ribbonButton1 = new System.Windows.Forms.RibbonButton();
            this.btnKitapOkuyanlar = new System.Windows.Forms.RibbonButton();
            this.btnOkunanKitaplar = new System.Windows.Forms.RibbonButton();
            this.ribbonButton3 = new System.Windows.Forms.RibbonButton();
            this.ribbonButton4 = new System.Windows.Forms.RibbonButton();
            this.SuspendLayout();
            // 
            // ribbon1
            // 
            this.ribbon1.CaptionBarVisible = false;
            this.ribbon1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.ribbon1.Location = new System.Drawing.Point(0, 0);
            this.ribbon1.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.ribbon1.Minimized = false;
            this.ribbon1.Name = "ribbon1";
            // 
            // 
            // 
            this.ribbon1.OrbDropDown.BorderRoundness = 8;
            this.ribbon1.OrbDropDown.CausesValidation = false;
            this.ribbon1.OrbDropDown.ContentRecentItemsMinWidth = 0;
            this.ribbon1.OrbDropDown.Font = new System.Drawing.Font("Segoe UI", 25F, System.Drawing.FontStyle.Bold);
            this.ribbon1.OrbDropDown.Location = new System.Drawing.Point(0, 0);
            this.ribbon1.OrbDropDown.MenuItems.Add(this.ribbonOrbMenuItem1);
            this.ribbon1.OrbDropDown.MenuItems.Add(this.ribbonSeparator1);
            this.ribbon1.OrbDropDown.MenuItems.Add(this.ribbonOrbMenuItem2);
            this.ribbon1.OrbDropDown.MenuItems.Add(this.ribbonSeparator2);
            this.ribbon1.OrbDropDown.MenuItems.Add(this.ribbonOrbMenuItem3);
            this.ribbon1.OrbDropDown.MenuItems.Add(this.ribbonSeparator3);
            this.ribbon1.OrbDropDown.MenuItems.Add(this.ribbonOrbMenuItem4);
            this.ribbon1.OrbDropDown.MenuItems.Add(this.ribbonSeparator4);
            this.ribbon1.OrbDropDown.MenuItems.Add(this.ribbonOrbMenuItem6);
            this.ribbon1.OrbDropDown.MenuItems.Add(this.ribbonSeparator6);
            this.ribbon1.OrbDropDown.MenuItems.Add(this.ribbonOrbMenuItem7);
            this.ribbon1.OrbDropDown.Name = "";
            this.ribbon1.OrbDropDown.OptionItemsPadding = 0;
            this.ribbon1.OrbDropDown.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ribbon1.OrbDropDown.Size = new System.Drawing.Size(162, 351);
            this.ribbon1.OrbDropDown.TabIndex = 0;
            this.ribbon1.OrbImage = null;
            this.ribbon1.OrbStyle = System.Windows.Forms.RibbonOrbStyle.Office_2010;
            this.ribbon1.OrbText = "Menü";
            // 
            // 
            // 
            this.ribbon1.QuickAcessToolbar.Visible = false;
            this.ribbon1.RibbonTabFont = new System.Drawing.Font("Trebuchet MS", 9F, System.Drawing.FontStyle.Bold);
            this.ribbon1.Size = new System.Drawing.Size(1099, 138);
            this.ribbon1.TabIndex = 0;
            this.ribbon1.Tabs.Add(this.ribbonTab1);
            this.ribbon1.Tabs.Add(this.ribbonTab2);
            this.ribbon1.Tabs.Add(this.ribbonTab4);
            this.ribbon1.Tabs.Add(this.ribbonTab5);
            this.ribbon1.TabsMargin = new System.Windows.Forms.Padding(12, 2, 20, 0);
            this.ribbon1.Text = "ribbon1";
            this.ribbon1.ThemeColor = System.Windows.Forms.RibbonTheme.Blue;
            this.ribbon1.ActiveTabChanged += new System.EventHandler(this.ribbon1_ActiveTabChanged);
            // 
            // ribbonTab1
            // 
            this.ribbonTab1.Panels.Add(this.ribbonPanel1);
            this.ribbonTab1.Panels.Add(this.ribbonPanel2);
            this.ribbonTab1.Panels.Add(this.ribbonPanel11);
            this.ribbonTab1.Panels.Add(this.ribbonPanel17);
            this.ribbonTab1.Panels.Add(this.ribbonPanel12);
            this.ribbonTab1.Text = "ÖĞRENCİ";
            // 
            // ribbonPanel1
            // 
            this.ribbonPanel1.ButtonMoreVisible = false;
            this.ribbonPanel1.Items.Add(this.btnOgrenci);
            this.ribbonPanel1.Text = "Öğrenciler";
            // 
            // ribbonPanel2
            // 
            this.ribbonPanel2.ButtonMoreVisible = false;
            this.ribbonPanel2.Items.Add(this.btnOgrenciGorusme);
            this.ribbonPanel2.Text = "Öğrenci Görüşme";
            // 
            // ribbonPanel11
            // 
            this.ribbonPanel11.ButtonMoreVisible = false;
            this.ribbonPanel11.Items.Add(this.btnGorusmeKayitlari);
            this.ribbonPanel11.Text = "Görüşme Kayıtları";
            // 
            // ribbonPanel17
            // 
            this.ribbonPanel17.ButtonMoreVisible = false;
            this.ribbonPanel17.Items.Add(this.btnGorusmeMebForm);
            this.ribbonPanel17.Text = "Görüşme MEB Form";
            // 
            // ribbonPanel12
            // 
            this.ribbonPanel12.ButtonMoreVisible = false;
            this.ribbonPanel12.Items.Add(this.btnOgrenciIstatistik);
            this.ribbonPanel12.Text = "İstatistikler";
            // 
            // ribbonTab2
            // 
            this.ribbonTab2.Panels.Add(this.ribbonPanel3);
            this.ribbonTab2.Panels.Add(this.ribbonPanel18);
            this.ribbonTab2.Panels.Add(this.ribbonPanel4);
            this.ribbonTab2.Panels.Add(this.ribbonPanel13);
            this.ribbonTab2.Panels.Add(this.ribbonPanel5);
            this.ribbonTab2.Text = "VELİ";
            // 
            // ribbonPanel3
            // 
            this.ribbonPanel3.ButtonMoreVisible = false;
            this.ribbonPanel3.Items.Add(this.btnVeliGorusme);
            this.ribbonPanel3.Text = "Veli Görüşme";
            // 
            // ribbonPanel4
            // 
            this.ribbonPanel4.ButtonMoreVisible = false;
            this.ribbonPanel4.Items.Add(this.btnVeliZiyaret);
            this.ribbonPanel4.Text = "Veli Ziyareti";
            // 
            // ribbonPanel13
            // 
            this.ribbonPanel13.ButtonMoreVisible = false;
            this.ribbonPanel13.Items.Add(this.btnVeliKayitlari);
            this.ribbonPanel13.Text = "Veli Kayıtları";
            // 
            // ribbonPanel5
            // 
            this.ribbonPanel5.ButtonMoreVisible = false;
            this.ribbonPanel5.Items.Add(this.btnVeliIstatistik);
            this.ribbonPanel5.Text = "İstatistikler";
            // 
            // ribbonTab4
            // 
            this.ribbonTab4.Panels.Add(this.ribbonPanel6);
            this.ribbonTab4.Panels.Add(this.ribbonPanel14);
            this.ribbonTab4.Panels.Add(this.ribbonPanel7);
            this.ribbonTab4.Text = "ÖĞRETMEN";
            // 
            // ribbonPanel6
            // 
            this.ribbonPanel6.ButtonMoreVisible = false;
            this.ribbonPanel6.Items.Add(this.btnOgrtGorusme);
            this.ribbonPanel6.Text = "Öğretmen Görüşme";
            // 
            // ribbonPanel14
            // 
            this.ribbonPanel14.ButtonMoreVisible = false;
            this.ribbonPanel14.Items.Add(this.btnOgretmenKayitlari);
            this.ribbonPanel14.Text = "Öğretmen Kayıtları";
            // 
            // ribbonPanel7
            // 
            this.ribbonPanel7.ButtonMoreVisible = false;
            this.ribbonPanel7.Items.Add(this.btnOgretmenIstatistik);
            this.ribbonPanel7.Text = "İstatistikler  ";
            // 
            // ribbonTab5
            // 
            this.ribbonTab5.Panels.Add(this.ribbonPanel10);
            this.ribbonTab5.Panels.Add(this.ribbonPanel20);
            this.ribbonTab5.Panels.Add(this.ribbonPanel15);
            this.ribbonTab5.Panels.Add(this.ribbonPanel16);
            this.ribbonTab5.Text = "AYARLAR";
            // 
            // ribbonPanel10
            // 
            this.ribbonPanel10.ButtonMoreVisible = false;
            this.ribbonPanel10.Items.Add(this.btnGenelAyarlar);
            this.ribbonPanel10.Text = "Genel Ayarlar";
            // 
            // ribbonPanel20
            // 
            this.ribbonPanel20.ButtonMoreVisible = false;
            this.ribbonPanel20.Items.Add(this.ribbonButton2);
            this.ribbonPanel20.Text = "Sınıf Düzenleme";
            // 
            // ribbonPanel15
            // 
            this.ribbonPanel15.ButtonMoreVisible = false;
            this.ribbonPanel15.Items.Add(this.btnSabitleriDuzenle);
            this.ribbonPanel15.Text = "Sabitleri Düzenle";
            // 
            // ribbonPanel16
            // 
            this.ribbonPanel16.ButtonMoreVisible = false;
            this.ribbonPanel16.Items.Add(this.ribbonButton1);
            this.ribbonPanel16.Text = "Öğrenci İşlemleri";
            // 
            // ribbonPanel8
            // 
            this.ribbonPanel8.ButtonMoreVisible = false;
            this.ribbonPanel8.Items.Add(this.btnKitapOkuyanlar);
            this.ribbonPanel8.Text = "Kitap Okuyanlar";
            // 
            // ribbonPanel9
            // 
            this.ribbonPanel9.ButtonMoreVisible = false;
            this.ribbonPanel9.Items.Add(this.btnOkunanKitaplar);
            this.ribbonPanel9.Text = "Okunan Kitaplar";
            // 
            // ribbonPanel18
            // 
            this.ribbonPanel18.ButtonMoreVisible = false;
            this.ribbonPanel18.Items.Add(this.btnVeliGorusmeMEB);
            this.ribbonPanel18.Text = "Veli Görüşme MEB";
            // 
            // anaPanel
            // 
            this.anaPanel.AutoScroll = true;
            this.anaPanel.BackColor = System.Drawing.Color.Silver;
            this.anaPanel.BackgroundImage = global::rbsProje.Properties.Resources._01_150ppp;
            this.anaPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.anaPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.anaPanel.Location = new System.Drawing.Point(0, 138);
            this.anaPanel.Name = "anaPanel";
            this.anaPanel.Size = new System.Drawing.Size(1099, 608);
            this.anaPanel.TabIndex = 1;
            // 
            // ribbonOrbMenuItem1
            // 
            this.ribbonOrbMenuItem1.DrawIconsBar = false;
            this.ribbonOrbMenuItem1.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.ribbonOrbMenuItem1.Image = global::rbsProje.Properties.Resources.house_with_chimney;
            this.ribbonOrbMenuItem1.SmallImage = global::rbsProje.Properties.Resources.house_with_chimney;
            this.ribbonOrbMenuItem1.Text = "Ana Ekran";
            this.ribbonOrbMenuItem1.Click += new System.EventHandler(this.ribbonOrbMenuItem1_Click);
            // 
            // ribbonOrbMenuItem2
            // 
            this.ribbonOrbMenuItem2.DrawIconsBar = false;
            this.ribbonOrbMenuItem2.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.ribbonOrbMenuItem2.Image = global::rbsProje.Properties.Resources.kmeeting;
            this.ribbonOrbMenuItem2.SmallImage = global::rbsProje.Properties.Resources.kmeeting;
            this.ribbonOrbMenuItem2.Text = "Öğrenci";
            this.ribbonOrbMenuItem2.Click += new System.EventHandler(this.ribbonOrbMenuItem2_Click);
            // 
            // ribbonOrbMenuItem3
            // 
            this.ribbonOrbMenuItem3.DrawIconsBar = false;
            this.ribbonOrbMenuItem3.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.ribbonOrbMenuItem3.Image = global::rbsProje.Properties.Resources.kmeetingVeli;
            this.ribbonOrbMenuItem3.SmallImage = global::rbsProje.Properties.Resources.kmeetingVeli;
            this.ribbonOrbMenuItem3.Text = "Veli";
            this.ribbonOrbMenuItem3.Click += new System.EventHandler(this.ribbonOrbMenuItem3_Click);
            // 
            // ribbonOrbMenuItem4
            // 
            this.ribbonOrbMenuItem4.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.ribbonOrbMenuItem4.Image = global::rbsProje.Properties.Resources.kmeetingOgrt;
            this.ribbonOrbMenuItem4.SmallImage = global::rbsProje.Properties.Resources.kmeetingOgrt;
            this.ribbonOrbMenuItem4.Text = "Öğretmen";
            this.ribbonOrbMenuItem4.Click += new System.EventHandler(this.ribbonOrbMenuItem4_Click);
            // 
            // ribbonOrbMenuItem6
            // 
            this.ribbonOrbMenuItem6.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.ribbonOrbMenuItem6.Image = global::rbsProje.Properties.Resources.k1489885830_Settings_5;
            this.ribbonOrbMenuItem6.SmallImage = global::rbsProje.Properties.Resources.k1489885830_Settings_5;
            this.ribbonOrbMenuItem6.Text = "Ayarlar";
            this.ribbonOrbMenuItem6.Click += new System.EventHandler(this.ribbonOrbMenuItem6_Click);
            // 
            // ribbonOrbMenuItem7
            // 
            this.ribbonOrbMenuItem7.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.ribbonOrbMenuItem7.Image = global::rbsProje.Properties.Resources.error;
            this.ribbonOrbMenuItem7.SmallImage = global::rbsProje.Properties.Resources.error;
            this.ribbonOrbMenuItem7.Text = "Çıkış";
            this.ribbonOrbMenuItem7.Click += new System.EventHandler(this.ribbonOrbMenuItem7_Click);
            // 
            // btnOgrenci
            // 
            this.btnOgrenci.Image = global::rbsProje.Properties.Resources.meeting;
            this.btnOgrenci.MinimumSize = new System.Drawing.Size(100, 81);
            this.btnOgrenci.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnOgrenci.SmallImage")));
            this.btnOgrenci.Text = "";
            this.btnOgrenci.Click += new System.EventHandler(this.btnOgrenci_Click);
            // 
            // btnOgrenciGorusme
            // 
            this.btnOgrenciGorusme.Image = global::rbsProje.Properties.Resources.interview;
            this.btnOgrenciGorusme.MinimumSize = new System.Drawing.Size(100, 81);
            this.btnOgrenciGorusme.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnOgrenciGorusme.SmallImage")));
            this.btnOgrenciGorusme.Text = "";
            this.btnOgrenciGorusme.Click += new System.EventHandler(this.btnOgrenciGorusme_Click);
            // 
            // btnGorusmeKayitlari
            // 
            this.btnGorusmeKayitlari.Image = global::rbsProje.Properties.Resources.criminal_record;
            this.btnGorusmeKayitlari.MinimumSize = new System.Drawing.Size(110, 84);
            this.btnGorusmeKayitlari.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnGorusmeKayitlari.SmallImage")));
            this.btnGorusmeKayitlari.Text = "";
            this.btnGorusmeKayitlari.Click += new System.EventHandler(this.btnGorusmeKayitlari_Click);
            // 
            // btnGorusmeMebForm
            // 
            this.btnGorusmeMebForm.Image = global::rbsProje.Properties.Resources.gorBtnLogo;
            this.btnGorusmeMebForm.MinimumSize = new System.Drawing.Size(120, 84);
            this.btnGorusmeMebForm.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnGorusmeMebForm.SmallImage")));
            this.btnGorusmeMebForm.Text = "";
            this.btnGorusmeMebForm.Click += new System.EventHandler(this.btnGorusmeMebForm_Click);
            // 
            // btnOgrenciIstatistik
            // 
            this.btnOgrenciIstatistik.Image = global::rbsProje.Properties.Resources.group_and_arrow;
            this.btnOgrenciIstatistik.MinimumSize = new System.Drawing.Size(110, 81);
            this.btnOgrenciIstatistik.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnOgrenciIstatistik.SmallImage")));
            this.btnOgrenciIstatistik.Text = "";
            this.btnOgrenciIstatistik.Click += new System.EventHandler(this.btnOgrenciIstatistik_Click);
            // 
            // btnVeliGorusme
            // 
            this.btnVeliGorusme.Image = global::rbsProje.Properties.Resources.meetingVeli;
            this.btnVeliGorusme.MinimumSize = new System.Drawing.Size(89, 81);
            this.btnVeliGorusme.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnVeliGorusme.SmallImage")));
            this.btnVeliGorusme.Text = "";
            this.btnVeliGorusme.Click += new System.EventHandler(this.btnVeliGorusme_Click);
            // 
            // btnVeliGorusmeMEB
            // 
            this.btnVeliGorusmeMEB.Image = global::rbsProje.Properties.Resources.ogrGorMEB;
            this.btnVeliGorusmeMEB.MinimumSize = new System.Drawing.Size(110, 81);
            this.btnVeliGorusmeMEB.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnVeliGorusmeMEB.SmallImage")));
            this.btnVeliGorusmeMEB.Text = "";
            this.btnVeliGorusmeMEB.Click += new System.EventHandler(this.btnVeliGorusmeMEB_Click);
            // 
            // btnVeliZiyaret
            // 
            this.btnVeliZiyaret.Image = global::rbsProje.Properties.Resources.business_meeting;
            this.btnVeliZiyaret.MinimumSize = new System.Drawing.Size(89, 81);
            this.btnVeliZiyaret.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnVeliZiyaret.SmallImage")));
            this.btnVeliZiyaret.Text = "";
            this.btnVeliZiyaret.Click += new System.EventHandler(this.btnVeliZiyaret_Click);
            // 
            // btnVeliKayitlari
            // 
            this.btnVeliKayitlari.Image = global::rbsProje.Properties.Resources.document;
            this.btnVeliKayitlari.MinimumSize = new System.Drawing.Size(85, 82);
            this.btnVeliKayitlari.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnVeliKayitlari.SmallImage")));
            this.btnVeliKayitlari.Text = "";
            this.btnVeliKayitlari.Click += new System.EventHandler(this.btnVeliKayitlari_Click);
            // 
            // btnVeliIstatistik
            // 
            this.btnVeliIstatistik.Image = global::rbsProje.Properties.Resources.statistics;
            this.btnVeliIstatistik.MinimumSize = new System.Drawing.Size(89, 81);
            this.btnVeliIstatistik.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnVeliIstatistik.SmallImage")));
            this.btnVeliIstatistik.Text = "";
            this.btnVeliIstatistik.Click += new System.EventHandler(this.btnVeliIstatistik_Click);
            // 
            // btnOgrtGorusme
            // 
            this.btnOgrtGorusme.Image = global::rbsProje.Properties.Resources.meetingOgrt;
            this.btnOgrtGorusme.MinimumSize = new System.Drawing.Size(120, 81);
            this.btnOgrtGorusme.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnOgrtGorusme.SmallImage")));
            this.btnOgrtGorusme.Text = "";
            this.btnOgrtGorusme.Click += new System.EventHandler(this.btnOgrtGorusme_Click);
            // 
            // btnOgretmenKayitlari
            // 
            this.btnOgretmenKayitlari.Image = global::rbsProje.Properties.Resources.filing_cabinet;
            this.btnOgretmenKayitlari.MinimumSize = new System.Drawing.Size(110, 82);
            this.btnOgretmenKayitlari.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnOgretmenKayitlari.SmallImage")));
            this.btnOgretmenKayitlari.Text = "";
            this.btnOgretmenKayitlari.Click += new System.EventHandler(this.btnOgretmenKayitlari_Click);
            // 
            // btnOgretmenIstatistik
            // 
            this.btnOgretmenIstatistik.Image = global::rbsProje.Properties.Resources.statistics__1_;
            this.btnOgretmenIstatistik.MinimumSize = new System.Drawing.Size(92, 81);
            this.btnOgretmenIstatistik.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnOgretmenIstatistik.SmallImage")));
            this.btnOgretmenIstatistik.Text = "";
            // 
            // btnGenelAyarlar
            // 
            this.btnGenelAyarlar.Image = global::rbsProje.Properties.Resources._1489885830_Settings_5;
            this.btnGenelAyarlar.MinimumSize = new System.Drawing.Size(95, 81);
            this.btnGenelAyarlar.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnGenelAyarlar.SmallImage")));
            this.btnGenelAyarlar.Text = "";
            this.btnGenelAyarlar.Click += new System.EventHandler(this.btnGenelAyarlar_Click);
            // 
            // ribbonButton2
            // 
            this.ribbonButton2.Image = global::rbsProje.Properties.Resources.lesson;
            this.ribbonButton2.MinimumSize = new System.Drawing.Size(100, 81);
            this.ribbonButton2.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton2.SmallImage")));
            this.ribbonButton2.Text = "";
            this.ribbonButton2.Click += new System.EventHandler(this.ribbonButton2_Click_1);
            // 
            // btnSabitleriDuzenle
            // 
            this.btnSabitleriDuzenle.Image = global::rbsProje.Properties.Resources.notebook;
            this.btnSabitleriDuzenle.MinimumSize = new System.Drawing.Size(110, 82);
            this.btnSabitleriDuzenle.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnSabitleriDuzenle.SmallImage")));
            this.btnSabitleriDuzenle.Text = "";
            this.btnSabitleriDuzenle.Click += new System.EventHandler(this.btnSabitleriDuzenle_Click);
            // 
            // ribbonButton1
            // 
            this.ribbonButton1.Image = global::rbsProje.Properties.Resources.hierarchical_structure;
            this.ribbonButton1.MinimumSize = new System.Drawing.Size(120, 82);
            this.ribbonButton1.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton1.SmallImage")));
            this.ribbonButton1.Text = "";
            this.ribbonButton1.Click += new System.EventHandler(this.ribbonButton1_Click);
            // 
            // btnKitapOkuyanlar
            // 
            this.btnKitapOkuyanlar.Image = global::rbsProje.Properties.Resources.popularity;
            this.btnKitapOkuyanlar.MaximumSize = new System.Drawing.Size(100, 0);
            this.btnKitapOkuyanlar.MinimumSize = new System.Drawing.Size(104, 81);
            this.btnKitapOkuyanlar.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnKitapOkuyanlar.SmallImage")));
            this.btnKitapOkuyanlar.Text = "";
            this.btnKitapOkuyanlar.ToolTip = "En Çok Kitap Okuyan Öğrencileri Listeler";
            // 
            // btnOkunanKitaplar
            // 
            this.btnOkunanKitaplar.Image = global::rbsProje.Properties.Resources.analytics;
            this.btnOkunanKitaplar.MinimumSize = new System.Drawing.Size(104, 81);
            this.btnOkunanKitaplar.SmallImage = ((System.Drawing.Image)(resources.GetObject("btnOkunanKitaplar.SmallImage")));
            this.btnOkunanKitaplar.Text = "";
            this.btnOkunanKitaplar.ToolTip = "En Çok Okunan Kitapları Listeler";
            // 
            // ribbonButton3
            // 
            this.ribbonButton3.Image = ((System.Drawing.Image)(resources.GetObject("ribbonButton3.Image")));
            this.ribbonButton3.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton3.SmallImage")));
            // 
            // ribbonButton4
            // 
            this.ribbonButton4.Image = ((System.Drawing.Image)(resources.GetObject("ribbonButton4.Image")));
            this.ribbonButton4.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton4.SmallImage")));
            // 
            // AnaEkran
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1099, 746);
            this.Controls.Add(this.anaPanel);
            this.Controls.Add(this.ribbon1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.Name = "AnaEkran";
            this.Text = "Rehberlik Bilgi Sistemi";
            this.Load += new System.EventHandler(this.AnaEkran_Load);
            this.SizeChanged += new System.EventHandler(this.AnaEkran_SizeChanged);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Ribbon ribbon1;
        private System.Windows.Forms.RibbonTab ribbonTab1;
        private System.Windows.Forms.RibbonPanel ribbonPanel1;
        private System.Windows.Forms.RibbonPanel ribbonPanel2;
        private System.Windows.Forms.RibbonTab ribbonTab2;
        private System.Windows.Forms.RibbonPanel ribbonPanel3;
        private System.Windows.Forms.RibbonPanel ribbonPanel4;
        private System.Windows.Forms.RibbonPanel ribbonPanel5;
        private System.Windows.Forms.RibbonPanel ribbonPanel12;
        private System.Windows.Forms.RibbonTab ribbonTab4;
        private System.Windows.Forms.RibbonPanel ribbonPanel6;
        private System.Windows.Forms.RibbonPanel ribbonPanel7;
        private System.Windows.Forms.RibbonTab ribbonTab5;
        private System.Windows.Forms.RibbonPanel ribbonPanel10;
        private System.Windows.Forms.RibbonButton ribbonButton3;
        private System.Windows.Forms.RibbonButton ribbonButton4;
        private System.Windows.Forms.RibbonButton btnOgrenciGorusme;
        private System.Windows.Forms.RibbonButton btnVeliIstatistik;
        private System.Windows.Forms.RibbonButton btnVeliGorusme;
        private System.Windows.Forms.RibbonButton btnVeliZiyaret;
        private System.Windows.Forms.RibbonButton btnOgrtGorusme;
        private System.Windows.Forms.RibbonButton btnOgretmenIstatistik;
        private System.Windows.Forms.RibbonButton btnGenelAyarlar;
        private System.Windows.Forms.RibbonButton btnOgrenci;
        private System.Windows.Forms.RibbonButton btnOgrenciIstatistik;
        private System.Windows.Forms.RibbonOrbMenuItem ribbonOrbMenuItem1;
        private System.Windows.Forms.RibbonOrbMenuItem ribbonOrbMenuItem2;
        private System.Windows.Forms.RibbonOrbMenuItem ribbonOrbMenuItem3;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator1;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator2;
        private System.Windows.Forms.RibbonOrbMenuItem ribbonOrbMenuItem4;
        private System.Windows.Forms.RibbonOrbMenuItem ribbonOrbMenuItem6;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator3;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator4;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator6;
        private System.Windows.Forms.RibbonOrbMenuItem ribbonOrbMenuItem7;
        public System.Windows.Forms.Panel anaPanel;
        private System.Windows.Forms.RibbonPanel ribbonPanel20;
        private System.Windows.Forms.RibbonButton ribbonButton2;
        private System.Windows.Forms.RibbonButton btnKitapOkuyanlar;
        private System.Windows.Forms.RibbonPanel ribbonPanel8;
        private System.Windows.Forms.RibbonButton btnOkunanKitaplar;
        private System.Windows.Forms.RibbonPanel ribbonPanel9;
        private System.Windows.Forms.RibbonPanel ribbonPanel11;
        private System.Windows.Forms.RibbonButton btnGorusmeKayitlari;
        private System.Windows.Forms.RibbonPanel ribbonPanel13;
        private System.Windows.Forms.RibbonButton btnVeliKayitlari;
        private System.Windows.Forms.RibbonPanel ribbonPanel14;
        private System.Windows.Forms.RibbonButton btnOgretmenKayitlari;
        private System.Windows.Forms.RibbonPanel ribbonPanel15;
        private System.Windows.Forms.RibbonButton btnSabitleriDuzenle;
        private System.Windows.Forms.RibbonPanel ribbonPanel16;
        private System.Windows.Forms.RibbonButton ribbonButton1;
        private System.Windows.Forms.RibbonPanel ribbonPanel17;
        private System.Windows.Forms.RibbonButton btnGorusmeMebForm;
        private System.Windows.Forms.RibbonPanel ribbonPanel18;
        private System.Windows.Forms.RibbonButton btnVeliGorusmeMEB;
    }
}