﻿using rbsProje.Kontroller;
using rbsProje.Kontroller.Ayarlar;
using rbsProje.Kontroller.Ogretmen;
using rbsProje.Kontroller.Uyeler;
using rbsProje.Kontroller.Veli;
using System;
using System.Windows.Forms;

namespace rbsProje
{
    public partial class AnaEkran : Form
    {
        public AnaEkran()
        {
            InitializeComponent();
        }

        public void PaneliTemizle()
        {
            if (anaPanel.Controls.Count > 0)
            {
                anaPanel.Controls[0].Dispose();
            }
            anaPanel.Controls.Clear();
            anaPanel.BackgroundImage = null;
        }

        private void PaneliYenile()
        {
            if (anaPanel.Controls.Count > 0)
            {
                anaPanel.Controls[0].Dispose();
            }
            anaPanel.Controls.Clear();
            anaPanel.BackgroundImage = rbsProje.Properties.Resources._01_150ppp;
        }

        private void PaneliOrtala()
        {
            if (anaPanel.Controls.Count > 0)
            {
                anaPanel.Controls[0].Left = (anaPanel.Width - anaPanel.Controls[0].Width) / 2;
                anaPanel.Controls[0].Top = ((anaPanel.Height - anaPanel.Controls[0].Height) / 2);
            }
        }

        private void AnaEkran_SizeChanged(object sender, EventArgs e)
        {
            PaneliOrtala();
        }

        private void AnaEkran_Load(object sender, EventArgs e)
        {
            this.Text = this.Text + "  v1.0";
        }


        #region Menü Butonları İşlemleri

        private void ribbon1_ActiveTabChanged(object sender, EventArgs e)
        {
            PaneliYenile();
        }

        private void ribbonOrbMenuItem1_Click(object sender, EventArgs e)
        {
            PaneliYenile();
            ribbon1.ActiveTab = ribbonTab1;
        }

        private void ribbonOrbMenuItem2_Click(object sender, EventArgs e)
        {
            PaneliYenile();
            ribbon1.ActiveTab = ribbonTab1;
        }

        private void ribbonOrbMenuItem3_Click(object sender, EventArgs e)
        {
            PaneliYenile();
            ribbon1.ActiveTab = ribbonTab2;
        }

        private void ribbonOrbMenuItem4_Click(object sender, EventArgs e)
        {
            PaneliYenile();
            ribbon1.ActiveTab = ribbonTab4;
        }

        private void ribbonOrbMenuItem6_Click(object sender, EventArgs e)
        {
            PaneliYenile();
            ribbon1.ActiveTab = ribbonTab5;
        }

        private void ribbonOrbMenuItem7_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        #endregion


        #region Ayar İşlemleri

        private void btnGenelAyarlar_Click(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCayarlar ayar = new UCayarlar();
            anaPanel.Controls.Add(ayar);
            PaneliOrtala();
        }

        private void ribbonButton2_Click_1(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCsinifEkle sinif = new UCsinifEkle();
            anaPanel.Controls.Add(sinif);
            PaneliOrtala();
        }

        #endregion


        private void btnVeliIstatistik_Click(object sender, EventArgs e)
        {

        }

        private void btnOgrenci_Click(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCuyeListele2 uye = new UCuyeListele2();
            anaPanel.Controls.Add(uye);
            PaneliOrtala();
        }



        private void btnOgrenciGorusme_Click(object sender, EventArgs e)
        {
            frmUyeGorusme.GetForm.Show();
        }

        private void btnVeliGorusme_Click(object sender, EventArgs e)
        {
            frmVeliGor.GetForm.Show();
        }

        private void btnGorusmeKayitlari_Click(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCuyeGorusmeKayitlar uyeGorusmeKayitlar = new UCuyeGorusmeKayitlar();
            anaPanel.Controls.Add(uyeGorusmeKayitlar);
            PaneliOrtala();
        }

        private void btnVeliKayitlari_Click(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCveliGorusmeKayitlar veliGorusmeKayitlar = new UCveliGorusmeKayitlar();
            anaPanel.Controls.Add(veliGorusmeKayitlar);
            PaneliOrtala();
        }

        private void btnOgrtGorusme_Click(object sender, EventArgs e)
        {
            frmOgretmenGorusme.GetForm.Show();
        }

        private void btnOgretmenKayitlari_Click(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCgorusmeKayitlar ogretmenGorusmeKayitlar = new UCgorusmeKayitlar();
            anaPanel.Controls.Add(ogretmenGorusmeKayitlar);
            PaneliOrtala();
        }

        private void btnSabitleriDuzenle_Click(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCsabitler sabitler = new UCsabitler();
            anaPanel.Controls.Add(sabitler);
            PaneliOrtala();
        }

        private void ribbonButton1_Click(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCogrenciIslemleri ogrenciIslemler = new UCogrenciIslemleri();
            anaPanel.Controls.Add(ogrenciIslemler);
            PaneliOrtala();
        }

        private void btnVeliZiyaret_Click(object sender, EventArgs e)
        {
            frmVeliZiyaret.GetForm.Show();
        }

        private void btnOgrenciIstatistik_Click(object sender, EventArgs e)
        {
            PaneliTemizle();
            UCogrenciIstatistik ogrenciIstatistik = new UCogrenciIstatistik();
            anaPanel.Controls.Add(ogrenciIstatistik);
            PaneliOrtala();
        }

        private void btnGorusmeMebForm_Click(object sender, EventArgs e)
        {
            frmUyeGorusmeMEB.GetForm.Show(); 
        }

        private void btnVeliGorusmeMEB_Click(object sender, EventArgs e)
        {
            frmVeliGorusmeMEB.GetForm.Show();
        }
    }
}
